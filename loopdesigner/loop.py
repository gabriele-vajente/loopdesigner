# loop.py - LoopDesigner, Gabriele Vajente (vajente@caltech.edu), 2024-02-06

import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore
from pyqtgraph.Qt import QtWidgets
from pyqtgraph.dockarea.Dock import Dock
from pyqtgraph.dockarea.DockArea import DockArea
import warnings
warnings.filterwarnings('ignore')
from .tf import TF
from .optimize import Optimizer
from queue import LifoQueue
import pickle
try:
    import foton
except:
    print('Cannot import foton library, filter import/export in LIGO format not available')
import scipy
import fnmatch
import os
import gpstime

# # for debugging purposes [https://stackoverflow.com/questions/6234405/logging-uncaught-exceptions-in-python]
import sys
import logging
import traceback

def log_except_hook(*exc_info):
    text = "".join(traceback.format_exception(*exc_info()))
    logging.error("Unhandled exception: %s", text)

sys.excepthook = log_except_hook

########################################################################################################################

class LoopDesigner:
    """
    Graphical user interface to design and tune single-input single-output feedback control loops. Loosely inspired by
    MATLAB sisotool

    Gabriele Vajente (vajente@caltech.edu) - 2024-02-06

    Parameters
    ----------
    plant: TF or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
        the plant to be controller, either as a TF object, or as a list [z, p, k]
    controller: TF or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
        optional, the controller transfer function, either as a TF object, or as a list [z, p, k]
    references: dict
        optional, a dictionary of references, where the keys are the names and the values are
        TF objects that will be added initially as references

    If no plant is provided, opens a dialog to load a saved session from file.

    Examples
    --------

    Please refer to the README.md file for instructions on how to use the GUI

    """
    def __init__(self, plant=None, controller=None, references=None):
        """
        Class constructor

        Parameters
        ----------
        plant: TF or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
            the plant to be controller, either as a TF object, or as a list [z, p, k]
        controller: TF or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
            optional, the controller transfer function, either as a TF object, or as a list [z, p, k]
        references: dict
            optional, a dictionary of references, where the keys are the names and the values are
            TF objects that will be added initially as references
        """

        #### Initialize variables

        if plant is not None:
            # save plant
            self.plant = plant.copy()
            load_session = False
        else:
            # start with empty plant, will ask user later to load a saved session
            self.plant = TF([], [], 1)
            load_session = True

        if controller is not None:
            # save controller
            self.ctrl = controller.copy()
        else:
            # start with unity controller if none is passed
            self.ctrl = TF([], [], 1)

        # convert [z,p,k] into TF
        if type(self.plant) is not TF:
            self.plant = TF(self.plant[0], self.plant[1], self.plant[2])
        if type(self.ctrl) is not TF:
            self.ctrl = TF(self.ctrl[0], self.ctrl[1], self.ctrl[2])

        # check if the plant or controller have a delay
        if self.plant.delay != 0 and not self.plant.delay_approx_pade:
            print("WARNING: the plant transfer function contains an exact delay, it will be replaced using a Padé approximation")
            self.plant = TF(z=self.plant.z, p=self.plant.p, k=self.plant.k, delay=self.plant.delay, 
                            pade_order=3, name=self.plant.name)
        if self.ctrl.delay != 0 and not self.ctrl.delay_approx_pade:
            print("WARNING: the controller transfer function contains an exact delay, it will be replaced using a Padé approximation")
            self.ctrl = TF(z=self.ctrl.z, p=self.ctrl.p, k=self.ctrl.k, delay=self.ctrl.delay, 
                            pade_order=3, name=self.ctrl.name)

        # to avoid plotting issues when the sampled value of the TF is zero (probably not needed)
        self.plant.zero_to_nan = True
        self.ctrl.zero_to_nan = True

        # frequency bins to use, find the lowest and highest frequencies of poles and zeros and space logarithmically
        self.nfr = 1000           # number of points for the  plots
        self.fr_update_thr = 0.1  # update plots when zoom changes this much
        self.fr = self.initial_frequency_bins(nfr=1000, factor=3)
        # frequency bins for margin computation, span a larger frequency band and use more points
        self.fr_stab = np.geomspace(self.fr.min()/10, self.fr.max()*10, 3*self.nfr)

        # variables to contain graphical elements
        self.line_olg_abs = None
        self.line_olg_phi = None
        self.line_clg1 = None
        self.line_clg2 = None
        self.plant_graph = None
        self.gain_margin1 = None
        self.gain_margin2 = None
        self.phase_margin = None

        # legends
        self.legend_abs = None
        self.legend_phi = None
        self.legend_clg1 = None
        self.legend_clg2 = None

        # variables for reference fits
        self.reference_ctrl = []
        self.reference_color = []
        self.reference_items = []
        self.reference_plot_abs = []
        self.reference_plot_phi = []
        self.reference_plot_clg1 = []
        self.reference_plot_clg2 = []

        # variable to allow keeping gain locked at a given frequency
        self.lock_gain_freq = None
        self.lock_gain_value = None
        self.lock_gain_dot = None

        # LIFO queue for undos
        self.undo = LifoQueue()

        # how much poles and zeros are moved with the keyboard (user can adjust this)
        self.frequency_increment = 0.003
        self.q_increment = 0.05
        self.gain_increment = 1

        # true after plots are done, to prevent runaway zoom
        self.plotted = False

        #### create GUI

        # main app and window
        self.app = pg.mkQApp("Loop Designer")
        self.win = WindowKeyEvent(self)   # class defined below, inherited from QMainWindow, to handle keyboard events
        self.win.resize(1600,1000)        # not useful, since we start the window maximized below
        self.win.setWindowTitle("Loop Designer")
        # catch closing of main window to close everything else
        self.win.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.win.destroyed.connect(self.close_all)
        # pyqtgraph options, white background
        pg.setConfigOptions(antialias=True)
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')

        # add the edit toolbar
        self.edit_toolbar = QtWidgets.QToolBar("EditToolbar")
        self.win.addToolBar(self.edit_toolbar)
        # add buttons to the toolbar
        self.bCrosshair = QtWidgets.QAction("Crosshair", self.win)
        self.bCrosshair.setToolTip("Toggle display of crosshairs")
        self.bCrosshair.setCheckable(True)
        self.edit_toolbar.addAction(self.bCrosshair)
        self.bFitAddCmpxPole = QtWidgets.QAction("Add cpx p", self.win)
        self.bFitAddCmpxPole.setToolTip("Add a complex pole to the current controller. Click on the curve to select the "
                                        "frequency. \nThe pole is added with an initial Q of 1/sqrt(2). As long as the"
                                        " button is selected, you can add more poles")
        self.bFitAddCmpxPole.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitAddCmpxPole)
        self.bFitAddRealPole = QtWidgets.QAction("Add real p", self.win)
        self.bFitAddRealPole.setCheckable(True)
        self.bFitAddRealPole.setToolTip("Add a real pole to the current controller. Click on the curve to select the "
                                        "frequency. \nAs long as the button is selected, you can add more poles")
        self.edit_toolbar.addAction(self.bFitAddRealPole) 
        self.bFitAddCmpxZero = QtWidgets.QAction("Add cpx z", self.win)
        self.bFitAddCmpxZero.setToolTip("Add a complex zero to the current controller. Click on the curve to select the "
                                        "frequency. \nThe zero is added with an initial Q of 1/sqrt(2). As long as the"
                                        " button is selected, you can add more zeros")
        self.bFitAddCmpxZero.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitAddCmpxZero)
        self.bFitAddRealZero = QtWidgets.QAction("Add real z", self.win)
        self.bFitAddRealZero.setCheckable(True)
        self.bFitAddRealZero.setToolTip("Add a real zero to the current controller. Click on the curve to select "
                                        "the frequency. \nAs long as the button is selected, you can add more zeros")
        self.edit_toolbar.addAction(self.bFitAddRealZero)

        self.bFitAdd = QtWidgets.QAction("Add more", self.win)
        self.bFitAdd.setCheckable(False)
        self.bFitAdd.setToolTip("Add a low pass / band pass / high pass / band stop / notch /"
                                " resonant gain to the current controller. ")
        self.edit_toolbar.addAction(self.bFitAdd)

        self.bFitDelete = QtWidgets.QAction("Delete z/p", self.win)
        self.bFitDelete.setToolTip("Delete a pole or zero from the current fit, just click on it. ")
        self.bFitDelete.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitDelete)          
        self.bFitMovePZ = QtWidgets.QAction("Move z/p", self.win)
        self.bFitMovePZ.setToolTip("Move a pole or zero. You can either drag the marker around with your mouse, or "
                                   "select a pole or zero by clicking on a marker, \nand then use the keyboard to "
                                   "change frequency (left and right keys) or Q (up and down keys). The frequency and "
                                   "Q increment from keyboard can be adjusted in the status bar.")
        self.bFitMovePZ.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitMovePZ)            
        self.bFitChangeGain = QtWidgets.QAction("Change Gain", self.win)
        self.bFitChangeGain.setCheckable(True)
        self.bFitChangeGain.setToolTip("Change the overall gain, either by dragging the open loop line up or down, or "
                                       "using the keyboard: up and down keys for small adjustments, \nand page up and  "
                                       "page down for larger adjustments. The gain change from keyboad can be adjusted "
                                       "in the status bar.")
        self.edit_toolbar.addAction(self.bFitChangeGain)
        self.bFitLockGain = QtWidgets.QAction("Lock Gain at Freq", self.win)
        self.bFitLockGain.setToolTip('Lock the controller gain at a specified frequency. Initially, when this button is '
                                     'selected, you can click anywhere on the open loop curve \nand the gain at that '
                                     ' frequency will remain locked at the current value when you add or remove '
                                     ' poles and zeros. \nA black diamond marker shows the lock point. To disengage '
                                     ' simply deselect this button.')
        self.bFitLockGain.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitLockGain)
        self.bFitUndo = QtWidgets.QAction("Undo", self.win)
        self.bFitUndo.setToolTip("Undo the last action")
        self.bFitUndo.setCheckable(False)
        self.edit_toolbar.addAction(self.bFitUndo)
        self.bFitEditZPK = QtWidgets.QAction("Edit ZPK", self.win)
        self.bFitEditZPK.setToolTip("Open a dialog that allows you to change the value of all poles and zeros and gain")
        self.bFitEditZPK.setCheckable(False)
        self.edit_toolbar.addAction(self.bFitEditZPK)
        # make sure only one among the buttuns below can be active at any time
        self.edit_exclusive_group = QtWidgets.QActionGroup(self.win)
        self.edit_exclusive_group.setExclusionPolicy(QtWidgets.QActionGroup.ExclusionPolicy.ExclusiveOptional)
        self.edit_exclusive_group.addAction(self.bFitAddCmpxPole)
        self.edit_exclusive_group.addAction(self.bFitAddRealPole)
        self.edit_exclusive_group.addAction(self.bFitAddCmpxZero)
        self.edit_exclusive_group.addAction(self.bFitAddRealZero)
        self.edit_exclusive_group.addAction(self.bFitDelete)
        self.edit_exclusive_group.addAction(self.bFitMovePZ)
        self.edit_exclusive_group.addAction(self.bFitChangeGain)
        self.edit_exclusive_group.addAction(self.bCrosshair)

        # menu for additional plots
        self.plot_button = QtWidgets.QToolButton()
        self.plot_button.setText("More plots")
        self.plot_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.plot_button.setToolTip("Open a menu to manage additional plots")
        self.edit_toolbar.addWidget(self.plot_button)
        self.plot_menu = QtWidgets.QMenu()
        self.plot_menu_ctrl = QtWidgets.QAction('Controller bode', checkable=True)
        self.plot_menu.addAction(self.plot_menu_ctrl)
        self.plot_menu_plant = QtWidgets.QAction('Plant bode', checkable=True)
        self.plot_menu.addAction(self.plot_menu_plant)
        self.plot_menu.addSeparator()
        self.plot_menu_impulse = QtWidgets.QAction('Closed-loop impulse response', checkable=True)
        self.plot_menu.addAction(self.plot_menu_impulse)
        self.plot_menu_step = QtWidgets.QAction('Closed-loop step response', checkable=True)
        self.plot_menu.addAction(self.plot_menu_step)
        self.plot_menu.addSeparator()
        self.plot_menu_nyquist = QtWidgets.QAction('Nyquist plot', checkable=True)
        self.plot_menu.addAction(self.plot_menu_nyquist)
        self.plot_menu_nichols = QtWidgets.QAction('Nichols plot', checkable=True)
        self.plot_menu.addAction(self.plot_menu_nichols)
        self.plot_menu_root = QtWidgets.QAction('Root locus plot', checkable=True)
        self.plot_menu.addAction(self.plot_menu_root)
        # connect methods for the additional plot menu
        self.plot_button.setMenu(self.plot_menu)
        self.plot_menu_ctrl.triggered.connect(self.plot_ctrl)
        self.plot_menu_plant.triggered.connect(self.plot_plant)
        self.plot_menu_impulse.triggered.connect(self.plot_impulse)
        self.plot_menu_step.triggered.connect(self.plot_step)
        self.plot_menu_nyquist.triggered.connect(self.plot_nyquist)
        self.plot_menu_nichols.triggered.connect(self.plot_nichols)
        self.plot_menu_root.triggered.connect(self.plot_root)

        self.plot_menu_ctrl.toggled.connect(self.plot_menu_toggle)
        self.plot_menu_plant.toggled.connect(self.plot_menu_toggle)
        self.plot_menu_impulse.toggled.connect(self.plot_menu_toggle)
        self.plot_menu_step.toggled.connect(self.plot_menu_toggle)
        self.plot_menu_nyquist.toggled.connect(self.plot_menu_toggle)
        self.plot_menu_nichols.toggled.connect(self.plot_menu_toggle)
        self.plot_menu_root.toggled.connect(self.plot_menu_toggle)

        # variables to contain new plot windows and lines
        self.plot_windows = {}
        self.plot_lines = {}

        # menu for references
        self.ref_button = QtWidgets.QToolButton()
        self.ref_button.setText("Refs")
        self.ref_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.ref_button.setToolTip("Open a menu to manage reference traces. If you select 'Add reference' a new "
                                   "trace is added that will keep the shape of the current controller. You can add "
                                   "multiple traces: \nthey will be listed at the bottom of this menu; you can deselect "
                                   " any of them to hide the corresponding trace. If you select 'Delete selected "
                                   "reference(s)' then all selected reference traces will be deleted. \nIf you select "
                                   "'Revert to first selected reference' then the current fit is replaced by the first "
                                   "selected reference")
        self.edit_toolbar.addWidget(self.ref_button)
        self.ref_menu = QtWidgets.QMenu()
        self.ref_menu_add = QtWidgets.QAction('Add reference')
        self.ref_menu.addAction(self.ref_menu_add)
        self.ref_menu_delete = QtWidgets.QAction('Delete selected reference(s)')
        self.ref_menu.addAction(self.ref_menu_delete)
        self.ref_menu_revert = QtWidgets.QAction('Revert to first selected reference')
        self.ref_menu.addAction(self.ref_menu_revert)
        self.ref_menu.addSeparator()
        self.reference_items = []
        self.ref_button.setMenu(self.ref_menu)
        self.ref_menu_add.triggered.connect(self.add_reference)
        self.ref_menu_delete.triggered.connect(self.delete_references)
        self.ref_menu_revert.triggered.connect(self.revert_reference)

        # more toolbar buttons
        self.bFitSave = QtWidgets.QAction("Save sess", self.win)
        self.bFitSave.setToolTip("Save the current session to a file")
        self.bFitSave.setCheckable(False)
        self.edit_toolbar.addAction(self.bFitSave)

        self.bFitLoad = QtWidgets.QAction("Load sess", self.win)
        self.bFitLoad.setToolTip("Load a saved session from file")
        self.bFitLoad.setCheckable(False)
        self.edit_toolbar.addAction(self.bFitLoad)

        self.bExportController = QtWidgets.QAction("Export ctrl", self.win)
        self.edit_toolbar.addAction(self.bExportController)
        self.bExportController.setToolTip("Open a window showing the foton design string and the ZPK list"
                                          " for the current controller")

        self.bImportController = QtWidgets.QAction("Import ctrl", self.win)
        self.edit_toolbar.addAction(self.bImportController)
        self.bImportController.setToolTip("Open a window to load a controller from LIGO foton definition files"
                                          " and data from a given GPS time")

        self.bPrintPlant = QtWidgets.QAction("Plant zpk", self.win)
        self.bPrintPlant.setToolTip("Print to terminal a z,p,k representation of the current plant, as well "
                                  "as the list of poles and zeros. Q=0 means real pole or zero")
        self.edit_toolbar.addAction(self.bPrintPlant)

        self.bOptimizer = QtWidgets.QAction("Optimizer", self.win)
        self.bOptimizer.setToolTip("Open a new window with an experimental loop optimizer.")
        self.edit_toolbar.addAction(self.bOptimizer)

        self.bTutorial = QtWidgets.QAction("Tutorial", self.win)
        self.bTutorial.setToolTip("Open the TUTORIAL.md file in a new window")
        self.edit_toolbar.addAction(self.bTutorial)
        self.bTutorial.triggered.connect(self.open_tutorial)

        # add event handlers for toggles and clicks
        self.bFitAddCmpxPole.toggled.connect(self.curve_edit_toggle)
        self.bFitAddRealPole.toggled.connect(self.curve_edit_toggle)
        self.bFitAddCmpxZero.toggled.connect(self.curve_edit_toggle)
        self.bFitAddRealZero.toggled.connect(self.curve_edit_toggle)
        self.bFitAdd.triggered.connect(self.add_other)
        self.bFitChangeGain.toggled.connect(self.curve_edit_toggle)
        self.bFitLockGain.toggled.connect(self.curve_lockgain_toggle)
        self.bFitDelete.toggled.connect(self.curve_edit_toggle)
        self.bFitUndo.triggered.connect(self.undo_change)
        self.bFitEditZPK.triggered.connect(self.edit_zpk)
        self.bFitMovePZ.toggled.connect(self.toggle_move_zp)
        self.bFitChangeGain.toggled.connect(self.toggle_gain)
        self.bFitSave.triggered.connect(self.save)
        self.bFitLoad.triggered.connect(self.load_helper)
        self.bExportController.triggered.connect(self.export_ctrl)
        self.bImportController.triggered.connect(self.import_ctrl)
        self.bPrintPlant.triggered.connect(self.print_plant)
        self.bOptimizer.triggered.connect(self.optimizer)

        # Add status bar at the bottom of the window
        self.status_bar = QtWidgets.QStatusBar()
        self.win.setStatusBar(self.status_bar)

        # add stability text to status bar
        self.stability = QtWidgets.QLabel('Stable loop')
        self.stability.setStyleSheet("font-weight: bold; color : green")
        self.status_bar.insertPermanentWidget(0, self.stability)

        # add some more widgets to the status bar (to allow user to change keyboard increments)
        self.status_bar_flabel = QtWidgets.QLabel('Frequency increment (fraction of range)')
        self.status_bar_finput = QtWidgets.QLineEdit()
        self.status_bar_finput.setText('%.3f' % self.frequency_increment)
        self.status_bar_finput.setFixedWidth(70)
        self.status_bar_qlabel = QtWidgets.QLabel('Q increment (relative)')
        self.status_bar_qinput = QtWidgets.QLineEdit()
        self.status_bar_qinput.setText('%.3f' % self.q_increment)
        self.status_bar_qinput.setFixedWidth(70)
        self.status_bar_gainlabel = QtWidgets.QLabel('Gain increment (db)')
        self.status_bar_gaininput = QtWidgets.QLineEdit()
        self.status_bar_gaininput.setText('%.3f' % self.gain_increment)
        self.status_bar_gaininput.setFixedWidth(70)
        self.status_bar.insertPermanentWidget(0, self.status_bar_finput)
        self.status_bar.insertPermanentWidget(0, self.status_bar_flabel)
        self.status_bar.insertPermanentWidget(0, self.status_bar_qinput)
        self.status_bar.insertPermanentWidget(0, self.status_bar_qlabel)
        self.status_bar.insertPermanentWidget(0, self.status_bar_gaininput)
        self.status_bar.insertPermanentWidget(0, self.status_bar_gainlabel)
        self.status_bar_finput.hide()
        self.status_bar_flabel.hide()
        self.status_bar_qinput.hide()
        self.status_bar_qlabel.hide()
        self.status_bar_gaininput.hide()
        self.status_bar_gainlabel.hide()
        self.status_bar_finput.editingFinished.connect(self.finput_textChanged)
        self.status_bar_qinput.editingFinished.connect(self.qinput_textChanged)
        self.status_bar_gaininput.editingFinished.connect(self.gaininput_textChanged)

        # plots in the main window
        self.plot_abs = pg.PlotWidget()
        self.plot_phi = pg.PlotWidget()
        self.plot_clg1 = pg.PlotWidget()
        self.plot_clg2 = pg.PlotWidget()
        self.plot_abs.disableAutoRange()
        self.plot_phi.disableAutoRange()
        self.plot_clg1.disableAutoRange()
        self.plot_clg2.disableAutoRange()

        # main plot area using pyqtgraph DockArea to allow resizing
        self.area = DockArea()
        self.win.setCentralWidget(self.area)
        dock_abs = Dock('Magnitude')
        dock_phi = Dock("Phase")
        dock_clg1 = Dock("1/(1+OLG)")
        dock_clg2 = Dock("OLG/(1+OLG)")
        dock_abs.hideTitleBar()
        dock_phi.hideTitleBar()
        dock_clg1.hideTitleBar()
        dock_clg2.hideTitleBar()
        self.area.addDock(dock_abs, 'left')
        self.area.addDock(dock_clg1, 'right')
        self.area.addDock(dock_phi, 'bottom', dock_abs)
        self.area.addDock(dock_clg2, 'bottom', dock_clg1)
        # show grids
        self.plot_abs.showGrid(x=True, y=True, alpha=1)
        self.plot_phi.showGrid(x=True, y=True, alpha=1)
        self.plot_clg1.showGrid(x=True, y=True, alpha=1)
        self.plot_clg2.showGrid(x=True, y=True, alpha=1)
        # link all x axis together
        self.plot_phi.setXLink(self.plot_abs)
        self.plot_clg1.setXLink(self.plot_abs)
        self.plot_clg2.setXLink(self.plot_clg1)
        # set titles and labels
        self.plot_abs.setTitle('Transfer function [magnitude]')
        self.plot_phi.setTitle('Transfer function [phase]')
        self.plot_clg1.setTitle('1/(1+OLG)')
        self.plot_clg2.setTitle('OLG/(1+OLG)')
        self.plot_abs.setLabel('left', "Magnitude", units='db')
        self.plot_abs.setLabel('bottom', "Frequency", units='Hz')
        self.plot_phi.setLabel('left', "Phase", units='deg')
        self.plot_phi.setLabel('bottom', "Frequency", units='Hz')       
        self.plot_clg1.setLabel('left', "Magnitude", units='db')
        self.plot_clg1.setLabel('bottom', "Frequency", units='Hz')
        self.plot_clg2.setLabel('left', "Magnitude", units='db')
        self.plot_clg2.setLabel('bottom', "Frequency", units='Hz')
        # set log mode and avoid SI unit prefix (m, k, M, etc)
        self.plot_abs.setLogMode(x=True, y=False)
        self.plot_phi.setLogMode(x=True, y=False)
        self.plot_clg1.setLogMode(x=True, y=False)
        self.plot_clg2.setLogMode(x=True, y=False)
        self.plot_abs.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_phi.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_clg1.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_clg2.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_abs.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.plot_phi.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.plot_clg1.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.plot_clg2.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        # add plots to dock areas
        dock_abs.addWidget(self.plot_abs)
        dock_phi.addWidget(self.plot_phi)
        dock_clg1.addWidget(self.plot_clg1)
        dock_clg2.addWidget(self.plot_clg2)
        # connect zoom signal to resample of frequency bins
        self.plot_abs.sigXRangeChanged.connect(self.zoom)

        # horizontal lines at +-180 degrees and gain = 1
        self.phi_hLine1 = pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen('k', width=2))
        self.plot_phi.addItem(self.phi_hLine1)
        self.phi_hLine1.setPos(-180.0)
        self.phi_hLine2 = pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen('k', width=2))
        self.plot_phi.addItem(self.phi_hLine2)
        self.phi_hLine2.setPos(180.0)
        self.abs_hLine = pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen('k', width=2))
        self.plot_abs.addItem(self.abs_hLine)
        self.abs_hLine.setPos(0.0)

        # infrastructure to handle crosshairs
        class log_convert():
            # This is a trick to convert logscale x values to frequency. The 'label' argument of InfiniteLine
            # expects something with a .format() method. Normally it's a string, but here we create a class to
            # hack in a conversion
            def format(self, value=0):
                return "%.2f" % (10**value)
        logc = log_convert()
        # add a vertical crosshair and an horizontal crosshair for each plot. This is for the main traces,
        # references are handled below
        self.vLinea = pg.InfiniteLine(angle=90, movable=False, pen=pg.mkPen('k', width=0.5),
                                      label=logc,
                                      labelOpts={'position': 0.1, 'color': 'k', 'movable': True}
                                      )
        self.hLinea = pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen('r', width=0.5),
                                      label = '{value:0.2f}',
                                      labelOpts = {'position': 0.1, 'color': 'r', 'movable': True}
                                     )
        self.vLinep = pg.InfiniteLine(angle=90, movable=False, pen=pg.mkPen('k', width=0.5),
                                      label=logc,
                                      labelOpts={'position': 0.1, 'color': 'k', 'movable': True}
                                      )
        self.hLinep = pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen('r', width=0.5),
                                      label = '{value:0.1f}',
                                      labelOpts = {'position': 0.1, 'color': 'r', 'movable': True}
                                     )
        self.vLine1 = pg.InfiniteLine(angle=90, movable=False, pen=pg.mkPen('k', width=0.5),
                                      label=logc,
                                      labelOpts={'position': 0.1, 'color': 'k', 'movable': True}
                                      )
        self.hLine1 = pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen('r', width=0.5),
                                      label = '{value:0.2f}',
                                      labelOpts = {'position': 0.1, 'color': 'r', 'movable': True}
                                     )
        self.vLine2 = pg.InfiniteLine(angle=90, movable=False, pen=pg.mkPen('k', width=0.5),
                                      label=logc,
                                      labelOpts={'position': 0.1, 'color': 'k', 'movable': True}
                                      )
        self.hLine2 = pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen('r', width=0.5),
                                      label = '{value:0.2f}',
                                      labelOpts = {'position': 0.1, 'color': 'r', 'movable': True}
                                     )
        # hide them all by default
        self.vLinea.setVisible(False)
        self.hLinea.setVisible(False)
        self.vLinep.setVisible(False)
        self.hLinep.setVisible(False)
        self.vLine1.setVisible(False)
        self.hLine1.setVisible(False)
        self.vLine2.setVisible(False)
        self.hLine2.setVisible(False)
        # add them to plots
        self.plot_abs.addItem(self.vLinea, ignoreBounds=True)
        self.plot_abs.addItem(self.hLinea, ignoreBounds=True)
        self.plot_phi.addItem(self.vLinep, ignoreBounds=True)
        self.plot_phi.addItem(self.hLinep, ignoreBounds=True)
        self.plot_clg1.addItem(self.vLine1, ignoreBounds=True)
        self.plot_clg1.addItem(self.hLine1, ignoreBounds=True)
        self.plot_clg2.addItem(self.vLine2, ignoreBounds=True)
        self.plot_clg2.addItem(self.hLine2, ignoreBounds=True)
        # connect methods to move the crosshairs to follow the mouse cursor
        self.proxy1 = pg.SignalProxy(self.plot_abs.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
        self.proxy2 = pg.SignalProxy(self.plot_phi.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
        self.proxy3 = pg.SignalProxy(self.plot_clg1.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
        self.proxy4 = pg.SignalProxy(self.plot_clg2.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)

        # additional horizontal lines for reference traces, added in the methods that manage references
        self.ref_hLinea = []
        self.ref_hLinep = []
        self.ref_hLine1 = []
        self.ref_hLine2 = []

        #### Start app and GUI

        # if we need to load a session
        if load_session:
            fileName, ok = QtWidgets.QFileDialog.getOpenFileName(self.win, "Open saved session", "",
                                                                "All Files (*)")
            if ok:
                self.load(fileName)
            else:
                return


        # add references if they are provided as a parameter
        if references is not None:
            # loop over all references: key is name, value is the controller
            for k in references.keys():
                self.add_reference(True, [k, references[k]])   # True as first parameter is for compatibility with
                                                                        # call when button is pressed

        # plot all traces
        self.plot()
        # add graph with movable poles and zeros
        self.tf_graph = TranferFunctionGraph(self.ctrl, self.plant, self)
        self.plot_abs.addItem(self.tf_graph)
 
        # start Qt app full screen and start main Qt thread
        self.win.showMaximized()
        pg.exec()

    def open_tutorial(self):
        # Create a new window with grid layout
        q = QtWidgets.QWidget()
        q.resize(1000, 800)
        g = QtWidgets.QGridLayout()
        q.setLayout(g)
        # central widget text box
        tutorial = QtWidgets.QTextEdit()
        g.addWidget(tutorial, 0, 0)
        # open file
        import pathlib
        filename = str(pathlib.Path(__file__).parent.resolve()) + '/TUTORIAL.md'
        md = open(filename, 'r').read()
        # substitute pointers to images
        md = md.replace('imgs/', str(pathlib.Path(filename).parent.resolve()) +'/pics/')
        tutorial.setMarkdown(md)

        # open the window and wait for it to close
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()

    def initial_frequency_bins(self, nfr=1000, factor=3):
        """

        Parameters
        ----------
        nfr: int
            number of frequency bins
        factor: float
            enlarge the frequency span by this factor below the lowest pole or
            zero and above the largest pole or zero

        Returns
        -------
        fr: numpy.ndarray
            frequency bins

        """

        # extract all pole and zero frequencies
        freqs = np.concatenate([self.plant.z_f, self.plant.p_f, self.ctrl.z_f, self.ctrl.p_f])
        # remove any frequency equal to zero
        freqs = np.abs(freqs)[freqs != 0]
        if len(freqs):
            # log spacing between the minimum and maximum, with enlarging factor
            fr = np.geomspace(freqs.min() / factor, freqs.max() * factor, nfr)
        else:
            # if there are no poles or zeros, just pick something
            fr = np.geomspace(0.1, 10, nfr)
        return fr

    #### functions to assess stability and margins
    def closed_loop_poles(self, plant=None, ctrl=None):
        """
        Compute the close loop poles of the current feedback control system

        Parameters
        ----------
        plant: TF
            if passed, overrides object plant
        ctrl: TF
            if passed, overrides object controller

        Returns
        -------
        poles: numpy.ndarray
            complex poles in s-domain
        """

        if plant is None:
            plant = self.plant
        if ctrl is None:
            ctrl = self.ctrl

        # closed loop response is
        #
        # 1/(1+OLG) = plant.den * ctrl.den / (plant.den * ctrl.den + plan.num * ctrl.num)
        #
        # compute the denominator of the closed loop response
        den = np.polyadd(np.polymul(plant.den, ctrl.den), np.polymul(plant.num, ctrl.num))
        # return poles (roots of denominators)
        return np.roots(den)

    def isStable(self, plant=None, ctrl=None):
        """
        Asses the closed loop stability

        Parameters
        ----------
        plant: TF
            if passed, overrides object plant
        ctrl: TF
            if passed, overrides object controller

        Returns
        -------
        stability: bool
            True if stable, False is unstable
        poles: numpy.ndarray
            array of closed loop poles
        """

        # compute closed loop poles
        poles = self.closed_loop_poles(plant, ctrl)
        # stability criterion: Re(poles) < 0
        return np.all(np.real(poles) < 0), poles

    def showStability(self, plant=None, ctrl=None):
        """
        Update the status bar with stability info

        Parameters
        ----------
        plant: TF
            if passed, overrides object plant
        ctrl: TF
            if passed, overrides object controller
        """

        # check stability
        stability, poles = self.isStable(plant, ctrl)
        if stability:
            # stable loop
            self.stability.setStyleSheet("font-weight: bold; color : green")

            # get and show stability margins and closed-loop overshoot
            gain_margins, phase_margin, overshoot = self.margins(plant, ctrl)
            msg = 'gain margins: -%.0f db, +%.0f db | phase margin: %.0f deg | overshoot: %.1f db' % (
                                                                                        gain_margins[0][1],
                                                                                        -gain_margins[1][1],
                                                                                        phase_margin[1],
                                                                                        overshoot)
            self.stability.setText('Stable loop | ' + msg)

            # plot stability margins
            self.plot_abs.disableAutoRange(pg.ViewBox.XAxis)
            self.plot_phi.disableAutoRange(pg.ViewBox.XAxis)
            if self.gain_margin1 is None:
                # first time, add new lines
                self.gain_margin1 = self.plot_abs.plot(2 * [gain_margins[0][0]], [0, gain_margins[0][1]],
                                                       pen=pg.mkPen('g', width=3))
                self.gain_margin2 = self.plot_abs.plot(2 * [gain_margins[1][0]], [gain_margins[1][1], 0],
                                                       pen=pg.mkPen('g', width=3))
                self.phase_margin = self.plot_phi.plot(2 * [phase_margin[0]], [-180, phase_margin[1] - 180],
                                                       pen=pg.mkPen('g', width=3))
            else:
                # update existing lines
                self.gain_margin1.setData(2 * [gain_margins[0][0]], [0, gain_margins[0][1]])
                self.gain_margin2.setData(2 * [gain_margins[1][0]], [gain_margins[1][1], 0])
                self.phase_margin.setData(2 * [phase_margin[0]], [-180, phase_margin[1] - 180])

        else:
            # unstable loop
            self.stability.setStyleSheet("font-weight: bold; color : red")
            self.stability.setText('Unstable loop')
            # remove margin lines
            if self.gain_margin1 is not None:
                self.plot_abs.removeItem((self.gain_margin1))
                self.plot_abs.removeItem((self.gain_margin2))
                self.plot_phi.removeItem((self.phase_margin))
                self.gain_margin1 = None
                self.gain_margin2 = None
                self.phase_margin = None

    def margins(self, plant=None, ctrl=None):
        """
        Compute gain and phase margins and overshoot

        Parameters
        ----------
        plant: TF
            if passed, overrides object plant
        ctrl: TF
            if passed, overrides object controller

        Returns
        -------
        gain_margin: list of (float, float)
            list of (frequency, gain margin [deg])
        phase_margin: list of (float, float)
            frequency, phase margin [deg]
        overshoot: float
            closed-loop gain overshoot
        """

        # use provided plant and controller if any
        if plant is None:
            plant = self.plant
        if ctrl is None:
            ctrl = self.ctrl

        # compute open-loop gain
        olg = plant.fresp(self.fr_stab) * ctrl.fresp(self.fr_stab)

        # find frequencies where gain is unity
        gain = abs(olg)
        g = gain - 1
        phase = 180/np.pi*np.angle(olg)
        idx = np.where(g[:-1]*g[1:] < 0)[0]   # zero crossings of abs(olg) - 1
        # compute phase for each of them
        phase_margins = []
        for i in idx:
            phase_margins.append([0.5*(self.fr_stab[i]+self.fr_stab[i+1]), 0.5*(phase[i]+phase[i+1])])

        # find frequencies where phase crosses +-180 (by looking at zero crossing of the phase of -olg
        mphase = np.angle(-olg)
        idx = np.where( (mphase[:-1] * mphase[1:] < 0)*(abs(mphase[:-1] - mphase[1:]) < np.pi)) [0]
        # compute gain for each phase crossing
        gain_margins = []
        for i in idx:
            gain_margins.append([0.5 * (self.fr_stab[i] + self.fr_stab[i + 1]),
                                 20*np.log10(0.5 * (gain[i] + gain[i + 1]))])
        # find the smallest positive and negative margins
        gm_pos = [g for g in gain_margins if g[1] > 0]
        if len(gm_pos):
            i_pos = np.argmin([g[1] for g in gm_pos])
            gm_pos = gm_pos[i_pos]
        else:
            gm_pos = [0, np.Inf]
        gm_neg = [g for g in gain_margins if g[1] < 0]
        if len(gm_neg):
            i_neg = np.argmax([g[1] for g in gm_neg])
            gm_neg = gm_neg[i_neg]
        else:
            gm_neg = [np.Inf, -np.Inf]
        # return frequency and margin in db
        gain_margins = [gm_pos, gm_neg]

        # find the smallest phase margin. Returns frequency and margin in deg
        if len(phase_margins):
            pm = np.argmin([p[1]+180 for p in phase_margins])
            phase_margin = [phase_margins[pm][0], phase_margins[pm][1]+180]
        else:
            phase_margin = [0, np.Inf]

        # compute overshoot as maximum of 1/(1+OLG) in db
        overshoot = 20*np.log10(abs((1/(1+olg))).max())

        # return results
        return gain_margins, phase_margin, overshoot

    def impulse_response(self, plant=None, ctrl=None, npoints=1000, t=None):
        """
        Compute the closed-loop impulse response of the system

        Parameters
        ----------
        plant: TF
            if passed, overrides object plant
        ctrl: TF
            if passed, overrides object controller
        npoints: int
            number of samples in the impulse response
        t: numpy.ndarray
            if passed, use this vector of time samples

        Returns
        -------
        t: numpy.ndarray
            time vector
        y: numpy.ndarray
            impulse response sampled at the times in t
        """

        if plant is None:
            plant = self.plant
        if ctrl is None:
            ctrl = self.ctrl

        # compute numerator and denominator of 1/(1+OLG)
        num = np.polymul(ctrl.den, plant.den)
        den = np.polyadd(np.polymul(ctrl.den, plant.den), np.polymul(ctrl.num, plant.num))
        # compute impulse response
        t, y = scipy.signal.impulse((num, den), N=npoints, T=t)

        return t, y

    def step_response(self, plant=None, ctrl=None, npoints=1000, t=None):
        """
        Compute the closed-loop step response of the system

        Parameters
        ----------
        plant: TF
            if passed, overrides object plant
        ctrl: TF
            if passed, overrides object controller
        npoints: int
            number of samples in the impulse response
        t: numpy.ndarray
            if passed, use this vector of time samples

        Returns
        -------
        t: numpy.ndarray
            time vector
        y: numpy.ndarray
            impulse response sampled at the times in t
        """

        if plant is None:
            plant = self.plant
        if ctrl is None:
            ctrl = self.ctrl

        # compute numerator and denominator of 1/(1+OLG)
        num = np.polymul(ctrl.den, plant.den)
        den = np.polyadd(np.polymul(ctrl.den, plant.den), np.polymul(ctrl.num, plant.num))
        # compute impulse response
        t, y = scipy.signal.step((num, den), N=npoints, T=t)

        return t, y


    # save and load session
    def save(self):
        """
        Save the current status to a file, after opening a dialog to select the filename
        """

        # make a dictionary of stuff to save
        tosave = {
            'fr': self.fr,
            'fr_stab': self.fr_stab,
            'plant': self.plant,
            'ctrl':self.ctrl,
            'reference_ctrl': self.reference_ctrl,
            'reference_names': [r.text() for r in self.reference_items],
            'reference_visible': [r.isChecked() for r in self.reference_items],
            'reference_color': self.reference_color,
            'lock_gain_freq': self.lock_gain_freq,
            'lock_gain_value': self.lock_gain_value,
            'frequency_increment': self.frequency_increment,
            'q_increment': self.q_increment,
            'gain_increment': self.gain_increment,
            'layout': self.area.saveState()
        }

        # additional plots: True if they are shown
        tosave['additional_plots'] = {'ctrl': self.plot_menu_ctrl.isChecked(),
                                      'plant': self.plot_menu_plant.isChecked(),
                                      'impulse': self.plot_menu_impulse.isChecked(),
                                      'step': self.plot_menu_step.isChecked(),
                                      'nyquist': self.plot_menu_nyquist.isChecked(),
                                      'nichols': self.plot_menu_nichols.isChecked(),
                                      'root': self.plot_menu_root.isChecked()}

        # plot ranges
        tosave['ranges'] = {'abs': self.plot_abs.getViewBox().viewRange(),
                            'phi': self.plot_phi.getViewBox().viewRange(),
                            'clg1': self.plot_clg1.getViewBox().viewRange(),
                            'clg2': self.plot_clg2.getViewBox().viewRange()}
        # plot ranges of additional plots, if visible
        if self.plot_menu_ctrl.isChecked():
            tosave['ranges']['ctrl'] = self.plot_lines['ctrl'][2].getViewBox().viewRange()
        if self.plot_menu_plant.isChecked():
            tosave['ranges']['plant'] = self.plot_lines['plant'][2].getViewBox().viewRange()
        if self.plot_menu_impulse.isChecked():
            tosave['ranges']['impulse'] = self.plot_lines['impulse'][1].getViewBox().viewRange()
        if self.plot_menu_step.isChecked():
            tosave['ranges']['step'] = self.plot_lines['step'][1].getViewBox().viewRange()
        if self.plot_menu_nyquist.isChecked():
            tosave['ranges']['nyquist'] = self.plot_lines['nyquist'][1].getViewBox().viewRange()
        if self.plot_menu_nichols.isChecked():
            tosave['ranges']['michols'] = self.plot_lines['michols'][1].getViewBox().viewRange()
        if self.plot_menu_root.isChecked():
            tosave['ranges']['root'] = self.plot_lines['root'][1].getViewBox().viewRange()

        # ask filename
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(self.win, "Save session to file", "", "All Files (*)")
        # save to file
        pickle.dump(tosave, open(filename, 'wb'))

    def load_helper(self):
        """
        Open a dialog to read saved session, called by toolbar button
        """
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(self.win, "Open saved session", "", "All Files (*)")
        self.load(filename)

    def load(self, filename):
        """
        Load status from file and update

        Parameters
        filename: str
            name of the file where the current status will be saved
        """

        loaded = pickle.load(open(filename, 'rb'))
        self.fr = loaded['fr']
        self.fr_stab = loaded['fr_stab']
        self.plant = loaded['plant']
        self.ctrl = loaded['ctrl']
        self.reference_ctrl = loaded['reference_ctrl']
        reference_names = loaded['reference_names']
        reference_visible = loaded['reference_visible']
        if 'reference_color' in loaded.keys():
            self.reference_color = loaded['reference_color']
        else:
            self.reference_color = np.arange(1, len(reference_names)+1)
        self.lock_gain_freq = loaded['lock_gain_freq']
        self.lock_gain_value = loaded['lock_gain_value']
        self.frequency_increment = loaded['frequency_increment']
        self.q_increment = loaded['q_increment']
        self.gain_increment = loaded['gain_increment']
        self.area.restoreState(loaded['layout'])

        # add reference menu items
        for i in self.reference_items:
            self.ref_menu.removeAction(i)
        self.reference_items = []
        for n,r,v in zip(reference_names, self.reference_ctrl, reference_visible):
            self.reference_items.append(QtWidgets.QAction(n, checkable=True))
            self.reference_items[-1].setChecked(v)
            self.reference_items[-1].toggled.connect(self.plot)
            self.ref_menu.addAction(self.reference_items[-1])

        # update all plots
        self.plot_abs.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_phi.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_clg1.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_clg2.disableAutoRange(pg.ViewBox.XAxis)
        #self.tf_graph = TranferFunctionGraph(self.ctrl, self.plant, self)
        #self.plot_abs.addItem(self.tf_graph)
        self.plot()

        # set ranges
        self.plot_abs.setXRange(loaded['ranges']['abs'][0][0],
                                loaded['ranges']['abs'][0][1])
        self.plot_abs.setYRange(loaded['ranges']['abs'][1][0],
                                loaded['ranges']['abs'][1][1])
        self.plot_phi.setYRange(loaded['ranges']['phi'][1][0],
                                loaded['ranges']['phi'][1][1])
        self.plot_clg1.setYRange(loaded['ranges']['clg1'][1][0],
                                 loaded['ranges']['clg1'][1][1])
        self.plot_clg2.setYRange(loaded['ranges']['clg2'][1][0],
                                 loaded['ranges']['clg2'][1][1])
        # create additional plots
        if loaded['additional_plots']['ctrl']:
            self.plot_ctrl()
            self.plot_lines['ctrl'][2].setXRange(loaded['ranges']['ctrl'][0][0],
                                                 loaded['ranges']['ctrl'][0][1])
            self.plot_lines['ctrl'][2].setYRange(loaded['ranges']['ctrl'][1][0],
                                                 loaded['ranges']['ctrl'][1][1])
        if loaded['additional_plots']['plant']:
            self.plot_plant()
            self.plot_lines['plant'][2].setXRange(loaded['ranges']['plant'][0][0],
                                                  loaded['ranges']['plant'][0][1])
            self.plot_lines['plant'][2].setYRange(loaded['ranges']['plant'][1][0],
                                                  loaded['ranges']['plant'][1][1])
        if loaded['additional_plots']['impulse']:
            self.plot_impulse()
            self.plot_lines['impulse'][1].setXRange(loaded['ranges']['impulse'][0][0],
                                                    loaded['ranges']['impulse'][0][1])
            self.plot_lines['impulse'][1].setYRange(loaded['ranges']['impulse'][1][0],
                                                    loaded['ranges']['impulse'][1][1])
        if loaded['additional_plots']['step']:
            self.plot_step()
            self.plot_lines['step'][1].setXRange(loaded['ranges']['step'][0][0],
                                                 loaded['ranges']['step'][0][1])
            self.plot_lines['step'][1].setYRange(loaded['ranges']['step'][1][0],
                                                 loaded['ranges']['step'][1][1])
        if loaded['additional_plots']['nichols']:
            self.plot_nichols()
            self.plot_lines['nichols'][1].setXRange(loaded['ranges']['nichols'][0][0],
                                                    loaded['ranges']['nichols'][0][1])
            self.plot_lines['nichols'][1].setYRange(loaded['ranges']['nichols'][1][0],
                                                    loaded['ranges']['nichols'][1][1])
        if loaded['additional_plots']['nyquist']:
            self.plot_nyquist()
            self.plot_lines['nyquist'][1].setXRange(loaded['ranges']['nyquist'][0][0],
                                                    loaded['ranges']['nyquist'][0][1])
            self.plot_lines['nyquist'][1].setYRange(loaded['ranges']['nyquist'][1][0],
                                                    loaded['ranges']['nyquist'][1][1])
        if loaded['additional_plots']['root']:
            self.plot_root()
            self.plot_lines['root'][1].setXRange(loaded['ranges']['root'][0][0],
                                                 loaded['ranges']['root'][0][1])
            self.plot_lines['root'][1].setYRange(loaded['ranges']['root'][1][0],
                                                 loaded['ranges']['root'][1][1])

        self.plotted = True

    # plotting functions
    def plot(self):
        '''
        Replot transfer functions
        '''

        # get sampled version of current transfer functions
        ctrl = self.ctrl.fresp(self.fr)
        plant = self.plant.fresp(self.fr)
        # compute open loop gain (OLG)
        olg = ctrl * plant
        # plot OLG
        if self.line_olg_abs is None:
            # new plot line, using a custom class defined below, to handle mouse drag
            self.line_olg_abs = CustomPlotItem(self, self.fr, 20*np.log10(np.abs(olg)),
                                               pen=pg.mkPen('r', width=1))
            self.plot_abs.addItem(self.line_olg_abs)
            self.line_olg_phi = self.plot_phi.plot(self.fr, 180/np.pi*np.angle(olg),
                                                   pen=pg.mkPen('r', width=1))
        else:
            # existing plot line, just update data
            self.line_olg_abs.setData(self.fr, 20*np.log10(np.abs(olg)))
            self.line_olg_phi.setData(self.fr, 180/np.pi*np.angle(olg))

        # connect methods to handle clicks on the OLG fit line
        self.proxy_line_click = pg.SignalProxy(self.line_olg_abs.sigClicked,
                                               rateLimit=10, slot=self.click_abs_curve)
        # curve is clickable only if the proper buttons are selected
        self.line_olg_abs.curve.setClickable(self.bFitAddCmpxPole.isChecked() or
                                             self.bFitAddRealPole.isChecked() or
                                             self.bFitAddCmpxZero.isChecked() or
                                             self.bFitAddRealZero.isChecked())

        # plot markers for plant poles and zeros (not editable)
        if self.plant_graph is None:
            # create new graph element if needed
            self.plant_graph = pg.GraphItem()
            self.plot_abs.addItem(self.plant_graph)
        # compute marker positions and symbols
        frequencies = np.r_[self.plant.p_f, self.plant.z_f]
        Q = np.r_[self.plant.p_Q, self.plant.z_Q]
        symbols = np.array(['star'] * self.plant.p_f.shape[0] + ['o'] * self.plant.z_f.shape[0])
        vals = np.abs(self.plant.fresp(frequencies) * self.ctrl.fresp(frequencies))
        pos = np.c_[np.log10(np.abs(frequencies)), 20*np.log10(vals)]
        npts = frequencies.shape[0]
        pens = []
        brushes = []
        # empty symbols for real, solid symbols for complex
        for f, q in zip(frequencies, Q):
            if q == 0:
                pens.append(pg.mkPen('k'))
                brushes.append(pg.mkBrush('w'))
            else:
                pens.append(pg.mkPen('k'))
                brushes.append(pg.mkBrush('k'))
        sizes = np.array(npts * [10])
        data = {'pos': pos,
                'symbol': symbols,
                'size': sizes,
                'data': np.empty(npts, dtype=[('index', int)]),
                'symbolPen': pens, 'symbolBrush': brushes
                }
        data['data']['index'] = np.arange(npts)
        self.plant_graph.setData(**data)

        # plot 1/(1-OLG)
        if self.line_clg1 is None:
            # new plot line
            self.line_clg1 = self.plot_clg1.plot(self.fr, 20*np.log10(np.abs(1/(1+olg))),
                                                 pen=pg.mkPen('r', width=1))
        else:
            # existing plot line, just update data
            self.line_clg1.setData(self.fr, 20*np.log10(np.abs(1/(1+olg))))

        # plot OLG/(1-OLG)
        if self.line_clg2 is None:
            # new plot line
            self.line_clg2 = self.plot_clg2.plot(self.fr, 20*np.log10(np.abs(olg/(1+olg))),
                                                 pen=pg.mkPen('r', width=1))
        else:
            # existing plot line, just update data
            self.line_clg2.setData(self.fr, 20*np.log10(np.abs(olg/(1+olg))))

        # plot reference fits
        if len(self.reference_ctrl):
            # remove all previous traces, if any
            for a,p,c1,c2 in zip(self.reference_plot_abs, self.reference_plot_phi,
                             self.reference_plot_clg1, self.reference_plot_clg2):
                self.plot_abs.removeItem(a)
                self.plot_phi.removeItem(p)
                self.plot_clg1.removeItem(c1)
                self.plot_clg2.removeItem(c2)
            self.reference_plot_abs = []
            self.reference_plot_phi = []
            self.reference_plot_clg1 = []
            self.reference_plot_clg2 = []
            # add again legend if needed
            if self.legend_abs is None:
                self.legend_abs = self.plot_abs.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.legend_phi = self.plot_phi.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.legend_clg1 = self.plot_clg1.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.legend_clg2 = self.plot_clg2.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
            # replot all active references
            for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                # recompute references
                ref = r.fresp(self.fr)
                plant = self.plant.fresp(self.fr)
                olg = ref * plant
                # plot them
                self.reference_plot_abs.append(self.plot_abs.plot(self.fr, 20*np.log10(np.abs(olg)),
                                                pen=c, name=q.text()))
                self.reference_plot_phi.append(self.plot_phi.plot(self.fr, 180/np.pi*np.angle(olg),
                                                pen=c, name=q.text()))
                self.reference_plot_clg1.append(self.plot_clg1.plot(self.fr, 20*np.log10(np.abs(1/(1+olg))),
                                                pen=c, name=q.text()))
                self.reference_plot_clg2.append(self.plot_clg2.plot(self.fr, 20*np.log10(np.abs(olg/(1+olg))),
                                                pen=c, name=q.text()))
                # make it visible only if checked
                if q.isChecked():
                    self.reference_plot_abs[-1].show()
                    self.reference_plot_phi[-1].show()
                    self.reference_plot_clg1[-1].show()
                    self.reference_plot_clg2[-1].show()
                else:
                    self.reference_plot_abs[-1].hide()
                    self.reference_plot_phi[-1].hide()
                    self.reference_plot_clg1[-1].hide()
                    self.reference_plot_clg2[-1].hide()

        # add a marker where the gain is locked
        if self.lock_gain_freq is not None:
            plant = np.abs(self.plant.fresp(self.lock_gain_freq))
            if self.lock_gain_dot is None:
                self.lock_gain_dot = self.plot_abs.plot([self.lock_gain_freq],
                                                        [20*np.log10(plant * self.lock_gain_value)],
                                                        symbol='d', symbolPen='k', symbolBrush='k',
                                                        symbolSize=10)
            else:
                self.lock_gain_dot.setData([self.lock_gain_freq],
                                           [20*np.log10(plant * self.lock_gain_value)])

        # mark all traces as plotted
        self.plotted = True

        # update additional plots if any
        if 'plant' in self.plot_windows.keys():
            self.plot_plant()
        if 'ctrl' in self.plot_windows.keys():
            self.plot_ctrl()
        if 'impulse' in self.plot_windows.keys():
            self.plot_impulse()
        if 'step' in self.plot_windows.keys():
            self.plot_step()
        if 'root' in self.plot_windows.keys():
            self.plot_root()
        if 'nichols' in self.plot_windows.keys():
            self.plot_nichols()
        if 'nyquist' in self.plot_windows.keys():
            self.plot_nyquist()

        # Update stability in status bar
        self.showStability()

    ### Functions for additional plots

    def plot_menu_toggle(self):
        """
        Close windows when menu items are unchecked
        """
        if 'plant' in self.plot_windows.keys():
            if not self.plot_menu_plant.isChecked():
                self.plot_windows['plant'].close()
        if 'ctrl' in self.plot_windows.keys():
            if not self.plot_menu_ctrl.isChecked():
                self.plot_windows['ctrl'].close()
        if 'impulse' in self.plot_windows.keys():
            if not self.plot_menu_impulse.isChecked():
                self.plot_windows['impulse'].close()
        if 'step' in self.plot_windows.keys():
            if not self.plot_menu_step.isChecked():
                self.plot_windows['step'].close()
        if 'nichols' in self.plot_windows.keys():
            if not self.plot_menu_nichols.isChecked():
                self.plot_windows['nichols'].close()
        if 'nyquist' in self.plot_windows.keys():
            if not self.plot_menu_nyquist.isChecked():
                self.plot_windows['nyquist'].close()
        if 'root' in self.plot_windows.keys():
            if not self.plot_menu_root.isChecked():
                self.plot_windows['root'].close()

    def close_all(self):
        """
        Close all windows
        """
        for k in self.plot_windows.keys():
            self.plot_windows[k].close()

    def plot_plant(self):
        """
        Plot the plant bode in a new window
        """

        # create a new window if not already existing
        if 'plant' not in self.plot_windows.keys():
            # create window
            self.plot_windows['plant'] = QtWidgets.QWidget()
            self.plot_windows['plant'].setWindowTitle('Plant Bode plot')
            self.plot_windows['plant'].setAttribute(QtCore.Qt.WA_DeleteOnClose)
            # remove window from dictionary when closed
            def plant_window_closed():
                self.plot_windows.pop('plant', 0)
                self.plot_menu_plant.setChecked(False)
            self.plot_windows['plant'].destroyed.connect(plant_window_closed)
            # add two plots (abs and phase)
            fig = QtWidgets.QGridLayout()
            self.plot_windows['plant'].setLayout(fig)
            plot_abs = pg.PlotWidget()
            plot_phi = pg.PlotWidget()
            fig.addWidget(plot_abs, 0, 0)
            fig.addWidget(plot_phi, 1, 0)
            # setup and link axes
            plot_phi.setXLink(plot_abs)
            # labels and title
            plot_abs.setTitle('Transfer function [magnitude]')
            plot_phi.setTitle('Transfer function [phase]')
            plot_abs.setLabel('left', "Magnitude", units='db')
            plot_abs.setLabel('bottom', "Frequency", units='Hz')
            plot_phi.setLabel('left', "Phase", units='deg')
            plot_phi.setLabel('bottom', "Frequency", units='Hz')
            plot_abs.setLogMode(x=True, y=False)
            plot_phi.setLogMode(x=True, y=False)
            plot_abs.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_phi.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_abs.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_phi.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_abs.showGrid(x=True, y=True, alpha=1)
            plot_phi.showGrid(x=True, y=True, alpha=1)
            # start using the same frequency bins as the main plots
            self.plot_lines['plant_fr'] = self.fr.copy()
            fr = self.plot_lines['plant_fr']
            # plot plant TF
            line_abs = plot_abs.plot(fr, 20*np.log10(np.abs(self.plant.fresp(fr))),
                                     pen=pg.mkPen('r', width=1))
            line_phi = plot_phi.plot(fr, 180/np.pi*np.angle(self.plant.fresp(fr)),
                                     pen=pg.mkPen('r', width=1))
            # save pointers to traces and plot areas
            self.plot_lines['plant'] = [line_abs, line_phi, plot_abs, plot_phi]
            # set the same x range as the main plots
            xrange = self.plot_abs.getViewBox().viewRange()[0]
            plot_abs.setXRange(xrange[0], xrange[1])

            # connect zoom function to resample frequency bins
            plot_abs.sigXRangeChanged.connect(self.plot_plant_zoom)

            # show window
            self.plot_windows['plant'].show()
        else:
            # window is already open, simply update data
            fr = self.plot_lines['plant_fr']
            self.plot_lines['plant'][0].setData(fr, 20*np.log10(np.abs(self.plant.fresp(fr))))
            self.plot_lines['plant'][1].setData(fr, 180/np.pi*np.angle(self.plant.fresp(fr)))

    def plot_plant_zoom(self):
        """
        Function to handle plant plot resampling when zoomed
        """
        # minimum and maximum frequencies from the view range
        fmin = 10 ** self.plot_lines['plant'][2].getViewBox().viewRange()[0][0]
        fmax = 10 ** self.plot_lines['plant'][2].viewRange()[0][1]
        # check if the range changed enough to call a resample
        fr = self.plot_lines['plant_fr']
        current_fmin = fr[0]
        current_fmax = fr[-1]
        if fmin / current_fmin < 1 - self.fr_update_thr or \
                fmin / current_fmin > 1 + self.fr_update_thr or \
                fmax / current_fmax < 1 - self.fr_update_thr or \
                fmax / current_fmax > 1 + self.fr_update_thr:
            # new frequency bins to span all the current range
            self.plot_lines['plant_fr'] = np.logspace(np.log10(fmin), np.log10(fmax), self.nfr)
            # update plots
            self.plot_plant()

    def plot_ctrl(self, checked=False, ctrl=None):
        """
        Plot the controller bode in a new window
        """
        # quick override to just update the current trace when a controller is provided
        # (this is used while moving a pole or zero)
        if ('ctrl' in self.plot_windows.keys()) and (ctrl is not None):
            fr = self.plot_lines['ctrl_fr']
            self.plot_lines['ctrl'][0].setData(fr, 20 * np.log10(np.abs(ctrl.fresp(fr))))
            self.plot_lines['ctrl'][1].setData(fr, 180 / np.pi * np.angle(ctrl.fresp(fr)))
            return
        # if called with a controller but no window already open, just return
        if (ctrl is not None) and ('ctrl' not in self.plot_windows.keys()):
            return

        # create a new window if not already existing
        if 'ctrl' not in self.plot_windows.keys():
            # create window
            self.plot_windows['ctrl'] = QtWidgets.QWidget()
            self.plot_windows['ctrl'].setWindowTitle('Controller Bode plot')
            self.plot_windows['ctrl'].setAttribute(QtCore.Qt.WA_DeleteOnClose)
            # remove window from dictionary when closed
            def ctrl_window_closed():
                self.plot_windows.pop('ctrl', 0)
                self.plot_menu_ctrl.setChecked(False)
            self.plot_windows['ctrl'].destroyed.connect(ctrl_window_closed)
            # add two plots
            fig = QtWidgets.QGridLayout()
            self.plot_windows['ctrl'].setLayout(fig)
            plot_abs = pg.PlotWidget()
            plot_phi = pg.PlotWidget()
            fig.addWidget(plot_abs, 0, 0)
            fig.addWidget(plot_phi, 1, 0)
            # setup and link axes
            plot_phi.setXLink(plot_abs)
            # title and labels
            plot_abs.setTitle('Transfer function [magnitude]')
            plot_phi.setTitle('Transfer function [phase]')
            plot_abs.setLabel('left', "Magnitude", units='db')
            plot_abs.setLabel('bottom', "Frequency", units='Hz')
            plot_phi.setLabel('left', "Phase", units='deg')
            plot_phi.setLabel('bottom', "Frequency", units='Hz')
            plot_abs.setLogMode(x=True, y=False)
            plot_phi.setLogMode(x=True, y=False)
            plot_abs.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_phi.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_abs.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_phi.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_abs.showGrid(x=True, y=True, alpha=1)
            plot_phi.showGrid(x=True, y=True, alpha=1)
            # start with the same frequency bins as the main plot
            self.plot_lines['ctrl_fr'] = self.fr.copy()
            fr = self.plot_lines['ctrl_fr']
            line_abs = plot_abs.plot(fr, 20*np.log10(np.abs(self.ctrl.fresp(fr))),
                                     pen=pg.mkPen('r', width=1))
            line_phi = plot_phi.plot(fr, 180/np.pi*np.angle(self.ctrl.fresp(fr)),
                                     pen=pg.mkPen('r', width=1))
            # store pointers to traces and plot areas
            self.plot_lines['ctrl'] = [line_abs, line_phi, plot_abs, plot_phi]

            # plot references if any
            if len(self.reference_ctrl):
                references = []
                # make legends
                legend_a = plot_abs.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                legend_p = plot_phi.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.plot_lines['ctrl'].append(legend_a)
                self.plot_lines['ctrl'].append(legend_p)
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    ra = plot_abs.plot(fr, 20 * np.log10(np.abs(r.fresp(fr))),
                                  pen=c, name=q.text())
                    rp = plot_phi.plot(fr, 180 / np.pi * np.angle(r.fresp(fr)),
                                  pen=c, name=q.text())
                    # make visible if menu is checked
                    if q.isChecked():
                        ra.show()
                        rp.show()
                    else:
                        ra.hide()
                        rp.hide()
                    # save pointer to reference traces
                    references.append([ra, rp])
                # save pointers to reference traces
                self.plot_lines['ctrl_ref'] = references
            # use the same x range as main plot
            xrange = self.plot_abs.getViewBox().viewRange()[0]
            plot_abs.setXRange(xrange[0], xrange[1])
            # connect zoom function
            plot_abs.sigXRangeChanged.connect(self.plot_ctrl_zoom)

            # show window
            self.plot_windows['ctrl'].show()
        else:
            # window is already open, update data
            fr = self.plot_lines['ctrl_fr']
            self.plot_lines['ctrl'][0].setData(fr, 20*np.log10(np.abs(self.ctrl.fresp(fr))))
            self.plot_lines['ctrl'][1].setData(fr, 180/np.pi*np.angle(self.ctrl.fresp(fr)))

            # plot references

            # remove previous traces, if any
            if 'ctrl_ref' in self.plot_lines.keys():
                for r in self.plot_lines['ctrl_ref']:
                    self.plot_lines['ctrl'][2].removeItem(r[0])
                    self.plot_lines['ctrl'][3].removeItem(r[1])
            # replot all traces
            if len(self.reference_ctrl):
                references = []
                # add legend again if needed
                if len(self.plot_lines['ctrl']) <= 4:
                    self.plot_lines['ctrl'][2].getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                    self.plot_lines['ctrl'][3].getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                # add all traces again
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    ra = self.plot_lines['ctrl'][2].plot(fr, 20 * np.log10(np.abs(r.fresp(fr))),
                                  pen=c, name=q.text())
                    rp = self.plot_lines['ctrl'][3].plot(fr, 180 / np.pi * np.angle(r.fresp(fr)),
                                  pen=c, name=q.text())
                    # make them visible only if menu is selected
                    if q.isChecked():
                        ra.show()
                        rp.show()
                    else:
                        ra.hide()
                        rp.hide()
                    # save pointer to reference traces
                    references.append([ra, rp])
                # save pointers to reference traces
                self.plot_lines['ctrl_ref'] = references

    def plot_ctrl_zoom(self):
        """
        Function to handle controller plot resampling when zoomed
        """
        # minimum and maximum frequencies from the view range
        fmin = 10 ** self.plot_lines['ctrl'][2].getViewBox().viewRange()[0][0]
        fmax = 10 ** self.plot_lines['ctrl'][2].viewRange()[0][1]
        # check if the range changed enough to call a resample
        fr = self.plot_lines['ctrl_fr']
        current_fmin = fr[0]
        current_fmax = fr[-1]
        if fmin / current_fmin < 1 - self.fr_update_thr or \
                fmin / current_fmin > 1 + self.fr_update_thr or \
                fmax / current_fmax < 1 - self.fr_update_thr or \
                fmax / current_fmax > 1 + self.fr_update_thr:
            # new frequency bins to span all the current range
            self.plot_lines['ctrl_fr'] = np.logspace(np.log10(fmin), np.log10(fmax), self.nfr)
            # update plots
            self.plot_ctrl()

    def plot_impulse(self, checked=False, ctrl=None):
        """
        Plot the closed-loop impulse response in a new window
        """
        # quick override to just update the current trace when a controller is provided
        # (this is used while moving a pole or zero)
        if ('impulse' in self.plot_windows.keys()) and (ctrl is not None):
            t, y = self.impulse_response(plant=self.plant, ctrl=ctrl)
            self.plot_lines['impulse'][0].setData(t, y)
            return
        # if called with a controller but no window already open, just return
        if (ctrl is not None) and ('impulse' not in self.plot_windows.keys()):
            return

        # create a new window if not already existing
        if 'impulse' not in self.plot_windows.keys():
            # create window
            self.plot_windows['impulse'] = QtWidgets.QWidget()
            self.plot_windows['impulse'].setWindowTitle('Closed-loop impulse response')
            self.plot_windows['impulse'].setAttribute(QtCore.Qt.WA_DeleteOnClose)
            # remove window from dictionary when closed
            def impulse_window_closed():
                self.plot_windows.pop('impulse', 0)
                self.plot_menu_impulse.setChecked(False)
            self.plot_windows['impulse'].destroyed.connect(impulse_window_closed)
            # add one plot
            fig = QtWidgets.QGridLayout()
            self.plot_windows['impulse'].setLayout(fig)
            plot_y = pg.PlotWidget()
            fig.addWidget(plot_y, 0, 0)
            # title and labels
            plot_y.setTitle('Impulse response')
            plot_y.setLabel('left', "Magnitude", units='a.u.')
            plot_y.setLabel('bottom', "Time", units='s')
            plot_y.setLogMode(x=False, y=False)
            plot_y.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_y.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_y.showGrid(x=True, y=True, alpha=1)

            if 't_impulse' not in self.plot_lines.keys():
                # no time defined, so let scipy decide
                t, y = self.impulse_response(plant=self.plant, ctrl=self.ctrl)
                self.plot_lines['t_impulse'] = t
            else:
                # use existing time vector
                t, y = self.impulse_response(plant=self.plant, ctrl=self.ctrl, t=self.plot_lines['t_impulse'])
            # create plot
            line_y = plot_y.plot(t, y, pen=pg.mkPen('r', width=1))
            # save pointers to trace and plot area
            self.plot_lines['impulse'] = [line_y, plot_y]

            # plot references if any
            if len(self.reference_ctrl):
                references = []
                legend_y = plot_y.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.plot_lines['impulse'].append(legend_y)
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    t, y = self.impulse_response(plant=self.plant, ctrl=r, t=self.plot_lines['t_impulse'])
                    r = plot_y.plot(t, y, pen=c, name=q.text())
                    # make them visible only if menu is checked
                    if q.isChecked():
                        r.show()
                    else:
                        r.hide()
                    references.append([r])
                self.plot_lines['impulse_ref'] = references

            # connect zoom function
            plot_y.sigXRangeChanged.connect(self.plot_impulse_zoom)

            # show window
            self.plot_windows['impulse'].show()

        else:
            # window is already open, update data
            t, y = self.impulse_response(plant=self.plant, ctrl=self.ctrl, t=self.plot_lines['t_impulse'])
            self.plot_lines['impulse'][0].setData(t, y)

            # plot references if any
            # remove previous traces, if any
            if 'impulse_ref' in self.plot_lines.keys():
                for r in self.plot_lines['impulse_ref']:
                    self.plot_lines['impulse'][1].removeItem(r[0])
            # replot
            if len(self.reference_ctrl):
                references = []
                # add legend again if needed
                if len(self.plot_lines['impulse']) <= 2:
                    self.plot_lines['impulse'][1].getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                # add all traces again
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    t, y = self.impulse_response(plant=self.plant, ctrl=r, t=self.plot_lines['t_impulse'])
                    r = self.plot_lines['impulse'][1].plot(t, y, pen=c, name=q.text())
                    # make them visible only if menu is checked
                    if q.isChecked():
                        r.show()
                    else:
                        r.hide()
                    # append pointer to reference trace
                    references.append([r])
                # save pointers to reference traces
                self.plot_lines['impulse_ref'] = references

    def plot_impulse_zoom(self):
        """
        Function to handle impulse response plot resampling when zoomed
        """
        # minimum and maximum times from the view range
        self.plot_lines['impulse'][1].disableAutoRange(pg.ViewBox.XAxis)
        tmin = self.plot_lines['impulse'][1].getViewBox().viewRange()[0][0]
        tmax = self.plot_lines['impulse'][1].viewRange()[0][1]
        # check if the range changed enough to call a resample
        t_update_thr = 0.05
        t = self.plot_lines['t_impulse']
        current_tmin = t[0]
        current_tmax = t[-1]
        if tmin / current_tmin < 1 - t_update_thr or \
                tmin / current_tmin > 1 + t_update_thr or \
                tmax / current_tmax < 1 - t_update_thr or \
                tmax / current_tmax > 1 + t_update_thr:
            # new frequency bins to span all the current range (only positive times)
            self.plot_lines['t_impulse'] = np.linspace(max(0, tmin), tmax, 1000)
            # update plots
            self.plot_impulse()

    def plot_step(self, checked=False, ctrl=None):
        """
        Plot the closed-loop step response in a new window
        """
        # quick override to just update the current trace when a controller is provided
        # (this is used while moving a pole or zero)
        if ('step' in self.plot_windows.keys()) and (ctrl is not None):
            t, y = self.step_response(plant=self.plant, ctrl=ctrl)
            self.plot_lines['step'][0].setData(t, y)
            return
        # if called with a controller but no window already open, just return
        if (ctrl is not None) and ('step' not in self.plot_windows.keys()):
            return

        # create a new window if not already existing
        if 'step' not in self.plot_windows.keys():
            # create window
            self.plot_windows['step'] = QtWidgets.QWidget()
            self.plot_windows['step'].setWindowTitle('Closed-loop step response')
            self.plot_windows['step'].setAttribute(QtCore.Qt.WA_DeleteOnClose)
            # remove window from dictionary when closed
            def step_window_closed():
                self.plot_windows.pop('step', 0)
                self.plot_menu_step.setChecked(False)
            self.plot_windows['step'].destroyed.connect(step_window_closed)
            # add one plot
            fig = QtWidgets.QGridLayout()
            self.plot_windows['step'].setLayout(fig)
            plot_y = pg.PlotWidget()
            fig.addWidget(plot_y, 0, 0)
            # title and labels
            plot_y.setTitle('Step response')
            plot_y.setLabel('left', "Output", units='a.u.')
            plot_y.setLabel('bottom', "Time", units='s')
            plot_y.setLogMode(x=False, y=False)
            plot_y.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_y.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_y.showGrid(x=True, y=True, alpha=1)

            if 't_step' not in self.plot_lines.keys():
                # not time vector, let scipy decide
                t, y = self.step_response(plant=self.plant, ctrl=self.ctrl)
                self.plot_lines['t_step'] = t
            else:
                # use existing time vector
                t, y = self.step_response(plant=self.plant, ctrl=self.ctrl, t=self.plot_lines['t_step'])
            # create plot
            line_y = plot_y.plot(t, y, pen=pg.mkPen('r', width=1))
            self.plot_lines['step'] = [line_y, plot_y]

            # plot references if any
            if len(self.reference_ctrl):
                references = []
                legend_y = plot_y.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.plot_lines['step'].append(legend_y)
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    t, y = self.step_response(plant=self.plant, ctrl=r, t=self.plot_lines['t_step'])
                    r = plot_y.plot(t, y, pen=c, name=q.text())
                    # make them visible only if menu items are checked
                    if q.isChecked():
                        r.show()
                    else:
                        r.hide()
                    # append pointer to reference line
                    references.append([r])
                # save pointers to reference lines
                self.plot_lines['step_ref'] = references

            # connect zoom function
            plot_y.sigXRangeChanged.connect(self.plot_step_zoom)

            # show window
            self.plot_windows['step'].show()

        else:
            # window is already open, update data
            t, y = self.step_response(plant=self.plant, ctrl=self.ctrl, t=self.plot_lines['t_step'])
            self.plot_lines['step'][0].setData(t, y)

            # plot references if any
            # remove previous traces, if any
            if 'step_ref' in self.plot_lines.keys():
                for r in self.plot_lines['step_ref']:
                    self.plot_lines['step'][1].removeItem(r[0])
            # replot
            if len(self.reference_ctrl):
                references = []
                # add legend again if needed
                if len(self.plot_lines['step']) <= 2:
                    self.plot_lines['step'][1].getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                # add all traces again
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    t, y = self.step_response(plant=self.plant, ctrl=r, t=self.plot_lines['t_step'])
                    r = self.plot_lines['step'][1].plot(t, y, pen=c, name=q.text())
                    # make them visible only if menu item is checked
                    if q.isChecked():
                        r.show()
                    else:
                        r.hide()
                    # append pointer to reference line
                    references.append([r])
                # save pointers to reference lines
                self.plot_lines['step_ref'] = references

    def plot_step_zoom(self):
        """
        Function to handle step response plot resampling when zoomed
        """
        # minimum and maximum times from the view range
        self.plot_lines['step'][1].disableAutoRange(pg.ViewBox.XAxis)
        tmin = self.plot_lines['step'][1].getViewBox().viewRange()[0][0]
        tmax = self.plot_lines['step'][1].viewRange()[0][1]
        # check if the range changed enough to call a resample
        t_update_thr = 0.05
        t = self.plot_lines['t_step']
        current_tmin = t[0]
        current_tmax = t[-1]
        if tmin / current_tmin < 1 - t_update_thr or \
                tmin / current_tmin > 1 + t_update_thr or \
                tmax / current_tmax < 1 - t_update_thr or \
                tmax / current_tmax > 1 + t_update_thr:
            # new frequency bins to span all the current range (only positive time)
            self.plot_lines['t_step'] = np.linspace(max(0, tmin), tmax, 1000)
            # update plots
            self.plot_step()

    def plot_root(self, checked=False, ctrl=None):
        """
        Plot the root locus in a new window
        """
        # quick override to just update the current trace when a controller is provided
        # (this is used while moving a pole or zero)
        if ('root' in self.plot_windows.keys()) and (ctrl is not None):
            stab, poles = self.isStable(plant=self.plant, ctrl=ctrl)
            self.plot_lines['root'][0].setData(np.real(poles), np.imag(poles))
            return
        # if called with a controller but no window already open, just return
        if (ctrl is not None) and ('root' not in self.plot_windows.keys()):
            return

        # create a new window if not already existing
        if 'root' not in self.plot_windows.keys():
            # create window
            self.plot_windows['root'] = QtWidgets.QWidget()
            self.plot_windows['root'].setWindowTitle('Root Locus')
            self.plot_windows['root'].setAttribute(QtCore.Qt.WA_DeleteOnClose)
            # remove window from dictionary when closed
            def root_window_closed():
                self.plot_windows.pop('root', 0)
                self.plot_menu_root.setChecked(False)
            self.plot_windows['root'].destroyed.connect(root_window_closed)
            # add one plot
            fig = QtWidgets.QGridLayout()
            self.plot_windows['root'].setLayout(fig)
            plot_r = pg.PlotWidget()
            fig.addWidget(plot_r, 0, 0)
            # title and labels
            plot_r.setTitle('Root locus')
            plot_r.setLabel('left', "Imaginary", units='rad/s')
            plot_r.setLabel('bottom', "Real", units='rad/s')
            plot_r.setLogMode(x=False, y=False)
            plot_r.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_r.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_r.showGrid(x=True, y=True, alpha=1)
            stab, poles = self.isStable()
            line_r = plot_r.plot(np.real(poles), np.imag(poles), pen=None,
                                 symbolPen='r', symbolBrush='r', symbol='x')
            # save pointers to trace and plot area
            self.plot_lines['root'] = [line_r, plot_r]

            # plot references if any
            if len(self.reference_ctrl):
                references = []
                legend_r = plot_r.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.plot_lines['root'].append(legend_r)
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    stab, poles = self.isStable(ctrl=r)
                    r = plot_r.plot(np.real(poles), np.imag(poles), pen=None, symbol='x',
                                                        symbolPen=c, symbolBrush=c, name=q.text())
                    # make them visible only if menu item is checked
                    if q.isChecked():
                        r.show()
                    else:
                        r.hide()
                    # append pointer to trace
                    references.append([r])
                # save pointers to trace
                self.plot_lines['root_ref'] = references

            # show window
            self.plot_windows['root'].show()

        else:
            # window is already open, update data
            stab, poles = self.isStable()
            self.plot_lines['root'][0].setData(np.real(poles), np.imag(poles))

            # plot references if any
            # remove previous traces, if any
            if 'root_ref' in self.plot_lines.keys():
                for r in self.plot_lines['root_ref']:
                    self.plot_lines['root'][1].removeItem(r[0])

            # replot
            if len(self.reference_ctrl):
                references = []
                # add legend again if needed
                if len(self.plot_lines['root']) <= 2:
                    self.plot_lines['root'][1].getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                # add all traces again
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    stab, poles = self.isStable(ctrl=r)
                    r = self.plot_lines['root'][1].plot(np.real(poles), np.imag(poles), pen=None, symbol='x',
                                                        symbolPen=c, symbolBrush=c, name=q.text())
                    # make them visible if menu is checked
                    if q.isChecked():
                        r.show()
                    else:
                        r.hide()
                    # append pointer to trace
                    references.append([r])
                # save pointers to trace
                self.plot_lines['root_ref'] = references

    def plot_nichols(self, checked=False, ctrl=None):
        """
        Nichols plot in a new window
        """
        # quick override to just update the current trace when a controller is provided
        # (this is used while moving a pole or zero)
        if ('nichols' in self.plot_windows.keys()) and (ctrl is not None):
            fr = self.plot_lines['nichols_fr']
            olg = self.plant.fresp(fr) * ctrl.fresp(fr)
            self.plot_lines['nichols'][0].setData(180 / np.pi * np.unwrap(np.angle(olg)),
                                                  20 * np.log10(np.abs(olg)))
            return
        # if called with a controller but no window already open, just return
        if (ctrl is not None) and ('nichols' not in self.plot_windows.keys()):
            return

        # create a new window if not already existing
        if 'nichols' not in self.plot_windows.keys():
            # create window
            self.plot_windows['nichols'] = QtWidgets.QWidget()
            self.plot_windows['nichols'].setWindowTitle('Nichols plot')
            self.plot_windows['nichols'].setAttribute(QtCore.Qt.WA_DeleteOnClose)
            # remove window from dictionary when closed
            def nichols_window_closed():
                self.plot_windows.pop('nichols', 0)
                self.plot_menu_nichols.setChecked(False)
            self.plot_windows['nichols'].destroyed.connect(nichols_window_closed)
            # add one plot
            fig = QtWidgets.QGridLayout()
            self.plot_windows['nichols'].setLayout(fig)
            plot_n = pg.PlotWidget()
            fig.addWidget(plot_n, 0, 0)
            # title and labels
            plot_n.setTitle('Nichols plot')
            plot_n.setLabel('left', "Magnitude", units='db')
            plot_n.setLabel('bottom', "Phase", units='deg')
            plot_n.setLogMode(x=False, y=False)
            plot_n.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_n.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_n.showGrid(x=True, y=True, alpha=1)
            # compute frequency bins and open loop gain
            self.plot_lines['nichols_fr'] = self.initial_frequency_bins(nfr=3000, factor=10)
            fr = self.plot_lines['nichols_fr']
            olg = self.plant.fresp(fr) * self.ctrl.fresp(fr)
            line_n = plot_n.plot(180 / np.pi * np.unwrap(np.angle(olg)),
                                 20 * np.log10(np.abs(olg)),
                                 pen=pg.mkPen('r', width=1))
            # save pointers to trace and plot area
            self.plot_lines['nichols'] = [line_n, plot_n]

            # add some additional references to the Nichols plot
            # critical point -1
            plot_n.plot([-180], [0], pen=None, symbol='+', symbolPen='k', symbolBrush='k')
            # overshoot contours
            phi = np.linspace(-2*np.pi, 0, 1000)
            def overshoot(phi, db):
                """
                Compute the overshoot contour for db
                """
                return -1 + 1 / (10 ** (db / 20)) * np.exp(-1j * phi)
            # plot countours corresponding to this amount of overshoot in db
            for db in [0, 1, 2, 3, 6, 10]:
                z = overshoot(phi, db)
                plot_n.plot(180 / np.pi * np.unwrap(np.angle(z[abs(z)>1e-6])),
                            20 * np.log10(np.abs(z[abs(z)>1e-6])), pen='k')
                text = pg.TextItem('%d db' % db, color='k', anchor=(0.5,0.5))
                plot_n.addItem(text)
                if db == 0:
                    text.setPos(-90, -40)
                else:
                    text.setPos(-180, 20 * np.log10(np.abs(z[0])))
            # plot references if any
            if len(self.reference_ctrl):
                references = []
                legend_n = plot_n.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.plot_lines['nichols'].append(legend_n)
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    olg = self.plant.fresp(fr) * r.fresp(fr)
                    rn = plot_n.plot(180 / np.pi * np.unwrap(np.angle(olg)),
                                 20 * np.log10(np.abs(olg)),
                                  pen=c, name=q.text())
                    # make them visible only if menu item is checked
                    if q.isChecked():
                        rn.show()
                    else:
                        rn.hide()
                    # append pointer to reference trace
                    references.append([rn])
                # save pointers to reference traces
                self.plot_lines['nichols_ref'] = references

            # show window
            self.plot_windows['nichols'].show()

        else:
            # window is already open, update data
            self.plot_lines['nichols_fr'] = self.initial_frequency_bins(nfr=3000, factor=10)
            fr = self.plot_lines['nichols_fr']
            olg = self.plant.fresp(fr) * self.ctrl.fresp(fr)
            self.plot_lines['nichols'][0].setData(180 / np.pi * np.unwrap(np.angle(olg)),
                                                  20*np.log10(np.abs(olg)))

            # plot references if any
            # remove previous traces, if any
            if 'nichols_ref' in self.plot_lines.keys():
                for r in self.plot_lines['nichols_ref']:
                    self.plot_lines['nichols'][1].removeItem(r[0])
            # replot
            if len(self.reference_ctrl):
                references = []
                # add legend again if needed
                if len(self.plot_lines['nichols']) <= 2:
                    self.plot_lines['nichols'][1].getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                # add all traces again
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    olg = self.plant.fresp(fr) * r.fresp(fr)
                    rn = self.plot_lines['nichols'][1].plot(180 / np.pi * np.unwrap(np.angle(olg)),
                                       20 * np.log10(np.abs(olg)),
                                       pen=c, name=q.text())
                    # make it visible only if menu item is checked
                    if q.isChecked():
                        rn.show()
                    else:
                        rn.hide()
                    # append pointer to reference trace
                    references.append([rn])
                # save pointers to reference traces
                self.plot_lines['nichols_ref'] = references

    def plot_nyquist(self, checked=False, ctrl=None):
        """
        Nyquist plot in a new window
        """
        # quick override to just update the current trace when a controller is provided
        # (this is used while moving a pole or zero)
        if ('nyquist' in self.plot_windows.keys()) and (ctrl is not None):
            fr = self.plot_lines['nyquist_fr']
            olg = self.plant.fresp(fr) * ctrl.fresp(fr)
            self.plot_lines['nyquist'][0].setData(np.real(olg), np.imag(olg))
            return
        # if called with a controller but no window already open, just return
        if (ctrl is not None) and ('nyquist' not in self.plot_windows.keys()):
            return

        # create a new window if not already existing
        if 'nyquist' not in self.plot_windows.keys():
            # create window
            self.plot_windows['nyquist'] = QtWidgets.QWidget()
            self.plot_windows['nyquist'].setWindowTitle('Nyquist plot')
            self.plot_windows['nyquist'].setAttribute(QtCore.Qt.WA_DeleteOnClose)
            # remove window from dictionary when closed
            def nyquist_window_closed():
                self.plot_windows.pop('nyquist', 0)
                self.plot_menu_nyquist.setChecked(False)
            self.plot_windows['nyquist'].destroyed.connect(nyquist_window_closed)
            # add one plot
            fig = QtWidgets.QGridLayout()
            self.plot_windows['nyquist'].setLayout(fig)
            plot_n = pg.PlotWidget()
            fig.addWidget(plot_n, 0, 0)
            # title and labels
            plot_n.setTitle('Nyquist plot')
            plot_n.setLabel('left', "Imaginary", units='')
            plot_n.setLabel('bottom', "Real", units='')
            plot_n.setLogMode(x=False, y=False)
            plot_n.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
            plot_n.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
            plot_n.showGrid(x=True, y=True, alpha=1)
            # compute frequency bins
            self.plot_lines['nyquist_fr'] = self.initial_frequency_bins(nfr=3000, factor=10)
            # Nyquist plot takes fr from -inf to +inf
            self.plot_lines['nyquist_fr'] = np.concatenate([-self.plot_lines['nyquist_fr'][::-1],
                                                            self.plot_lines['nyquist_fr']])
            fr = self.plot_lines['nyquist_fr']
            # compute open loop gain
            olg = self.plant.fresp(fr) * self.ctrl.fresp(fr)
            line_n = plot_n.plot(np.real(olg), np.imag(olg), pen=pg.mkPen('r', width=1))
            # save pointers to trace and plot area
            self.plot_lines['nyquist'] = [line_n, plot_n]

            # add some additional references to the Nichols plot
            # critical point -1
            plot_n.plot([-1], [0], pen=None, symbol='+', symbolPen='k', symbolBrush='k')
            # overshoot contours
            phi = np.linspace(-2*np.pi, 0, 1000)
            def overshoot(phi, db):
                """
                Compute the overshoot contour for db
                """
                return -1 + 1 / (10 ** (db / 20)) * np.exp(-1j * phi)
            # plot contours where overshoot is that amount in db
            for db in [0, 1, 2, 3, 6, 10]:
                z = overshoot(phi, db)
                plot_n.plot(np.real(z), np.imag(z), pen='k')
                text = pg.TextItem('%d db' % db, color='k', anchor=(0.5,0.5))
                plot_n.addItem(text)
                text.setPos(np.real(z[250]), np.imag(z[250]))
            # plot references if any
            if len(self.reference_ctrl):
                references = []
                legend_n = plot_n.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.plot_lines['nyquist'].append(legend_n)
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    olg = self.plant.fresp(fr) * r.fresp(fr)
                    rn = plot_n.plot(np.real(olg), np.imag(olg), pen=c, name=q.text())
                    # make it visible only if menu is checked
                    if q.isChecked():
                        rn.show()
                    else:
                        rn.hide()
                    # append pointer to trace
                    references.append([rn])
                # save pointers to traces
                self.plot_lines['nyquist_ref'] = references

            # show window
            self.plot_windows['nyquist'].show()
        else:
            # window is already open, update data
            self.plot_lines['nyquist_fr'] = self.initial_frequency_bins(nfr=3000, factor=10)
            fr = self.plot_lines['nyquist_fr']
            olg = self.plant.fresp(fr) * self.ctrl.fresp(fr)
            self.plot_lines['nyquist'][0].setData(np.real(olg), np.imag(olg))

            # plot references if any
            # remove previous traces, if any
            if 'nyquist_ref' in self.plot_lines.keys():
                for r in self.plot_lines['nyquist_ref']:
                    self.plot_lines['nyquist'][1].removeItem(r[0])
            # replot
            if len(self.reference_ctrl):
                references = []
                # add legend again if needed
                if len(self.plot_lines['nyquist']) <= 2:
                    self.plot_lines['nyquist'][1].getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                # add all traces again
                for i,(r,q,c) in enumerate(zip(self.reference_ctrl, self.reference_items, self.reference_color)):
                    olg = self.plant.fresp(fr) * r.fresp(fr)
                    rn = self.plot_lines['nyquist'][1].plot(np.real(olg), np.imag(olg),
                                       pen=c, name=q.text())
                    # make them visible only if menu is checked
                    if q.isChecked():
                        rn.show()
                    else:
                        rn.hide()
                    # append pointer to trace
                    references.append([rn])
                # save pointers to all traces
                self.plot_lines['nyquist_ref'] = references


    ### Functions to handle events

    def mouseMoved(self, evt):
        """
        Called when the mouse hover over the plots, to move crosshair
        """
        pos = evt[0]     # get mouse pointer position
        # continue only if pointer is inside the plot areas
        if (self.plot_abs.sceneBoundingRect().contains(pos) or
                self.plot_phi.sceneBoundingRect().contains(pos) or
                self.plot_clg1.sceneBoundingRect().contains(pos) or
                self.plot_clg2.sceneBoundingRect().contains(pos)):

            # make lines visible (if not moving p/z or gain)
            if (not self.bFitChangeGain.isChecked() and
               not self.bFitAddCmpxPole.isChecked() and
               not self.bFitAddRealPole.isChecked() and
               not self.bFitAddCmpxZero.isChecked() and
               not self.bFitAddRealZero.isChecked() and
               not self.bFitMovePZ.isChecked() and
               not self.bFitDelete.isChecked() and self.bCrosshair.isChecked()):
                # crosshair for main traces
                self.vLinea.setVisible(True)
                self.hLinea.setVisible(True)
                self.hLinep.setVisible(True)
                self.vLinep.setVisible(True)
                self.vLine1.setVisible(True)
                self.hLine1.setVisible(True)
                self.hLine2.setVisible(True)
                self.vLine2.setVisible(True)
                # crosshairs for reference traces
                for la,lp,l1,l2,ri in zip(self.ref_hLinea, self.ref_hLinep,
                                          self.ref_hLine1, self.ref_hLine2,
                                          self.reference_items):
                    # visible only if menu is checked
                    la.setVisible(ri.isChecked())
                    lp.setVisible(ri.isChecked())
                    l1.setVisible(ri.isChecked())
                    l2.setVisible(ri.isChecked())
            else:
                # hide all since we're adding or deleting poles and zeros
                self.vLinea.setVisible(False)
                self.hLinea.setVisible(False)
                self.hLinep.setVisible(False)
                self.vLinep.setVisible(False)
                self.vLine1.setVisible(False)
                self.hLine1.setVisible(False)
                self.hLine2.setVisible(False)
                self.vLine2.setVisible(False)
                for la,lp,l1,l2 in zip(self.ref_hLinea, self.ref_hLinep,
                                          self.ref_hLine1, self.ref_hLine2):
                    la.setVisible(False)
                    lp.setVisible(False)
                    l1.setVisible(False)
                    l2.setVisible(False)

            # get the mouse position and move the lines
            mousePoint = self.plot_abs.plotItem.vb.mapSceneToView(pos)
            self.vLinea.setPos(mousePoint.x())
            # update the crosshair for the main traces
            fr = 10**mousePoint.x()
            olg = self.plant.fresp(fr) * self.ctrl.fresp(fr)
            self.hLinea.setPos(20*np.log10(np.abs(olg)))
            self.hLinep.setPos(180/np.pi*np.angle(olg))
            self.vLinep.setPos(mousePoint.x())
            self.vLine1.setPos(mousePoint.x())
            self.vLine2.setPos(mousePoint.x())
            self.hLine1.setPos(20*np.log10(np.abs(1/(1+olg))))
            self.hLine2.setPos(20*np.log10(np.abs(olg/(1+olg))))
            # update crosshairs for references
            for la, lp, l1, l2, rc in zip(self.ref_hLinea, self.ref_hLinep,
                                          self.ref_hLine1, self.ref_hLine2, self.reference_ctrl):
                olg = self.plant.fresp(fr) * rc.fresp(fr)
                la.setPos(20 * np.log10(np.abs(olg)))
                lp.setPos(180 / np.pi * np.angle(olg))
                l1.setPos(20 * np.log10(np.abs(1 / (1 + olg))))
                l2.setPos(20 * np.log10(np.abs(olg / (1 + olg))))
        else:
            # hide lines if the mouse is not inside the plot areas
            self.vLinea.setVisible(False)
            self.hLinea.setVisible(False)
            self.vLinep.setVisible(False)
            self.hLinep.setVisible(False)
            self.vLine1.setVisible(False)
            self.hLine1.setVisible(False)
            self.vLine2.setVisible(False)
            self.hLine2.setVisible(False)
            for la, lp, l1, l2 in zip(self.ref_hLinea, self.ref_hLinep,
                                  self.ref_hLine1, self.ref_hLine2):
                la.setVisible(False)
                lp.setVisible(False)
                l1.setVisible(False)
                l2.setVisible(False)

    def undo_change(self):
        """
        Called when the Undo button is clicked
        """
        if not self.undo.empty():
            # there's something in the Undo list, so put it back and update all plots
            self.ctrl = self.undo.get().copy()
            self.tf_graph.updateTF(self.ctrl)
            self.plot()

    def curve_edit_toggle(self):
        """
        Change cursor and make olg line clickable when edit buttons are active
        """
        self.app.restoreOverrideCursor()
        if self.bFitAddCmpxPole.isChecked() or self.bFitAddRealPole.isChecked() or \
                self.bFitAddCmpxZero.isChecked() or self.bFitAddRealZero.isChecked() \
                or self.bFitChangeGain.isChecked():
            self.app.setOverrideCursor(QtCore.Qt.CrossCursor)
            self.line_olg_abs.curve.setClickable(True)
        else:
            self.app.restoreOverrideCursor()
            self.line_olg_abs.curve.setClickable(False)
        if self.bFitDelete.isChecked():
            self.app.setOverrideCursor(QtCore.Qt.CrossCursor)
        else:
            self.app.restoreOverrideCursor()

    def add_reference(self, checked, reference=None):
        """
        Add a reference trace, called by the Reference menu

        Parameters
        ----------
        reference: [str, TF]
            optional [name, controller TF] to specify the reference to be added
        """
        # add fit to the references list
        if reference is None:
            # nothing passsed, use current controller
            self.reference_ctrl.append(self.ctrl.copy())
        else:
            # save the reference that was passed
            self.reference_ctrl.append(reference[1].copy())
        if len(self.reference_color):     # pick a color
            self.reference_color.append(self.reference_color[-1]+1)
        else:
            self.reference_color.append(1)
        # create new menu item
        if reference is None:
            # ask for the reference name
            ref, ok = QtWidgets.QInputDialog.getText(self.win, "Reference name", 'Reference Name',
                                                 text='Reference %d' % (len(self.reference_ctrl)))
        else:
            # name was passed
            ref = reference[0]
            ok = True
        if ok:
            # add the reference menu item
            self.reference_items.append(QtWidgets.QAction(ref, checkable=True))
            self.reference_items[-1].setChecked(True)
            self.reference_items[-1].toggled.connect(self.plot)
            self.ref_menu.addAction(self.reference_items[-1])
            # replot
            self.plot()

            # add horizontal lines for crosshairs for this reference
            self.ref_hLinea.append(pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen(self.reference_color[-1],
                                                                                        width=0.5),
                                  label='{value:0.2f}', labelOpts={'position': 0.2+0.1*len(self.ref_hLinea),
                                                                      'color': self.reference_color[-1],
                                                                      'movable': True}))
            self.ref_hLinep.append(pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen(self.reference_color[-1],
                                                                                        width=0.5),
                                  label='{value:0.2f}', labelOpts={'position': 0.2+0.1*len(self.ref_hLinep),
                                                                      'color': self.reference_color[-1],
                                                                      'movable': True}))
            self.ref_hLine1.append(pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen(self.reference_color[-1],
                                                                                        width=0.5),
                                  label='{value:0.2f}', labelOpts={'position': 0.2+0.1*len(self.ref_hLine1),
                                                                      'color': self.reference_color[-1],
                                                                      'movable': True}))
            self.ref_hLine2.append(pg.InfiniteLine(angle=0, movable=False, pen=pg.mkPen(self.reference_color[-1],
                                                                                        width=0.5),
                                  label='{value:0.2f}', labelOpts={'position': 0.2+0.1*len(self.ref_hLine2),
                                                                      'color': self.reference_color[-1],
                                                                      'movable': True}))
            self.plot_abs.addItem(self.ref_hLinea[-1], ignoreBounds=True)
            self.plot_phi.addItem(self.ref_hLinep[-1], ignoreBounds=True)
            self.plot_clg1.addItem(self.ref_hLine1[-1], ignoreBounds=True)
            self.plot_clg2.addItem(self.ref_hLine2[-1], ignoreBounds=True)
            # visible only if menu is checked
            self.ref_hLinea[-1].setVisible(self.bCrosshair.isChecked())
            self.ref_hLinep[-1].setVisible(self.bCrosshair.isChecked())
            self.ref_hLine1[-1].setVisible(self.bCrosshair.isChecked())
            self.ref_hLine2[-1].setVisible(self.bCrosshair.isChecked())

    def delete_references(self):
        """
        Delete all checked references, called when the user click on the Delete references menu item.
        """
        # find the items that are selected
        remove = [(i, ri) for i, ri in enumerate(zip(self.reference_items,
                                                     self.reference_color,
                                                     self.reference_ctrl,
                                                     self.reference_plot_abs,
                                                     self.reference_plot_phi,
                                                     self.reference_plot_clg1,
                                                     self.reference_plot_clg2,
                                                     self.ref_hLinea, self.ref_hLinep,
                                                     self.ref_hLine1, self.ref_hLine2)) if ri[0].isChecked()]
        # remove them from menu and lists and plots
        for i,r in remove:
            ri, rc, rf, rpa, rpp, rp1, rp2, hla, hlp, hl1, hl2 = r
            self.ref_menu.removeAction(ri)
            self.reference_color.remove(rc)
            self.reference_items.remove(ri)
            self.reference_ctrl.remove(rf)
            self.plot_abs.removeItem(rpa)
            self.plot_phi.removeItem(rpp)
            self.plot_clg1.removeItem(rp1)
            self.plot_clg2.removeItem(rp2)
            # remove also crosshair lines
            self.plot_abs.removeItem(hla)
            self.plot_phi.removeItem(hlp)
            self.plot_phi.removeItem(hl1)
            self.plot_phi.removeItem(hl2)
            self.ref_hLinea.remove(hla)
            self.ref_hLinep.remove(hlp)
            self.ref_hLine1.remove(hl1)
            self.ref_hLine2.remove(hl2)

        # replot
        self.plot()

    def revert_reference(self):
        """
        Revert transfer function to first selected reference
        """
        # find the first selected item
        for i,r in enumerate(self.reference_items):
            if r.isChecked():
                break
        # save transfer function to undo stack
        self.undo.put(self.ctrl.copy())
        # revert to reference transfer function
        self.ctrl = self.reference_ctrl[i].copy()
        # update graphs
        self.plot()
        self.tf_graph.updateTF(self.ctrl)

    def curve_lockgain_toggle(self):
        """
        Called when the Lock gain button is toggled
        """
        self.app.restoreOverrideCursor()
        if self.bFitLockGain.isChecked() and self.lock_gain_freq is None:
            # clicked on Lock gain to set the frequency, disable other actions
            self.bFitAddCmpxPole.setChecked(False)
            self.bFitAddRealPole.setChecked(False)
            self.bFitAddCmpxZero.setChecked(False)
            self.bFitAddRealZero.setChecked(False)
            self.bFitChangeGain.setChecked(False)
            self.bFitDelete.setChecked(False)
            self.line_olg_abs.curve.setClickable(True)
        if not self.bFitLockGain.isChecked():
            # disabled the Lock Gain button, so clear the gain lock variables
            self.lock_gain_freq = None
            self.lock_gain_value = None
            if self.lock_gain_dot is not None:
                self.plot_abs.removeItem(self.lock_gain_dot)
            self.lock_gain_dot = None
            self.plot()
            self.line_olg_abs.curve.setClickable(False)

    def toggle_move_zp(self):
        """
        Called when the Move button is toggled, shows or hide the status bar widgets
        """
        if self.bFitMovePZ.isChecked():
            # show status bas widgets if something is selected
            if self.tf_graph.selected:
                self.status_bar_finput.show()
                self.status_bar_finput.setText('%.4f' % self.frequency_increment)
                self.status_bar_flabel.show()
                self.status_bar_qinput.show()
                self.status_bar_qinput.setText('%.4f' % self.q_increment)
                self.status_bar_qlabel.show()
            else:
                self.status_bar_finput.hide()
                self.status_bar_flabel.hide()
                self.status_bar_qinput.hide()
                self.status_bar_qlabel.hide()
        else:
            self.status_bar_finput.hide()
            self.status_bar_flabel.hide()
            self.status_bar_qinput.hide()
            self.status_bar_qlabel.hide()

    def toggle_gain(self):
        """
        Called when the Change gain button is toggled, shows or hide the status bar widgets
        """
        if self.bFitChangeGain.isChecked():
            self.status_bar_gaininput.show()
            self.status_bar_gaininput.setText('%.4f' % self.gain_increment)
            self.status_bar_gainlabel.show()
        else:
            self.status_bar_gaininput.hide()
            self.status_bar_gainlabel.hide()

    def finput_textChanged(self):
        """
        Called when the frequency increment input is changed
        """
        try:
            new_gain = float(self.status_bar_finput.text())
            if new_gain > 0 and new_gain < 1:
                self.frequency_increment = new_gain
        except:
            self.status_bar_finput.setText('%.4f' % self.frequency_increment)

    def qinput_textChanged(self):
        """
        Called when the frequency increment input is changed
        """
        try:
            new_gain = float(self.status_bar_qinput.text())
            if new_gain > 0 and new_gain < 1:
                self.q_increment = new_gain
        except:
            self.status_bar_qinput.setText('%.4f' % self.q_increment)

    def gaininput_textChanged(self):
        """
        Called when the frequency increment input is changed
        """
        try:
            new_gain = float(self.status_bar_gaininput.text())
            if new_gain > 0:
                self.gain_increment = new_gain
        except:
            self.status_bar_gaininput.setText('%.4f' % self.gain_increment)


    def key_press(self, key):
        """
        Move poles, zeros and gain using keyboard. This method is called by the event handler of the main window class
        """

        if self.bFitMovePZ.isChecked():
            # react to a keyboard event if the Move Poles/zeros button is active
            if self.tf_graph.selected is not None:
                # fine the pole or zero currently selected
                pz  = self.tf_graph.symbols[self.tf_graph.selected]      # 'star' for poles, 'o' for zeros
                fr  = self.tf_graph.frequencies[self.tf_graph.selected]  # frequency
                q   = self.tf_graph.Q[self.tf_graph.selected]            # Q
                idx = self.tf_graph.idx[self.tf_graph.selected]          # sequential index in the zeros or poles lists
                # start with same frequency and Q
                new_fr = fr
                new_q  = q
                # set the frequency increment as a fraction of the currently displayed range
                increment = 10**((np.log10(self.fr[-1])-np.log10(self.fr[0])) * self.frequency_increment)
                # set the Q increment with opposite directions for poles and zeros, so that KeyUp always move
                # the pole or zero up in the abs plot
                if pz == 'star':
                    Qincrement = 1 + self.q_increment
                else:
                    Qincrement = 1/(1 + self.q_increment)
                # change values depending on the key pressed
                if key == QtCore.Qt.Key_Right:
                    new_fr = fr * increment
                if key == QtCore.Qt.Key_Left:
                    new_fr = fr / increment
                if key == QtCore.Qt.Key_Up:
                    new_q = q * Qincrement
                if key == QtCore.Qt.Key_Down:
                    new_q = q / Qincrement
                # if there's a change, update the values in the transfer function object
                if fr != new_fr or q != new_q:
                    self.undo.put(self.ctrl.copy())     # add current TF to the undo stack
                    if self.lock_gain_freq is not None:
                        # remember the TF gain at the lock frequency, f gain is locked
                        gain = abs(self.ctrl.fresp(self.lock_gain_freq))
                    # update pole or zero
                    if pz == 'star':
                        self.ctrl.p_f[idx] = new_fr
                        self.ctrl.p_Q[idx] = new_q
                    else:
                        self.ctrl.z_f[idx] = new_fr
                        self.ctrl.z_Q[idx] = new_q
                    # propagate to ttransfer function
                    self.ctrl.set_fQ(self.ctrl.z_f, self.ctrl.z_Q,
                                       self.ctrl.p_f, self.ctrl.p_Q, self.ctrl.k)
                    if self.lock_gain_freq is not None:
                        # set gain if gain was locked
                        self.ctrl.set_gain(self.lock_gain_freq, gain)
                    # replot and update graphs
                    self.tf_graph.updateTF(self.ctrl)
                    self.plot()

                    # update status line
                    self.showStability()
                    if pz == 'star':
                        self.status_bar.showMessage('Selected pole fr=%f Hz, Q=%f' % (new_fr, new_q))
                    else:
                        self.status_bar.showMessage('Selected zero fr=%f Hz, Q=%f' % (new_fr, new_q))
        if self.bFitChangeGain.isChecked():
            # react to a keyboard event if the Change Gain button is active
            # start from current gain
            k = self.ctrl.k
            new_k = k

            # change by 2% if KeyUp or 50%% if PageUp
            if key == QtCore.Qt.Key_Up:
                new_k = k * 10**(self.gain_increment/20)
            if key == QtCore.Qt.Key_Down:
                new_k = k / 10**(self.gain_increment/20)
            if key == QtCore.Qt.Key_PageUp:
                new_k = k * 1.5
            if key == QtCore.Qt.Key_PageDown:
                new_k = k / 1.5
            if k != new_k:
                # update if there is a change
                self.ctrl.set_k(new_k)
                if self.lock_gain_value is not None:
                    self.lock_gain_value = self.lock_gain_value * new_k/k
            # update plots and graphs
            self.tf_graph.updateTF(self.ctrl)
            self.plot()

    def add_other(self):
        """
        Open a dialog to add low pass / high pass / band pass / band stop /
        notch / resonant gain
        """

        # list of things that can be added
        kind_of_filter = {
            'Butterworth' : ['Order'],
            'Elliptical' : ['Order', 'Bandpass ripple [db]', 'Minimum attenuation [db]'],
            'Chebyshev 1': ['Order', 'Bandpass ripple [db]'],
            'Chebyshev 2': ['Order', 'Minimum attenuation [db]'],
            'Bessel': ['Order']
        }
        features = {
            'Low Pass': [kind_of_filter, ['Frequency [Hz]']],
            'High Pass': [kind_of_filter, ['Frequency [Hz]']],
            'Band Pass': [kind_of_filter, ['Low Frequency [Hz]', 'High Frequency [Hz]']],
            'Band Stop': [kind_of_filter, ['Low Frequency [Hz]', 'High Frequency [Hz]']],
            'Notch': [None, ['Frequency [Hz]', 'Q', 'Depth [db]']],
            'Resonant Gain': [None, ['Frequency [Hz]', 'Q', 'Gain [db]']],
        }

        # Create a new window with grid layout
        q = QtWidgets.QWidget()
        q.setWindowTitle("Add feature")
        g = QtWidgets.QGridLayout()
        q.setLayout(g)
        # first combo box for what feature to add
        v1 = QtWidgets.QHBoxLayout()
        g.addLayout(v1, 1, 1, 1, 2)
        label1 = QtWidgets.QLabel('What to add')
        v1.addWidget(label1)
        combo1 = QtWidgets.QComboBox()
        for k in features.keys():
            combo1.addItem(k)
        combo1.setCurrentIndex(0)
        v1.addWidget(combo1)
        # second combo box for kind of filter
        v2 = QtWidgets.QHBoxLayout()
        g.addLayout(v2, 2, 1, 1, 2)
        label2 = QtWidgets.QLabel('Style')
        v2.addWidget(label2)
        combo2 = QtWidgets.QComboBox()
        v2.addWidget(combo2)
        # labels and input fields for parameters (will be renamed and make visible depending on what's selected)
        label3 = QtWidgets.QLabel('Parameter 1')
        g.addWidget(label3, 3, 1)
        param1 = QtWidgets.QLineEdit()
        param1.setValidator(pg.QtGui.QDoubleValidator())
        g.addWidget(param1, 3, 2)
        label4 = QtWidgets.QLabel('Parameter 2')
        g.addWidget(label4, 4, 1)
        param2 = QtWidgets.QLineEdit()
        param2.setValidator(pg.QtGui.QDoubleValidator())
        g.addWidget(param2, 4, 2)
        label5 = QtWidgets.QLabel('Parameter 3')
        g.addWidget(label5, 5, 1)
        param3 = QtWidgets.QLineEdit()
        param3.setValidator(pg.QtGui.QDoubleValidator())
        g.addWidget(param3, 5, 2)
        label6 = QtWidgets.QLabel('Parameter 4')
        g.addWidget(label6, 6, 1)
        param4 = QtWidgets.QLineEdit()
        param4.setValidator(pg.QtGui.QDoubleValidator())
        g.addWidget(param4, 6, 2)
        label7 = QtWidgets.QLabel('Parameter 5')
        g.addWidget(label7, 7, 1)
        param5 = QtWidgets.QLineEdit()
        param5.setValidator(pg.QtGui.QDoubleValidator())
        g.addWidget(param5, 7, 2)
        label8 = QtWidgets.QLabel('Parameter 6')
        g.addWidget(label8, 8, 1)
        param6 = QtWidgets.QLineEdit()
        param6.setValidator(pg.QtGui.QDoubleValidator())
        g.addWidget(param6, 8, 2)
        # keep all of them in a list (this could have been done with a loop!!)
        param_inputs = [param1, param2, param3, param4, param5, param6]
        param_labels = [label3, label4, label5, label6, label7, label8]

        # buttons
        button_done = QtWidgets.QPushButton("Add", q)
        button_cancel = QtWidgets.QPushButton("Cancel", q)
        g.addWidget(button_done, 9, 1)
        g.addWidget(button_cancel, 9, 2)
        button_cancel.clicked.connect(q.close)

        # method to update list of filter styles and parameters when first combo selection changes
        def update_combo2():
            # get combo box selections
            filt = combo1.currentText()
            style = combo2.currentText()
            # get the parameter loist corresponding to the selected feature to be added
            params = features[combo1.currentText()][1]
            if features[combo1.currentText()][0] is not None:
                params = params + features[combo1.currentText()][0][style]
            # update labels
            for i,p in enumerate(params):
                param_labels[i].setText(p)
                param_labels[i].setHidden(False)
                param_inputs[i].setHidden(False)
            for i in range(len(params), 6):
                param_labels[i].setHidden(True)
                param_inputs[i].setHidden(True)
        # connect method to change in combo box
        combo2.activated.connect(update_combo2)

        # method to update second combo box based on first
        def update_combo1():
            # get list of style depending on selection on first combo box
            styles = features[combo1.currentText()][0]
            # update items in second combo box
            combo2.clear()
            if styles is not None:
                for k in styles.keys():
                    combo2.addItem(k)
            update_combo2()
        # connect method to first combo box change
        combo1.activated.connect(update_combo1)
        update_combo1()

        # function to compute and add feature when clicking on done
        def add_feature():
            # get the filter type and style from the selections
            filt = combo1.currentText()
            style = combo2.currentText()
            # depending on selections, compute the filter z, p, k
            if style == 'Butterworth' and filt == 'Low Pass':
                z, p, k = scipy.signal.butter(int(param2.text()), # order
                                              2*np.pi*float(param1.text()), # Wn
                                              btype='lowpass', analog=True, output='zpk')
            if style == 'Butterworth' and filt == 'High Pass':
                z, p, k = scipy.signal.butter(int(param2.text()), # order
                                              2*np.pi*float(param1.text()), # Wn
                                              btype='highpass', analog=True, output='zpk')
            if style == 'Butterworth' and filt == 'Band Pass':
                z, p, k = scipy.signal.butter(int(param3.text()), # order
                                                [2*np.pi*float(param1.text()),
                                                    2*np.pi*float(param2.text())],   # Wn
                                              btype='bandpass', analog=True, output='zpk')
            if style == 'Butterworth' and filt == 'Band Stop':
                z, p, k = scipy.signal.butter(int(param3.text()), # order
                                                [2*np.pi*float(param1.text()),
                                                    2*np.pi*float(param2.text())],   # Wn
                                              btype='bandstop', analog=True, output='zpk')

            if style == 'Elliptical' and filt == 'Low Pass':
                z, p, k = scipy.signal.ellip(int(param2.text()),  # order
                                              float(param3.text()),  # rp
                                              float(param4.text()),  # rs
                                              2 * np.pi * float(param1.text()),  # Wn
                                              btype='lowpass', analog=True, output='zpk')
            if style == 'Elliptical' and filt == 'High Pass':
                z, p, k = scipy.signal.ellip(int(param2.text()),  # order
                                              float(param3.text()),  # rp
                                              float(param4.text()),  # rs
                                              2 * np.pi * float(param1.text()),  # Wn
                                              btype='highpass', analog=True, output='zpk')
            if style == 'Elliptical' and filt == 'Band Pass':
                z, p, k = scipy.signal.ellip(int(param3.text()), # order
                                                 float(param4.text()),  # rp
                                                 float(param5.text()),  # rs
                                                 [2 * np.pi * float(param1.text()),
                                                  2 * np.pi * float(param2.text())],   # Wn
                                              btype='bandpass', analog=True, output='zpk')
            if style == 'Elliptical' and filt == 'Band Stop':
                z, p, k = scipy.signal.ellip(int(param3.text()),  # order
                                              float(param4.text()),  # rp
                                              float(param5.text()),  # rs
                                              [2*np.pi*float(param1.text()),
                                                    2*np.pi*float(param2.text())],  # Wn
                                              btype='bandstop', analog=True, output='zpk')

            if style == 'Chebyshev 1' and filt == 'Low Pass':
                z, p, k = scipy.signal.cheby1(int(param2.text()),  # order
                                              float(param3.text()),  # rp
                                              2 * np.pi * float(param1.text()),  # Wn
                                              btype='lowpass', analog=True, output='zpk')
            if style == 'Chebyshev 1' and filt == 'High Pass':
                z, p, k = scipy.signal.cheby1(int(param2.text()),  # order
                                              float(param3.text()),  # rp
                                              2 * np.pi * float(param1.text()),  # Wn
                                              btype='highpass', analog=True, output='zpk')
            if style == 'Chebyshev 1' and filt == 'Band Pass':
                z, p, k = scipy.signal.cheby1(int(param3.text()),  # order
                                               float(param4.text()),  # rp
                                               [2 * np.pi * float(param1.text()),
                                                2 * np.pi * float(param2.text())],  # Wn
                                              btype='bandpass', analog=True, output='zpk')
            if style == 'Chebyshev 1' and filt == 'Band Stop':
                z, p, k = scipy.signal.cheby1(int(param3.text()),  # order
                                              float(param4.text()),  # rp
                                              [2 * np.pi * float(param1.text()),
                                               2 * np.pi * float(param2.text())],  # Wn
                                              btype='bandstop', analog=True, output='zpk')

            if style == 'Chebyshev 2' and filt == 'Low Pass':
                z, p, k = scipy.signal.cheby2(int(param2.text()),  # order
                                              float(param3.text()),  # rp
                                              2 * np.pi * float(param1.text()),  # Wn
                                              btype='lowpass', analog=True, output='zpk')
            if style == 'Chebyshev 2' and filt == 'High Pass':
                z, p, k = scipy.signal.cheby2(int(param2.text()),  # order
                                              float(param3.text()),  # rp
                                              2 * np.pi * float(param1.text()),  # Wn
                                              btype='highpass', analog=True, output='zpk')
            if style == 'Chebyshev 2' and filt == 'Band Pass':
                z, p, k = scipy.signal.cheby2(int(param3.text()),  # order
                                               float(param4.text()),  # rp
                                               [2 * np.pi * float(param1.text()),
                                               2 * np.pi * float(param2.text())],  # Wn
                                              btype='bandpass', analog=True, output='zpk')
            if style == 'Chebyshev 2' and filt == 'Band Stop':
                z, p, k = scipy.signal.cheby2(int(param3.text()),  # order
                                              float(param4.text()),  # rp
                                              [2 * np.pi * float(param1.text()),
                                               2 * np.pi * float(param2.text())],  # Wn
                                              btype='bandstop', analog=True, output='zpk')

            if style == 'Bessel' and filt == 'Low Pass':
                z, p, k = scipy.signal.bessel(int(param2.text()), # order
                                              2*np.pi*float(param1.text()), # Wn
                                              btype='lowpass', analog=True, output='zpk')
            if style == 'Bessel' and filt == 'High Pass':
                z, p, k = scipy.signal.bessel(int(param2.text()), # order
                                              2*np.pi*float(param1.text()), # Wn
                                              btype='highpass', analog=True, output='zpk')
            if style == 'Bessel' and filt == 'Band Pass':
                z, p, k = scipy.signal.bessel(int(param3.text()), # order
                                                [2*np.pi*float(param1.text()),
                                                    2*np.pi*float(param2.text())],   # Wn
                                              btype='bandpass', analog=True, output='zpk')
            if style == 'Bessel' and filt == 'Band Stop':
                z, p, k = scipy.signal.bessel(int(param3.text()), # order
                                                [2*np.pi*float(param1.text()),
                                                    2*np.pi*float(param2.text())],   # Wn
                                              btype='bandstop', analog=True, output='zpk')

            if filt == 'Notch':
                f = float(param1.text())
                Q = float(param2.text())
                depth = float(param3.text())
                notch = TF(k=1, zero_to_nan=True)
                notch.add_pole_fQ(f, Q)
                notch.add_zero_fQ(f, 10**(depth/20)*Q)
                z, p, k = notch.z, notch.p, notch.k
            if filt == 'Resonant Gain':
                f = float(param1.text())
                Q = float(param2.text())
                depth = float(param3.text())
                notch = TF(k=1, zero_to_nan=True)
                notch.add_zero_fQ(f, Q)
                notch.add_pole_fQ(f, 10**(depth/20)*Q)
                z, p, k = notch.z, notch.p, notch.k

            # add z, p, k to controller
            self.undo.put(self.ctrl.copy())   # but before add current controller to undo stack
            self.ctrl.add_zpk(z[np.argsort(abs(z))], p[np.argsort(abs(p))], k)
            # update all plots
            self.tf_graph.updateTF(self.ctrl)
            self.plot()
            # close window
            q.close()
        # connect method to ok button click
        button_done.clicked.connect(add_feature)

        # open the window and wait for it to close
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()

    def zoom(self):
        """
        Called when the user zoom in the plot area, to resample the main area frequency bins
        """
        # get the new frequency range 
        if self.plotted:
            # disable autorange
            self.plot_abs.disableAutoRange(pg.ViewBox.XAxis)
            self.plot_phi.disableAutoRange(pg.ViewBox.XAxis)
            self.plot_clg1.disableAutoRange(pg.ViewBox.XAxis)
            self.plot_clg2.disableAutoRange(pg.ViewBox.XAxis)
            # minimum and maximum frequencies from the view range
            fmin = 10**self.plot_abs.getViewBox().viewRange()[0][0]
            fmax = 10**self.plot_abs.getViewBox().viewRange()[0][1]

            # check if the range changed enough to call a resample
            current_fmin = self.fr[0]
            current_fmax = self.fr[-1]
            if fmin/current_fmin < 1 - self.fr_update_thr or \
                    fmin/current_fmin > 1 + self.fr_update_thr or \
                    fmax/current_fmax < 1 - self.fr_update_thr or \
                    fmax/current_fmax > 1 + self.fr_update_thr or \
                    (current_fmax - current_fmin)/(fmax - fmin) < 1 - self.fr_update_thr or \
                    (current_fmax - current_fmin)/(fmax - fmin) > 1 - self.fr_update_thr:
                # new frequency bins to span all the current range
                self.fr = np.logspace(np.log10(fmin), np.log10(fmax), self.nfr)
                # recompute and replot all curves
                ctrl = self.ctrl.fresp(self.fr)
                plant = self.plant.fresp(self.fr)
                olg = ctrl * plant
                self.line_olg_abs.setData(self.fr, 20*np.log10(np.abs(olg)))
                self.line_olg_phi.setData(self.fr, 180 / np.pi * np.angle(olg))
                self.line_clg1.setData(self.fr, 20*np.log10(np.abs(1 / (1 + olg))))
                self.line_clg2.setData(self.fr, 20*np.log10(np.abs(olg / (1 + olg))))
                if len(self.reference_ctrl):
                    # update reference plots
                    for i, (r, q) in enumerate(zip(self.reference_ctrl, self.reference_items)):
                        # recompute references
                        ref = r.fresp(self.fr)
                        olg = ref * plant
                        # plot them
                        self.reference_plot_abs[i].setData(self.fr, 20*np.log10(np.abs(olg)))
                        self.reference_plot_phi[i].setData(self.fr, 180/np.pi*np.angle(olg))
                        self.reference_plot_clg1[i].setData(self.fr, 20*np.log10(np.abs(1/(1 + olg))))
                        self.reference_plot_clg2[i].setData(self.fr, 20*np.log10(np.abs(olg / (1 + olg))))


    def click_abs_curve(self, ev):#, click):
        '''
        React to a click on the olg curve, to add poles and zeros or to set where the gain should be locked
        '''
        ev, click = ev
        if self.bFitAddCmpxPole.isChecked():
            # add current tf to undo stack
            self.undo.put(self.ctrl.copy())
            if self.lock_gain_freq is None:
                # add a pole maintaining the gain at DC, with Q=1/sqrt(2)
                self.ctrl.add_pole_fQ(10**click.pos().x(), 1/np.sqrt(2))
            else:
                # add a pole maintaining the gain at the lock frequency
                gain = np.abs(self.ctrl.fresp(self.lock_gain_freq))
                self.ctrl.add_pole_fQ(10 ** click.pos().x(), 1 / np.sqrt(2))
                self.ctrl.set_gain(self.lock_gain_freq, gain)
        if self.bFitAddCmpxZero.isChecked():
            # add current tf to undo stack
            self.undo.put(self.ctrl.copy())
            if self.lock_gain_freq is None:
                # add a zero maintaining the gain at DC, with Q=1/sqrt(2)
                self.ctrl.add_zero_fQ(10**click.pos().x(), 1/np.sqrt(2))
            else:
                # add a zero maintaining the gain at the lock frequency
                gain = np.abs(self.ctrl.fresp(self.lock_gain_freq))
                self.ctrl.add_zero_fQ(10**click.pos().x(), 1/np.sqrt(2))
                self.ctrl.set_gain(self.lock_gain_freq, gain)
        if self.bFitAddRealPole.isChecked():
            # add current tf to undo stack
            self.undo.put(self.ctrl.copy())
            if self.lock_gain_freq is None:
                # add a pole maintaining the gain at DC
                self.ctrl.add_pole_fQ(10**click.pos().x(), 0)
            else:
                # add a pole maintaining the gain at the lock frequency
                gain = np.abs(self.ctrl.fresp(self.lock_gain_freq))
                self.ctrl.add_pole_fQ(10**click.pos().x(), 0)
                self.ctrl.set_gain(self.lock_gain_freq, gain)
        if self.bFitAddRealZero.isChecked():
            # add current tf to undo stack
            self.undo.put(self.ctrl.copy())
            if self.lock_gain_freq is None:
                # add a zero maintaining the gain at DC
                self.ctrl.add_zero_fQ(10**click.pos().x(), 0)
            else:
                # add a zero maintaining the gain at the lock frequency
                gain = np.abs(self.ctrl.fresp(self.lock_gain_freq))
                self.ctrl.add_zero_fQ(10**click.pos().x(), 0)
                self.ctrl.set_gain(self.lock_gain_freq, gain)
        if self.bFitLockGain.isChecked() and self.lock_gain_freq is None:
            # if the lock gain button is selected and we don't have a lock gain frequency, add itt
            self.lock_gain_freq = 10**click.pos().x()
            self.lock_gain_value = abs(self.ctrl.fresp(self.lock_gain_freq))
        # update all plots and graphs
        self.tf_graph.updateTF(self.ctrl)
        self.plot()
        
    def export_ctrl(self):
        """
        Open a dialog and print the foton command string and ZPK
        """

        # Create a new window with grid layout
        q = QtWidgets.QWidget()
        q.resize(800,600)
        g = QtWidgets.QGridLayout()
        q.setLayout(g)
        # two combo boxes to select what to export
        v = QtWidgets.QHBoxLayout()
        g.addLayout(v, 1, 1)
        label1 = QtWidgets.QLabel('Show controller')
        v.addWidget(label1)
        combo1 = QtWidgets.QComboBox()
        combo1.addItem('Current')
        for i in self.reference_items:
            combo1.addItem(i.text())
        combo1.setCurrentIndex(0)
        v.addWidget(combo1)
        label2 = QtWidgets.QLabel('divided by')
        v.addWidget(label2)
        combo2 = QtWidgets.QComboBox()
        combo2.addItem('None')
        for i in self.reference_items:
            combo2.addItem(i.text())
        combo2.setCurrentIndex(0)
        v.addWidget(combo2)
        v.addStretch()

        # text boxes
        label3 = QtWidgets.QLabel('Foton design string')
        g.addWidget(label3, 2, 1)
        label4 = QtWidgets.QLabel('ZPK description')
        g.addWidget(label4, 4, 1)
        txt_foton = QtWidgets.QTextEdit()
        txt_foton.setFont(pg.QtGui.QFont('Monotype'))
        txt_zpk = QtWidgets.QTextEdit()
        txt_zpk.setFont(pg.QtGui.QFont('Monotype'))
        g.addWidget(txt_foton, 3, 1)
        g.addWidget(txt_zpk, 5, 1)

        # button
        done_button = QtWidgets.QPushButton("Done", q)
        g.addWidget(done_button, 6, 1)
        done_button.clicked.connect(q.close)

        # function to update text boxes
        def update():
            # find the controllers to be divided out
            idx1 = combo1.currentIndex()   # numerator
            idx2 = combo2.currentIndex()   # denominator
            if idx1 == 0:
                num = self.ctrl.copy()
            else:
                num = self.reference_ctrl[idx1-1].copy()
            if idx2 == 0:
                den = TF([], [], 1, zero_to_nan=True)
            else:
                den = self.reference_ctrl[idx2-1].copy()
            # divide
            exp_ctrl = TF(np.concatenate([num.z, den.p]),
                          np.concatenate([num.p, den.z]),
                          num.k/den.k, zero_to_nan=True)
            # remove redundant poles and zeros
            exp_ctrl = exp_ctrl.simplify()
            
            try:
                fs = float(fs_finput.text())
            except:
                fs = 16384
                fs_finput.setText('16384')

            # generate foton string
            try:
                import foton
                filt = foton.FilterDesign(16384)
                res = filt.set_zpk(exp_ctrl.z, exp_ctrl.p, exp_ctrl.k)
                if res:
                    # update text box
                    txt_foton.setText(filt.design)
                else:
                    txt_foton.setText('foton did not return a valid design string')
            except:
                txt_foton.setText('Failed to import foton library')

            # generate ZPK description
            s = "\nLaplace domain:\n\n"
            s += 'Z = \n' + np.array2string(exp_ctrl.z, separator=',') + '\n'
            s += 'P = \n' + np.array2string(exp_ctrl.p, separator=',') + '\n'
            s += 'K = \n' + str(exp_ctrl.k) + '\n'
            s += "\nFrequency domain [Hz]:\n"
            s += "Gain: %8.3e\n" % exp_ctrl.k
            s += "Poles\t     f\t       Q\n"
            s += "-----------------------------------\n"
            for f, q in zip(exp_ctrl.p_f, exp_ctrl.p_Q):
                s += "\t%8.3f\t%8.3f\n" % (f, q)
            s += "Zeros\t     f\t       Q\n"
            s += "-----------------------------------\n"
            for f, q in zip(exp_ctrl.z_f, exp_ctrl.z_Q):
                s += "\t%8.3f\t%8.3f\n" % (f, q)
            # update text box
            txt_zpk.setText(s)

        # text box to set sampling frequency
        label5 = QtWidgets.QLabel('Sampling frequency [Hz]')
        v.addWidget(label5)
        fs_finput = QtWidgets.QLineEdit()
        fs_finput.setText('16384')
        fs_finput.setFixedWidth(70)
        fs_finput.editingFinished.connect(update)
        v.addWidget(fs_finput)
        v.addStretch()

        # update text fields
        update()
        # connect update to changes of the combo boxes
        combo1.activated.connect(update)
        combo2.activated.connect(update)

        # open the window and wait for it to close
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()

    def print_plant(self):
        """
        Print the plant to terminal
        """
        print("\n\n*** Plant ***")
        print("\nLaplace domain:\n")
        print('Z = \n' + np.array2string(self.plant.z, separator=','))
        print('P = \n' + np.array2string(self.plant.p, separator=','))
        print('K = \n' + str(self.plant.k))
        print("\nFrequency domain [Hz]:")
        print("Gain: %8.3e" % self.plant.k)
        print("Poles\t     f\t            Q")
        print("-----------------------------------")
        for f, q in zip(self.plant.p_f, self.plant.p_Q):
            print("\t%8.3f\t%8.3f" % (f, q))
        print("Zeros\t     f\t            Q")
        print("-----------------------------------")
        for f, q in zip(self.plant.z_f, self.plant.z_Q):
            print("\t%8.3f\t%8.3f" % (f, q))

    def edit_zpk(self):
        """
        Open a new window with editable lists of pole and zero frequencies and Qs
        """

        # work on a copy of the transfer function
        tf = self.ctrl.copy()

        # Create a new window with grid layout
        q = QtWidgets.QWidget()
        g = QtWidgets.QGridLayout()
        q.setLayout(g)

        # make four lists and link scrollbars between frequency and Q, and labels
        list_zeros_f = QtWidgets.QListWidget(q)
        list_zeros_f.setAlternatingRowColors(True)
        list_zeros_q = QtWidgets.QListWidget(q)
        list_zeros_q.setAlternatingRowColors(True)
        label_zeros_f = QtWidgets.QLabel('Zero Frequencies [Hz]')
        label_zeros_q = QtWidgets.QLabel('Zero Qs')
        list_zeros_f.verticalScrollBar().valueChanged.connect(list_zeros_q.verticalScrollBar().setValue)
        list_zeros_q.verticalScrollBar().valueChanged.connect(list_zeros_f.verticalScrollBar().setValue)
        list_poles_f = QtWidgets.QListWidget(q)
        list_poles_f.setAlternatingRowColors(True)
        list_poles_q = QtWidgets.QListWidget(q)
        list_poles_q.setAlternatingRowColors(True)
        label_poles_f = QtWidgets.QLabel('Pole Frequencies [Hz]')
        label_poles_q = QtWidgets.QLabel('Pole Qs')
        list_poles_f.verticalScrollBar().valueChanged.connect(list_poles_q.verticalScrollBar().setValue)
        list_poles_q.verticalScrollBar().valueChanged.connect(list_poles_f.verticalScrollBar().setValue)
        g.addWidget(list_zeros_f, 1, 0)
        g.addWidget(list_zeros_q, 1, 1)
        g.addWidget(list_poles_f, 3, 0)
        g.addWidget(list_poles_q, 3, 1)
        g.addWidget(label_zeros_f, 0, 0)
        g.addWidget(label_zeros_q, 0, 1)
        g.addWidget(label_poles_f, 2, 0)
        g.addWidget(label_poles_q, 2, 1)
        # link selections between frequency and Q
        list_zeros_f.currentRowChanged.connect(list_zeros_q.setCurrentRow)
        list_poles_f.currentRowChanged.connect(list_poles_q.setCurrentRow)
        list_zeros_q.currentRowChanged.connect(list_zeros_f.setCurrentRow)
        list_poles_q.currentRowChanged.connect(list_poles_f.setCurrentRow)

        # add text input for gain
        def gain_change(x):
            """
            Interpreter the gain input as float adn set TF gain accordingly
            """
            try:
                tf.k = float(x)
            except:
                pass
        label_gain = QtWidgets.QLabel('Gain (k)')
        label_gain.setAlignment(QtCore.Qt.AlignRight)
        g.addWidget(label_gain, 4, 0)
        gain = QtWidgets.QLineEdit()
        gain.setValidator(pg.QtGui.QDoubleValidator())
        gain.textChanged.connect(gain_change)
        gain.setText(str(tf.k))
        g.addWidget(gain, 4, 1)

        # add buttons at the bottom
        button_done = QtWidgets.QPushButton("Done", q)
        button_cancel = QtWidgets.QPushButton("Cancel", q)
        g.addWidget(button_done, 6, 0)
        g.addWidget(button_cancel, 6, 1)

        # buttons to add / remove zero
        button_zero_layout = QtWidgets.QVBoxLayout()
        button_delete_zero = QtWidgets.QPushButton("Delete", q)
        button_add_zero = QtWidgets.QPushButton("Add", q)
        button_zero_layout.addWidget(button_delete_zero)
        button_zero_layout.addWidget(button_add_zero)
        button_zero_layout.addStretch()
        g.addLayout(button_zero_layout, 1,2)

        def add_zero():
            """
            Method called when the Add zero button is called
            """
            # Add at the bottom, f=0, Q=0
            list_zeros_f.addItem('0.0' )
            list_zeros_q.addItem('0.0' )
            # select added item and make it editable
            list_zeros_f.setCurrentRow(list_zeros_f.count() - 1)
            item = list_zeros_f.currentItem()
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            item = list_zeros_q.currentItem()
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            # add to transfer function
            tf.z_f = np.append(tf.z_f, 0)
            tf.z_Q = np.append(tf.z_Q, 0)
        # connect method to button click
        button_add_zero.clicked.connect(add_zero)
        def delete_zero():
            """
            Method called when the Delet buttton is clicked
            """
            # find current item and remove it
            i = list_zeros_f.currentRow()
            list_zeros_f.takeItem(i)
            list_zeros_q.takeItem(i)
            # remove from transfer function
            tf.z_f = np.delete(tf.z_f, i)
            tf.z_Q = np.delete(tf.z_Q, i)
        # connect method to button click
        button_delete_zero.clicked.connect(delete_zero)

        # buttons to add / remove pole
        button_pole_layout = QtWidgets.QVBoxLayout()
        button_delete_pole = QtWidgets.QPushButton("Delete", q)
        button_add_pole = QtWidgets.QPushButton("Add", q)
        button_pole_layout.addWidget(button_delete_pole)
        button_pole_layout.addWidget(button_add_pole)
        button_pole_layout.addStretch()
        g.addLayout(button_pole_layout, 3,2)

        def add_pole():
            """
                   Method called when the Add pole button is called
            """
            # Add at the bottom, f=0, Q=0
            list_poles_f.addItem('0.0' )
            list_poles_q.addItem('0.0' )
            # select added item and make it editable
            list_poles_f.setCurrentRow(list_poles_f.count()-1)
            item = list_poles_f.currentItem()
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            item = list_poles_q.currentItem()
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            # add to transfer function
            tf.p_f = np.append(tf.p_f, 0)
            tf.p_Q = np.append(tf.p_Q, 0)
        # connect method to button click
        button_add_pole.clicked.connect(add_pole)
        def delete_pole():
            """
            Method called when the Delet buttton is clicked
            """
            # find current item and remove it
            i = list_poles_f.currentRow()
            list_poles_f.takeItem(i)
            list_poles_q.takeItem(i)
            # remove from transfer function
            tf.p_f = np.delete(tf.p_f, i)
            tf.p_Q = np.delete(tf.p_Q, i)
        # connect method to button click
        button_delete_pole.clicked.connect(delete_pole)

        # fill lists with values from the transfer function
        for f, Q in zip(tf.p_f, tf.p_Q):
            list_poles_f.addItem('%.3f' % f)
            list_poles_q.addItem('%.3f' % Q)
        for f, Q in zip(tf.z_f, tf.z_Q):
            list_zeros_f.addItem('%.3f' % f)
            list_zeros_q.addItem('%.3f' % Q)
        # set initial selection for poles and zeros
        list_zeros_q.setCurrentRow(0)
        list_poles_q.setCurrentRow(0)

        # make all items editable
        for i in range(list_poles_f.count()):
            item = list_poles_f.item(i)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
        for i in range(list_poles_q.count()):
            item = list_poles_q.item(i)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
        for i in range(list_zeros_f.count()):
            item = list_zeros_f.item(i)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
        for i in range(list_zeros_q.count()):
            item = list_zeros_q.item(i)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)

        # methods to handle changes in the poles and zeros
        def modified_pole_f(ev):
            try:
                # interpreter as float
                f = float(ev.text())
                if f >= 0:
                    # update pole frequency only if frequency is >= 0
                    tf.p_f[list_poles_f.currentRow()] = f
                else:
                    # wrong frequency, reset to previous value
                    list_poles_f.currentItem().setText('%.3f' % tf.p_f[list_poles_f.currentRow()])
            except:
                # cannot interpreter as float, reset to previous value
                list_poles_f.currentItem().setText('%.3f' % tf.p_f[list_poles_f.currentRow()])
        def modified_pole_q(ev):
            try:
                # interpreter as float
                q = float(ev.text())
                if q >= 0:
                    # update pole Q only if Q is >= 0
                    tf.p_Q[list_poles_q.currentRow()] = q
                else:
                    # wrong Q, reset to previous value
                    list_poles_q.currentItem().setText('%.3f' % tf.p_Q[list_poles_q.currentRow()])
            except:
                # cannot interpreter as float, reset to previous value
                list_poles_q.currentItem().setText('%.3f' % tf.p_Q[list_poles_q.currentRow()])
        def modified_zero_f(ev):
            try:
                # interpreter as float
                f = float(ev.text())
                if f * tf.z_f[list_zeros_f.currentRow()] < 0 and tf.z_Q[list_zeros_f.currentRow()] == 0 :
                    # flip gain if the zero frequency changes sign
                    tf.k = -tf.k
                    gain.setText(str(tf.k))
                tf.z_f[list_zeros_f.currentRow()] = f
            except:
                # cannot interpreter as float, reset to previous value
                list_zeros_f.currentItem().setText('%.3f' % tf.z_f[list_zeros_f.currentRow()])
        def modified_zero_q(ev):
            try:
                # interpreter as float
                q = float(ev.text())
                if q >= 0:
                    # update zero Q only if Q is >= 0
                    tf.z_Q[list_zeros_q.currentRow()] = q
                else:
                    # wrong Q, reset to previous value
                    list_zeros_q.currentItem().setText('%.3f' % tf.z_Q[list_zeros_q.currentRow()])
            except:
                # cannot interpreter as float, reset to previous value
                list_zeros_q.currentItem().setText('%.3f' % tf.z_Q[list_zeros_q.currentRow()])
        # connect methods too item changes
        list_poles_f.itemDelegate().closeEditor.connect(modified_pole_f)
        list_poles_q.itemDelegate().closeEditor.connect(modified_pole_q)
        list_zeros_f.itemDelegate().closeEditor.connect(modified_zero_f)
        list_zeros_q.itemDelegate().closeEditor.connect(modified_zero_q)

        # meetod to react to click on buttons
        def click_done():
            """
            Called when Done is cliucked, update the transfer function
            """
            # add current TF too undo stack
            self.undo.put(self.ctrl.copy())
            # remember gain if lock gain is set
            if self.lock_gain_freq is not None:
                gain = abs(self.ctrl.fresp(self.lock_gain_freq))
            # set the new z,p,k
            self.ctrl.set_fQ(tf.z_f, tf.z_Q, tf.p_f, tf.p_Q, tf.k)
            # set gain if lock gain is set
            if self.lock_gain_freq is not None:
                self.ctrl.set_gain(self.lock_gain_freq, gain)
            # update all plots and graphs
            self.tf_graph.updateTF(self.ctrl)
            self.plot()
            q.close()
        def click_cancel():
            """
            Called when clicking on Cancel, just close window
            """
            q.close()
        # connect methods
        button_done.clicked.connect(click_done)
        button_cancel.clicked.connect(click_cancel)

        # open the window and wait for it to close
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()

    def import_ctrl(self):
        """
        Open a window to load a controller definition from LIGO foton file and
        data.
        """

        # open the window and wait for it to close
        q = LoadFoton(parent=self)   # using class defined below
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()

    def optimizer(self):
        """
        Open the optimizer window
        """

        # open the window and wait for it to close
        q = Optimizer(parent=self, plant=self.plant, controller=self.ctrl)   # using class defined in optimize.py
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()
        self.tf_graph.updateTF(self.ctrl)
        self.plot()

########################################################################################################################

class WindowKeyEvent(QtWidgets.QMainWindow):
    """
    Class to capture keypress events and pass them to the main application class
    """
    def __init__(self, parent):
        super().__init__()
        # remember parent InteractiveFitting class to pass along keys
        self.parent = parent

    def keyPressEvent(self, event):
        """
        Called when a key is pressed, pass keystroke along to main class
        """
        if isinstance(event, pg.QtGui.QKeyEvent):
            self.parent.key_press(event.key())

########################################################################################################################

class TranferFunctionGraph(pg.GraphItem):
    '''
    Class to handle plots of movable poles and zeros
    '''
    # based on https://github.com/pyqtgraph/pyqtgraph/blob/develop/examples/CustomGraphItem.py
    def __init__(self, ctrl, plant, parent):
        # keep track of which point is beingg dragged
        self.dragPoint = None
        # initial offset between click position and point position
        self.dragOffset = None
        # keep track of selected point
        self.selected = None
        # transfer functions
        self.tf = ctrl
        self.base = plant
        # remember parent class
        self.parent = parent
        # call class init
        pg.GraphItem.__init__(self)
        # connect click event
        self.scatter.sigClicked.connect(self.clicked)
        # update data from transfer function
        self.setData()

    def updateTF(self, tf):
        """
        Update points from transfer function tf
        """
        self.tf = tf
        self.setData()    
    
    def setData(self):
        """
        Update point coordinates from transfer function poles and zeros
        """
        # list of all frequencies (poles then zeros)
        self.frequencies = np.r_[self.tf.p_f, self.tf.z_f]
        # list of all Qs (poles then zeros)
        self.Q = np.r_[self.tf.p_Q, self.tf.z_Q]
        # list of all indexes in the pole and zeros lists (poles then zeros)
        self.idx = np.r_[np.arange(self.tf.p_f.shape[0]), np.arange(self.tf.z_f.shape[0])]
        # symbols ('star' for poles and 'o' for zeros
        self.symbols = np.array(['star']*self.tf.p_f.shape[0] + ['o']*self.tf.z_f.shape[0])
        # y positions of all points
        self.vals = np.abs(self.tf.fresp(self.frequencies) * self.base.fresp(self.frequencies))
        # if there are zeros with infinite Q, offset the frequency to avoid a value equal to zero
        idx = (self.Q == np.Inf)
        fr = (1 + 1e-4)*self.frequencies
        self.vals[idx] = np.abs(self.tf.fresp(fr) * self.base.fresp(fr))[idx]
        self.pos = np.c_[np.log10(np.abs(self.frequencies)), 20*np.log10(self.vals)]
        # set pens brushes and sizes
        self.pens = []
        self.brushes = []
        self.sizes = []
        # empty symbols for real, solid symbols for complex
        for f,q in zip(self.frequencies, self.Q):
            if q == 0:
                if f >= 0:
                    self.pens.append(pg.mkPen('r'))
                else:
                    self.pens.append(pg.mkPen('b'))
                self.brushes.append(pg.mkBrush('w'))
            else:
                if f >= 0:
                    self.pens.append(pg.mkPen('r'))
                    self.brushes.append(pg.mkBrush('r'))
                else:
                    self.pens.append(pg.mkPen('b'))
                    self.brushes.append(pg.mkBrush('b'))
        # 10 is the nominal marker size
        npts = self.frequencies.shape[0]
        self.sizes = np.array(npts * [10])
        
        # use only poles and zeros with frequency different from zero
        idx = (self.frequencies != 0)
        self.frequencies = self.frequencies[idx]
        self.Q = self.Q[idx]
        self.idx = self.idx[idx]
        self.symbols = self.symbols[idx]
        self.vals = self.vals[idx]
        self.pos = self.pos[idx,:]
        self.sizes = self.sizes[idx]
        self.pens = [p for p,i in zip(self.pens, idx) if i]
        self.brushes = [b for b,i in zip(self.brushes, idx) if i]

        # size 15 to show the selected one as a bigger marker
        if self.selected is not None:
            self.sizes[self.selected] = 15

        # build data dictionary and update graph
        self.data = {'pos': self.pos, 
                     'symbol': self.symbols, 
                     'size': self.sizes,
                     'data': np.empty(npts, dtype=[('index', int)])[idx],
                     'symbolPen': self.pens, 
                     'symbolBrush': self.brushes
                    }
        self.data['data']['index'] = np.arange(self.pos.shape[0])
        self.updateGraph()
        
    def updateGraph(self):
        """
        Set data and update all marker positions
        """
        pg.GraphItem.setData(self, **self.data)
             
    def mouseDragEvent(self, ev):
        """
        Respond to the user dragging a marker
        """
        if ev.button() != QtCore.Qt.MouseButton.LeftButton:
            # not the left mouse button, ignore
            ev.ignore()
            return
        if self.parent.bFitMovePZ.isChecked():
            # do something only if the Move Poles/Zeros is selected
            if ev.isStart():
                self.parent.plot_abs.disableAutoRange()
                self.parent.plot_phi.disableAutoRange()
                self.parent.plot_clg1.disableAutoRange()
                self.parent.plot_clg2.disableAutoRange()

                # We are already one step into the drag.
                # Find the point(s) at the mouse cursor when the button was first 
                # pressed:
                pos = ev.buttonDownPos()
                pts = self.scatter.pointsAt(pos)
                if len(pts) == 0:
                    ev.ignore()
                    return
                self.dragPoint = pts[0]      # point being dragged
                self.ind = pts[0].data()[0]  # corresponding index
                # create copy of the transfer function without the pole or zero
                self.tf1 = self.tf.copy()
                if self.symbols[self.ind] == 'star':
                    self.tf1.remove_pole_fQ(self.frequencies[self.ind], self.Q[self.ind])
                else:
                    self.tf1.remove_zero_fQ(self.frequencies[self.ind], self.Q[self.ind])
                # compute frequency response of this base TF and the plant
                self.base_tf = self.tf1.fresp(self.parent.fr)   # this is the controller TF without the current p/z
                self.plant_tf = self.base.fresp(self.parent.fr) # this is the plant
                # frequency bin and Laplace variables
                self.fr = self.parent.fr
                self.s = 2j*np.pi*self.fr
                # frequency bin scale and offset, used to quickly convert frequency to array index
                self.fr_scale = (np.log10(self.fr[-1]) - np.log10(self.fr[0])) / self.fr.shape[0]
                self.fr_offset = np.log10(self.fr[0])
                # start with new gain and frequency of the pole or zero being moved
                self.new_k = 1
                self.new_freq = self.frequencies[self.ind]
                # save the sign of the frequency (needed for zeros with frequency <0)
                self.sign = np.sign(self.frequencies[self.ind])
                # start with same Q
                self.new_q = self.Q[self.ind]
                # offset between the initial drag point and the marker position
                self.dragOffset = self.data['pos'][self.ind] - pos
                # if the gain is locked at some frequency use that, otherwise lock at 0 Hz
                if self.parent.lock_gain_freq is not None:
                    self.lock_gain_freq = self.parent.lock_gain_freq
                else:
                    # decide at what frequency we can keep the gain constant. We pick 1/100th of the lowest pole or zero
                    frs = np.r_[self.tf1.z_f, self.tf1.p_f]
                    self.lock_gain_freq = frs[frs != 0].min()/100
                # compute gain lock value without the moving pole / zero (controller only, no plant)
                self.lock_gain_value = np.abs(self.tf.fresp(self.lock_gain_freq) / self.tf1.fresp(self.lock_gain_freq))
            elif ev.isFinish():
                # the user released the mouse button, so drag is over
                self.dragPoint = None
                # we're done here, so we need to transfer the update to the fit object
                if self.symbols[self.ind] == 'star':
                    # add pole to transfer function and set gain
                    self.tf1.add_pole_fQ(self.new_freq, self.new_q)
                    self.tf1.set_gain(self.parent.fr[0], np.abs(self.base_tf * self.pole)[0])
                else:
                    # add zero to transfer function and set gain
                    self.tf1.add_zero_fQ(self.new_freq, self.new_q)
                    self.tf1.set_gain(self.parent.fr[0], np.abs(self.base_tf * self.zero)[0])
                # add previous TF to undo stack
                self.parent.undo.put(self.parent.ctrl.copy())
                # move new transfer function to main transfer function and parent
                self.tf = self.tf1.copy()
                self.setData()
                self.parent.ctrl = self.tf
                # replot
                self.parent.plot()
                # clear status message
                self.parent.status_bar.showMessage('')
                return
            else:
                # if we're dragging out of a point just forget about it
                if self.dragPoint is None:
                    ev.ignore()
                    return

            # here we are in the middle of the drag, update pole/zero values and fit curves
            # get new frequency and peak value
            pos = ev.pos() + self.dragOffset
            new_freq = self.sign * 10**pos.x()
            new_peak = 10**(pos.y()/20)
            # find value of base transfer function and relative peak value at the new frequency
            idx = int(np.round((np.log10(abs(new_freq)) - self.fr_offset)/self.fr_scale)) # quick frequency to index
            base = self.base_tf[idx] * self.plant_tf[idx]
            # peak of the new pole or zero is the ratio of drag position over base TF and plant
            new_peak = abs(new_peak/base)
            # compute new frequency and Q
            if self.Q[self.ind] == 0:
                # real pole or zero, change only frequency
                self.new_q = 0
                self.new_freq = new_freq
                if self.symbols[self.ind] == 'star':
                    # real pole, compute gain to maintain gain at lock frequency
                    self.new_k = self.lock_gain_value * np.abs((2j * np.pi * self.lock_gain_freq +
                                                                2 * np.pi * self.new_freq))
                    # transfer function of the pole for fit plots and residual plots
                    self.pole = self.new_k / (self.s + 2 * np.pi * self.new_freq)
                    # update plot curves
                    olg = self.base_tf * self.plant_tf * self.pole
                    self.parent.line_olg_abs.setData(self.fr, 20*np.log10(np.abs(olg)))
                    self.parent.line_olg_phi.setData(self.fr, 180 / np.pi * np.angle(olg))
                    self.parent.line_clg1.setData(self.parent.fr, 20*np.log10(np.abs(1 / (1 + olg))))
                    self.parent.line_clg2.setData(self.parent.fr, 20*np.log10(np.abs(olg / (1 + olg))))

                    # set the marker position to follow the real pole position
                    pos.setX(np.log10(abs(self.new_freq)))
                    pos.setY(20*np.log10(base * np.abs(self.new_k /
                                                    (2j * np.pi * self.new_freq + 2 * np.pi * self.new_freq))))

                    # update all markers
                    # if there are zeros with infinite Q, wiggle around the frequency to avoid a value equal to zero
                    idx = (self.Q == np.Inf)
                    fr = self.frequencies.copy()
                    fr[idx] = (1 + 1e-4) * self.frequencies[idx]
                    plant = self.parent.plant.fresp(fr)
                    vals = np.abs(plant * self.tf1.fresp(fr) * self.new_k /
                                  (2j * np.pi * fr + 2 * np.pi * self.new_freq))
                    self.data['pos'] = np.c_[np.log10(np.abs(fr)), 20*np.log10(vals)]
                    self.data['pos'][self.ind] = pos + self.dragOffset
                    # update plant markers
                    frequencies = np.r_[self.parent.plant.p_f, self.parent.plant.z_f]
                    plant = self.parent.plant.fresp(frequencies)
                    vals_plant = np.abs(plant * self.tf1.fresp(frequencies) * self.new_k /
                                  (2j * np.pi * frequencies + 2 * np.pi * self.new_freq))
                    self.parent.plant_graph.setData( pos=np.c_[np.log10(np.abs(frequencies)), 20*np.log10(vals_plant)])

                    # check stability by adding new pole
                    tf2 = self.tf1.copy()
                    tf2.add_pole_fQ(self.new_freq, self.new_q)
                    tf2.set_gain(self.parent.fr[0], np.abs(self.base_tf * self.pole)[0])
                    self.parent.showStability(self.parent.plant, tf2)
                    # update status bar
                    self.parent.status_bar.showMessage('Moving pole fr=%f Hz, Q=%f' % (self.new_freq, self.new_q))
                    # update additional plots if any
                    self.parent.plot_ctrl(ctrl=tf2)
                    self.parent.plot_impulse(ctrl=tf2)
                    self.parent.plot_step(ctrl=tf2)
                    self.parent.plot_root(ctrl=tf2)
                    self.parent.plot_nichols(ctrl=tf2)
                    self.parent.plot_nyquist(ctrl=tf2)
                else:
                    # real zero, compute gain to maintain gain at lock frequency
                    self.new_k = np.sign(self.new_freq) * (self.lock_gain_value /
                                                           np.abs((2j * np.pi * self.lock_gain_freq
                                                                   + 2 * np.pi * self.new_freq)))
                    # transfer function of the zero for fit plots and residual plots
                    self.zero = self.new_k * (self.s + 2 * np.pi * self.new_freq)
                    # update plot curves
                    olg = self.base_tf * self.plant_tf * self.zero
                    self.parent.line_olg_abs.setData(self.fr, 20*np.log10(np.abs(olg)))
                    self.parent.line_olg_phi.setData(self.fr, 180 / np.pi * np.angle(olg))
                    self.parent.line_clg1.setData(self.parent.fr, 20*np.log10(np.abs(1 / (1 + olg))))
                    self.parent.line_clg2.setData(self.parent.fr, 20*np.log10(np.abs(olg / (1 + olg))))
                    # set the marker position to follow the real zero position
                    pos.setX(np.log10(abs(self.new_freq)))
                    pos.setY(20*np.log10(base * np.abs(self.new_k *
                                                    (2j * np.pi * self.new_freq + 2 * np.pi * self.new_freq))))

                    # update all markers
                    # if there are zeros with infinite Q, wiggle around the frequency to avoid a value equal to zero
                    idx = (self.Q == np.Inf)
                    fr = self.frequencies.copy()
                    fr[idx] = (1 + 1e-4) * self.frequencies[idx]
                    plant = self.parent.plant.fresp(fr)
                    vals = np.abs(plant * self.tf1.fresp(fr) * self.new_k *
                                  (2j * np.pi * fr + 2 * np.pi * self.new_freq))
                    self.data['pos'] = np.c_[np.log10(np.abs(fr)), 20*np.log10(vals)]
                    self.data['pos'][self.ind] = pos + self.dragOffset
                    # update plant markers
                    frequencies = np.r_[self.parent.plant.p_f, self.parent.plant.z_f]
                    plant = self.parent.plant.fresp(frequencies)
                    vals_plant = np.abs(plant * self.tf1.fresp(frequencies) * self.new_k *
                                  (2j * np.pi * frequencies + 2 * np.pi * self.new_freq))
                    self.parent.plant_graph.setData( pos=np.c_[np.log10(np.abs(frequencies)), 20*np.log10(vals_plant)])

                    # check stability by adding new zero
                    tf2 = self.tf1.copy()
                    tf2.add_zero_fQ(self.new_freq, self.new_q)
                    tf2.set_gain(self.parent.fr[0], np.abs(self.base_tf * self.zero)[0])
                    self.parent.showStability(self.parent.plant, tf2)
                    # update status bar
                    self.parent.status_bar.showMessage('Moving zero fr=%f Hz, Q=%f' % (self.new_freq, self.new_q))
                    # update additional plots if any
                    self.parent.plot_ctrl(ctrl=tf2)
                    self.parent.plot_impulse(ctrl=tf2)
                    self.parent.plot_step(ctrl=tf2)
                    self.parent.plot_root(ctrl=tf2)
                    self.parent.plot_nichols(ctrl=tf2)
                    self.parent.plot_nyquist(ctrl=tf2)
            else:
                # complex poles or zeros
                if self.symbols[self.ind] == 'star':
                    # it's a complex pole, maintain gain at given frequency
                    w = 2 * np.pi * self.lock_gain_freq
                    B = self.lock_gain_value
                    # these are the new peak value and frequency
                    w0 = 2 * np.pi * new_freq  # peak frequency
                    A = new_peak  # peak value
                    # compute new gain and new Q
                    k = np.sqrt(B ** 2 * (w0 ** 2 - w ** 2) ** 2 / (1 - B ** 2 / A ** 2 * w ** 2 / w0 ** 2))
                    Q = A * w0 ** 2 / k

                    if not np.isnan(k) and not np.isnan(Q) and not np.isinf(k) and not np.isinf(Q):
                        # update only if the results is not a NaN (NaN happen when the Q would need to be negative)
                        self.new_k = k
                        self.new_q = Q
                        self.new_freq = new_freq
                        # compute the pole transfer function
                        self.pole = self.new_k / (self.s ** 2 + 2 * np.pi * self.new_freq * self.s / self.new_q +
                                                  (2 * np.pi * self.new_freq) ** 2)
                        # update plots
                        olg = self.base_tf * self.plant_tf * self.pole
                        self.parent.line_olg_abs.setData(self.fr, 20*np.log10(np.abs(olg)))
                        self.parent.line_olg_phi.setData(self.fr, 180 / np.pi * np.angle(olg))
                        self.parent.line_clg1.setData(self.parent.fr, 20*np.log10(np.abs(1 / (1 + olg))))
                        self.parent.line_clg2.setData(self.parent.fr, 20*np.log10(np.abs(olg / (1 + olg))))
                        # update all markers
                        # if there are zeros with infinite Q, wiggle around the frequency to avoid a value equal to zero
                        idx = (self.Q == np.Inf)
                        fr = self.frequencies.copy()
                        fr[idx] = (1 + 1e-4) * self.frequencies[idx]
                        plant = self.parent.plant.fresp(fr)
                        vals = np.abs(plant * self.tf1.fresp(fr) *
                                      self.new_k / ((2j * np.pi * fr) ** 2
                                                    + 2 * np.pi * self.new_freq * (
                                                    2j * np.pi * fr) / self.new_q +
                                                    (2 * np.pi * self.new_freq) ** 2))
                        self.data['pos'] = np.c_[np.log10(np.abs(fr)), 20*np.log10(vals)]
                        self.data['pos'][self.ind] = ev.pos() + self.dragOffset
                        # update plant markers
                        frequencies = np.r_[self.parent.plant.p_f, self.parent.plant.z_f]
                        plant = self.parent.plant.fresp(frequencies)
                        vals_plant = np.abs(plant * self.tf1.fresp(frequencies) *
                                      self.new_k / ((2j * np.pi * frequencies) ** 2
                                                    + 2 * np.pi * self.new_freq * (
                                                    2j * np.pi * frequencies) / self.new_q +
                                                    (2 * np.pi * self.new_freq) ** 2))
                        self.parent.plant_graph.setData(pos=np.c_[np.log10(np.abs(frequencies)),
                                                                    20*np.log10(vals_plant)])

                        # check stability by adding new pole
                        tf2 = self.tf1.copy()
                        tf2.add_pole_fQ(self.new_freq, self.new_q)
                        tf2.set_gain(self.parent.fr[0], np.abs(self.base_tf * self.pole)[0])
                        self.parent.showStability(self.parent.plant, tf2)
                        # update status bar
                        self.parent.status_bar.showMessage('Moving pole fr=%f Hz, Q=%f' % (self.new_freq, self.new_q))
                        # update additional plots if any
                        self.parent.plot_ctrl(ctrl=tf2)
                        self.parent.plot_impulse(ctrl=tf2)
                        self.parent.plot_step(ctrl=tf2)
                        self.parent.plot_root(ctrl=tf2)
                        self.parent.plot_nichols(ctrl=tf2)
                        self.parent.plot_nyquist(ctrl=tf2)
                else:
                    # it's a complex zero,  maintain gain at given frequency
                    w = 2 * np.pi * self.lock_gain_freq
                    B = self.lock_gain_value
                    # these are the new peak value and frequency
                    w0 = 2 * np.pi * new_freq  # peak frequency
                    A = new_peak  # peak value
                    # compute new gain and new Q
                    k = 1 / np.sqrt(1 / B ** 2 * (w0 ** 2 - w ** 2) ** 2 / (1 - A ** 2 / B ** 2 * w ** 2 / w0 ** 2))
                    Q = k * w0 ** 2 / A

                    if not np.isnan(k) and not np.isnan(Q) and not np.isinf(k) and not np.isinf(Q):
                        # update only if the results is not a NaN (NaN happen when the Q would need to be negative)
                        self.new_k = k
                        self.new_q = Q
                        self.new_freq = new_freq
                        # compute the zero transfer function
                        self.zero = self.new_k * (self.s ** 2 + 2 * np.pi * self.new_freq * self.s / self.new_q + (
                                2 * np.pi * self.new_freq) ** 2)
                        # update plots
                        olg = self.base_tf * self.plant_tf * self.zero
                        self.parent.line_olg_abs.setData(self.fr, 20*np.log10(np.abs(olg)))
                        self.parent.line_olg_phi.setData(self.fr, 180 / np.pi * np.angle(olg))
                        self.parent.line_clg1.setData(self.parent.fr, 20*np.log10(np.abs(1 / (1 + olg))))
                        self.parent.line_clg2.setData(self.parent.fr, 20*np.log10(np.abs(olg / (1 + olg))))
                        # update all markers
                        # if there are zeros with infinite Q, wiggle around the frequency to avoid a value equal to zero
                        idx = (self.Q == np.Inf)
                        fr = self.frequencies.copy()
                        fr[idx] = (1 + 1e-4) * self.frequencies[idx]
                        plant = self.parent.plant.fresp(fr)
                        vals = np.abs(plant * self.tf1.fresp(fr) * self.new_k *
                                      ((2j * np.pi * fr) ** 2 +
                                       2 * np.pi * self.new_freq * (2j * np.pi * fr) / self.new_q + (
                                               2 * np.pi * self.new_freq) ** 2))
                        self.data['pos'] = np.c_[np.log10(np.abs(fr)), 20*np.log10(vals)]
                        self.data['pos'][self.ind] = ev.pos() + self.dragOffset
                        # update plant markers
                        frequencies = np.r_[self.parent.plant.p_f, self.parent.plant.z_f]
                        plant = self.parent.plant.fresp(frequencies)
                        vals_plant = np.abs(plant * self.tf1.fresp(frequencies) * self.new_k *
                                      ((2j * np.pi * frequencies) ** 2 +
                                       2 * np.pi * self.new_freq * (2j * np.pi * frequencies) / self.new_q + (
                                               2 * np.pi * self.new_freq) ** 2))
                        self.parent.plant_graph.setData( pos=np.c_[np.log10(np.abs(frequencies)),
                                                                        20*np.log10(vals_plant)])
                        # check stability by adding new zero
                        tf2 = self.tf1.copy()
                        tf2.add_zero_fQ(self.new_freq, self.new_q)
                        tf2.set_gain(self.parent.fr[0], np.abs(self.base_tf * self.zero)[0])
                        self.parent.showStability(self.parent.plant, tf2)
                        # update status bar
                        self.parent.status_bar.showMessage('Moving zero fr=%f Hz, Q=%f' % (self.new_freq, self.new_q))
                        # update additional plots if any
                        self.parent.plot_ctrl(ctrl=tf2)
                        self.parent.plot_impulse(ctrl=tf2)
                        self.parent.plot_step(ctrl=tf2)
                        self.parent.plot_root(ctrl=tf2)
                        self.parent.plot_nichols(ctrl=tf2)
                        self.parent.plot_nyquist(ctrl=tf2)
            # update all graphs and plots
            self.updateGraph()
            ev.accept()
        
    def clicked(self, pts, ev):
        """
        Method called when one marker is clicked
        """
        # index of clicked marker
        idx = ev[0].index()

        if self.parent.bFitDelete.isChecked():
            # we are deleting poles and zeros
            if self.symbols[idx] == 'o':
                # delete a zero, add current TTF to undo stack
                self.parent.undo.put(self.parent.ctrl.copy())
                if self.parent.lock_gain_freq is None:
                    # remove the zero
                    self.tf.remove_zero_fQ(self.frequencies[idx], self.Q[idx])
                else:
                    # remove the zero, but maintain gain at lock frequency
                    gain = np.abs(self.tf.fresp(self.parent.lock_gain_freq))
                    self.tf.remove_zero_fQ(self.frequencies[idx], self.Q[idx])
                    self.tf.set_gain(self.parent.lock_gain_freq, gain)
            if self.symbols[idx] == 'star':
                # delete a pole, add current TTF to undo stack
                self.parent.undo.put(self.parent.ctrl.copy())
                if self.parent.lock_gain_freq is None:
                    # remove the pole
                    self.tf.remove_pole_fQ(self.frequencies[idx], self.Q[idx])
                else:
                    # remove the pole, but maintain gain at lock frequency
                    gain = np.abs(self.tf.fresp(self.parent.lock_gain_freq))
                    self.tf.remove_pole_fQ(self.frequencies[idx], self.Q[idx])
                    self.tf.set_gain(self.parent.lock_gain_freq, gain)
            # update graphs and curves
            self.parent.tf_graph.updateTF(self.tf)
            self.parent.plot()
        else:
            # we are clicking to select a marker
            if self.selected is None:
                # none was selected, remember new selection and increase size
                self.selected = idx
                self.data['size'][idx] = 15
            elif self.selected != idx:
                # clicking of a new marker, change selection
                self.data['size'][self.selected] = 10
                self.data['size'][idx] = 15
                self.selected = idx
            else:
                # clicking on the currently selected marker, unselect it
                self.data['size'][self.selected] = 10
                self.selected = None
            if self.selected is not None:
                ## check stability
                #self.parent.showStability()
                # show the frequency and Q in the status bar
                if self.symbols[idx] == 'star':
                    self.parent.status_bar.showMessage(
                        'Selected pole fr=%f Hz, Q=%f' % (self.frequencies[idx], self.Q[idx]))
                else:
                    self.parent.status_bar.showMessage(
                        'Selected zero fr=%f Hz, Q=%f' % (self.frequencies[idx], self.Q[idx]))
            else:
                # clear status bar if nothing is selected
                self.parent.status_bar.clearMessage()
            self.updateGraph()
        # show the status bar widgets to adjust increments for keyboard change, if needed
        if self.selected is not None and self.parent.bFitMovePZ.isChecked():
            self.parent.status_bar_finput.show()
            self.parent.status_bar_flabel.show()
            self.parent.status_bar_qinput.show()
            self.parent.status_bar_qlabel.show()
        else:
            self.parent.status_bar_finput.hide()
            self.parent.status_bar_flabel.hide()
            self.parent.status_bar_qinput.hide()
            self.parent.status_bar_qlabel.hide()


########################################################################################################################
class CustomPlotItem(pg.PlotDataItem):
    """
    Custom class derived from PlotDataItem to handle drag events
    """
    def __init__(self, parent, *args, **kargs):
        super().__init__(*args, **kargs)
        # remember the parent class
        self.parent = parent
        # flag true when draggging the curve
        self.dragging = False
        # Need to switch off the "has no contents" flag (I don't understand this, copied from the Internet)
        self.setFlags(self.flags() & ~self.ItemHasNoContents)

    def mouseDragEvent(self, ev):
        if self.parent.bFitChangeGain.isChecked():
            # accept dragging only when the Change gain button is selected
            if ev.button() != QtCore.Qt.LeftButton:
                # and accept only left mouse buttons
                ev.ignore()
                return
            if ev.isStart():
                self.parent.plot_abs.disableAutoRange()
                # check if the drag started close to the curve
                pos = ev.buttonDownPos()
                scale = self.parent.plot_abs.plotItem.vb.viewPixelSize()
                dist = np.min((self.curve.xData - pos.x())**2/scale[0]**2 + (self.curve.yData - pos.y())**2/scale[1]**2)
                if dist < 10:
                    # drag started close enough to the curve
                    ev.accept()
                    # add current transfer function to the undo stack
                    self.parent.undo.put(self.parent.ctrl.copy())
                    # remember initial click position and gain
                    self.initial_pos = pos.y()
                    self.initial_k = self.parent.ctrl.k
                    self.dragging = True
                else:
                    ev.ignore()
                    return
            elif ev.isFinish():
                # stop dragging and replot
                self.dragging = False
                self.parent.tf_graph.setData()
                ev.accept()
            else:
                if self.dragging:
                    pos = ev.pos()
                    # update the transfer function gain with the new value
                    self.parent.ctrl.set_k(self.initial_k * 10**((pos.y() - self.initial_pos)/20))
                    if self.parent.lock_gain_value is not None:
                        self.parent.lock_gain_value = abs(self.parent.ctrl.fresp(self.parent.lock_gain_freq))
                    self.parent.tf_graph.updateTF(self.parent.ctrl)
                    # replot everything
                    self.parent.plot()
                    ev.accept()
                else:
                    ev.ignore()
        else:
            ev.ignore()

    # methods copied from the internet...
    def shape(self):
        # Inherit shape from the curve item
        return self.curve.shape()

    def boundingRect(self):
        # All graphics items require this method (unless they have no contents)
        return self.shape().boundingRect()

    def paint(self, p, *args):
        # All graphics items require this method (unless they have no contents)
        return

########################################################################################################################
class LoadFoton(QtWidgets.QWidget):
    """
    Load a filter definition from foton files and data.
    """

    def __init__(self, parent=None):
        super().__init__()
        self.parent = parent

        # create GUI
        g = QtWidgets.QGridLayout()
        self.setLayout(g)
        self.setWindowTitle("Load filter definition from file and data")
        # input for filename
        label1 = QtWidgets.QLabel('Filter definition file:')
        self.file = QtWidgets.QLineEdit()
        self.file.setMinimumWidth(300)
        # button to open select dialog
        self.file_button = QtWidgets.QPushButton("Select file...", self)
        self.file_button.setDefault(False)
        self.file_button.clicked.connect(self.open_file)
        g.addWidget(label1, 0, 0)
        g.addWidget(self.file, 0, 1)
        g.addWidget(self.file_button, 0, 2)
        # button to read and parse file
        self.load_button = QtWidgets.QPushButton("Load filter definitions", self)
        g.addWidget(self.load_button, 1, 1, 1, 2    )
        self.load_button.clicked.connect(self.load_file)
        # list of all filter banks in the file
        label2 = QtWidgets.QLabel('Select filter:')
        self.filter_list = QtWidgets.QListWidget()
        self.filter_list.currentRowChanged.connect(self.filter_selected)
        g.addWidget(label2, 2, 0)
        g.addWidget(self.filter_list, 2, 1, 1, 2)
        h = QtWidgets.QHBoxLayout()
        # text input to search among list
        label3 = QtWidgets.QLabel('Match pattern')
        self.match = QtWidgets.QLineEdit()
        g.addLayout(h, 3, 1, 1, 2)
        h.addWidget(label3)
        h.addWidget(self.match)
        self.match.editingFinished.connect(self.select_filters)
        # show buttons and labels for FM1 ... FM10
        label4 = QtWidgets.QLabel('Active filters:')
        g.addWidget(label4, 4, 0)
        fm = QtWidgets.QGridLayout()
        fm.setHorizontalSpacing(1)
        fm.setVerticalSpacing(1)
        self.fm_buttons = []
        self.fm_labels = []
        for i in range(10):
            self.fm_buttons.append(QtWidgets.QPushButton('FM%d' % (i+1)))
            self.fm_labels.append(QtWidgets.QLabel(''))
            self.fm_buttons[-1].setCheckable(True)
            fm.addWidget(self.fm_buttons[-1], 2 * (i // 5) + 1, i % 5)
            fm.addWidget(self.fm_labels[-1], 2 * (i // 5), i % 5)
        g.addLayout(fm, 4,1,1,3)
        # button to load active filters from data
        h = QtWidgets.QHBoxLayout()
        g.addLayout(h, 5, 1, 1, 2)
        self.load_gps = QtWidgets.QPushButton('Load filter configuration from GPS time:')
        self.load_gps.clicked.connect(self.fetch_configuration)
        self.gps = QtWidgets.QLineEdit()
        self.gps.setText('%d' % (gpstime.gpsnow() - 300))
        self.ifo = QtWidgets.QComboBox()
        self.ifo.addItem('H1')
        self.ifo.addItem('L1')
        h.addWidget(self.load_gps)
        h.addWidget(self.gps)
        h.addWidget(self.ifo)
        # check box to flip sign
        self.flip_sign = QtWidgets.QCheckBox('Flip sign')
        self.flip_sign.setChecked(True)
        g.addWidget(self.flip_sign, 6, 0, 1, 3)
        # buttons
        self.ok = QtWidgets.QPushButton('Load controller')
        self.ok.clicked.connect(self.load_controller)
        self.cancel = QtWidgets.QPushButton('Cancel')
        self.cancel.clicked.connect(self.close)
        h = QtWidgets.QHBoxLayout()
        h.addWidget(self.ok)
        h.addWidget(self.cancel)
        g.addLayout(h, 7, 0, 1, 3)

    def open_file(self):
        """
        Open the system dialog to select a file
        """
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open transfer function data file", "",
                                                  "Text Files (*.txt);;All Files (*)")
        if fileName:
            self.file.setText(fileName)

    def load_file(self):
        """
        Load all filter definitions from file
        """
        try:
            import foton
            if self.file.text() != '':
                # if there's a file name, load filter definitions
                self.ff = foton.FilterFile(self.file.text())
                # add all of them to the list
                self.filter_list.clear()
                for k in self.ff.keys():
                    self.filter_list.addItem(k)
            # try to guess IFO from file name
            filename = os.path.split(self.file.text())[-1]
            if 'H1' in filename:
                self.ifo.setCurrentIndex(0)
            elif 'L1' in filename:
                self.ifo.setCurrentIndex(1)
        except:
            print('Cannot import foton library')
            pass

    def select_filters(self):
        """
        Use match pattern to select visible filter definitions
        """
        # loop over all items in list
        for i in range(self.filter_list.count()):
            # make it visible only if the name contains the pattern or is a match for it
            if (self.match.text() in self.filter_list.item(i).text() or
                    fnmatch.fnmatch(self.filter_list.item(i).text(), self.match.text())):
                self.filter_list.item(i).setHidden(False)
            else:
                self.filter_list.item(i).setHidden(True)

    def filter_selected(self):
        """
        Update the FM1 ... FM10 filter bank names
        """
        # get currently selected filter
        filt = self.ff[self.filter_list.currentItem().text()]
        # update labels
        for i in range(10):
            self.fm_labels[i].setText(filt[i].name)

    def fetch_configuration(self):
        """
        Read filter bank configuration from data
        """
        if self.filter_list.currentItem().text() != '':
            # use gwpy to read SWSTAT channel
            from gwpy.timeseries import TimeSeries
            channel = self.filter_list.currentItem().text()
            channel = channel[0:3] + '-' + channel[4:]
            channel = (self.ifo.currentText() + ':' +
                       channel +
                       '_SWSTAT')
            data = TimeSeries.get(channel, int(self.gps.text()), int(self.gps.text())+1)
            # convert value to FM state
            value = data.value[0].astype(int)
            SWSTAT_BITS = [ 1 << 0,
                            1 << 1,
                            1 << 2,
                            1 << 3,
                            1 << 4,
                            1 << 5,
                            1 << 6,
                            1 << 7,
                            1 << 8,
                            1 << 9]
            SWSTAT = dict(zip(self.fm_buttons, SWSTAT_BITS))
            # activate correct buttons
            for button, val in SWSTAT.items():
                if value & val:
                    button.setChecked(True)
                else:
                    button.setChecked(False)

    def load_controller(self):
        """
        Compute the controller corresponding to the current filter bank and active FMs
        """
        if self.filter_list.currentItem().text() != '':
            # active filter
            filt = self.ff[self.filter_list.currentItem().text()]
            Z = []
            P = []
            K = 1
            # add zeros, poles and gain for active FM buttons
            for c,f in zip(self.fm_buttons, filt):
                if c.isChecked():
                    z,p,k = f.get_zpk()
                    Z.append(z)
                    P.append(p)
                    K *= k
            # concatenate all poles and zeros
            Z = np.concatenate(Z)
            P = np.concatenate(P)
            # flip sign if selected
            if self.flip_sign.isChecked():
                K = -K
            # update controller back in main class
            if self.parent is not None:
                self.parent.undo.put(self.parent.ctrl.copy())
                self.parent.ctrl = TF(Z, P, K).simplify(tol=1e-3)
                self.parent.tf_graph.updateTF(self.parent.ctrl)
                self.parent.plot()
                self.close()
            else:
                print(Z, P, K)