# Step by step tutorial for LoopDesigner

## 0) Installation

Please refer to [INSTALL.md](../INSTALL.md) for installation instructions.

## 1) Creating the plant and controller transfer function

When you start LoopDesigner, you need to provide a plant transfer function (and optionally a controller transfer 
function, but yolu can also start with an empty controller, equal to unity).

You can load some example transfer functions with the command

```
from loopdesigner.examples import *
```

This provides you access to two examples. The first is a simple pendulum with a corresponding basic controller:

```
plant = TF([], -2*numpy.pi*numpy.array([0.05-1j, 0.05+1j]), 1)
ctrl = TF([-2.6+8.5j,-2.6-8.5j],[-314.159+314.159j,-314.159-314.159j,-0.0628], 10000000)
```

You can see from this example how you can also define your own transfer function with the `TF` object. The constructor
take the following parameters, from the method help string:

        Construct a transfer function. Zeros, Poles and gain (in Laplace domain)
        can be passed upon construction. Otherwise, the TF is empty (zero).

        The transfer function is defined as
        TF = k * prod(s - z[i]) / prod(s - p[i])

        Parameters
        ----------
        z: numpy.ndarray(complex)
            Optional, list of zeros
        p: numpy.ndarray(complex)
            Optional, list of poles
        k: float
            Optional, gain
        name: str
            Optional, the name of the transfer function
        delay: float
            delay in seconds (will be implemented with a Pade approximation
        pade_order: int
            Pade approximation order
        zero_to_nan: bool
            if True, substitute bins wiht zero value with a NaN
        """

Transfer function objects hjave several useful methods, please refer to [tf.py](../loopdesigner/tf.py) for more 
information.

The second exaaple provided is the LHO O4 DHARD_Y plant and controller, defined in `plant_dhard_y` and `ctrl_dhard_y`.

## 2) Starting the GUI

Let's start the graphical user interface to work on the simple pendulum controller. Again, you first import the 
LoopDesigner class and examples:
```
from loopdesigner import *
from loopdesigner.examples import *
```
Then you can start the GUI with the following command, where you're providing both the plant (necessary) and the
controller (optional):
```commandline
LoopDesigner(plant=plant, controller=ctrl)
```
If you didn't provide the controller, you start with an empty controller (no poles nor zeros) with a gain of one. This 
opens the interface shown below

![GUI](imgs/gui.png)

Let's walk through the information displayed in this main window. There are four plots:
- the left two panels show the open-loop gain transfer function, top is magnitude in db, bottom is phase in degrees. 
  - The convention used in LoopDesigner is such that -1 is the unstable point (there is a minus sign in the feedback 
    path), so the unstable phase is at -180 degrees. 
  - The solid red line shows the open loop gain. The black markers show the poles (stars) and zeros (circles) for the
    plant transfer function (you cannot change them). The red markers show the poles and zeros of the current controller,
    and you can modify them (more on that later). Solid markers are complex poles and zeros, while empty markers are
    real poles and zeros
  - The green lines show the current gain and phase margins: in the magnitude plot you have two vertical green lines 
    that show the gain margins and the corresponding frequencies. Similarly the phase amrgin is shown in green in the
    phase plots.
- the right panels shows the two closed-loop transfer functions: 1/(1+OLG) adn OLG/(1+OLG). The first one is useful to
  estimate teh closed-loop overshoot (peak gain above unity, that tells you how much the control loop is worsening the
  motion). The second closed-loop plot is usefult to estimate noise couplings.

The bottom status bar shows you some information on the current loop. In this particular example the green message reads

<font color=#009900> Stable loop | gain margins: -18 db, +22 db | phase margin: 76 deg | overshoot 1.3 db </font>

summarizing the stability margins and overshoot. If the loop is unstable, this message will turn red.

The toolbar at the top allows you to do all sort of operations on the control loop, as explained below.

## 3) Saving, importing and exporting

Before going into the details of how to modify the controller, let's look at how to export or import results and save 
the current session for future work.

### Saving and loading the session

You can click on "Save sess" in the toolbar to save the current design session to a file. This includes the current
controller, references, plot configuration and all that good stuff. Clicking on the button will open a dialog to allow
you to select a filename.

You can also click "Load sess" to open a previously saved session. Also, if you start `LoopDesigner()` without any 
plant, you'll be asked to select a saved session to open.

### Exporting the results

You can click on "Plant zpk" to print to terminal the current plant:

    *** Plant ***
    
    Laplace domain:
    
    Z = 
    []
    P = 
    [-0.31415927+6.28318531j,-0.31415927-6.28318531j]
    K = 
    1
    
    Frequency domain [Hz]:
    Gain: 1.000e+00
    Poles	     f	            Q
    -----------------------------------
           1.001	  10.012
    Zeros	     f	            Q
    -----------------------------------

Most important, clicking on "Export ctrl" will open a new window to export the current controller

![export](imgs/export.png)

The top bos shows you the LIGO foton design string for the controller, and the bottom box gives you the ZPK 
representation in Lapalce domain and a list of poles and zeros in terms of frequency and Q.

By default, you get the representation of the current controller. However, you can select any of the reference traces
using the combo box "Show controller". The second combo box "divide by" can be left to "None" to print out simply
the controller. You can also select any of the references, if any. In this case the boxes will show you the ratio of the
controller selected in the first combo box divided by the reference selected in the second combo box. This is useful
to implement incremental changes:
1) you save the currently running controller as a reference
2) you do some changes
3) you select "Current" in the first combo box and the initial reference in the second combo box, so to get a 
   representation of the additional filter you need to implement to turn the initial controller into the current
   controller

### Importing a controller from LIGO data

You can also import a controller directly from LIGO foton files and data, by clicking on "Import ctrl". This will open
the dialog below.
1) first select a filter definition file by clicking on "Select file...". An example of the LHO ASC file is provided in
   the `example_data` folder. 
2) click on "Load filter definitions" to load and parse the filter definition file and display all the available filter
   banks
3) all filter banks are listed in the "Select filter:" list box. You can scroll and select one of them, or downselect
   by specifying a match pattern. You can use either a partial name match (`DHARD`), or wildcars (`*Y*`) 
4) click on the filter you want, and the "Active filter" buttons FM1 ... FM10 will populate withe the filter names
5) you can click on any of those buttons to activate that FM, or you can get the configuration that was running at a 
   given GPS time. For that, write the GPS time in the text box, select the IFO, and click on "Load filter 
   configuration from GPS". This uses `gwpy`, you might be asked for your LIGO.ORG kerberos crededential, in the
   terminal.
6) click on "Load controller" to import all active FMs into the current controller

![import](imgs/import.png)

## 4) Keeping track of progress and undo

Everytime you make a change in the controller, the previous configuration is saved, and you can undo the change by
clicking on "Undo". 
Nevertheless, it's good to keep track of progress using reference traces. You can access this feature with the 
"Refs" menu. By clicking on "Add reference" you can save the current controller as a reference trace. You'll be 
prompted for a name ("Initial controller" in this case). The reference is then added to the list in this menu. You can hide a reference by unchecking it. 
You can also delete all selected references, or revert the current controller to the first selected reference.

![references](imgs/references.png)

## 5) Editing the controller

### Moving poles and zeros

When you select "Move z/p" in the toolbar, you can now move any controller pole or zero around. There are two ways of
doing this. You can simply click and drag one of the markers. All curves will update in real time as you move. You might
notice that you cannot drag the marker everywhere: if you mvoe a real pole or zero, all you can do is to change its 
frequency; if you move a complex pole or zero, you cannot make the Q smaller than zero!

You can also click on a pole or zero to select it. its market will become larger, and the status bar will show you
the frequency and Q of that pole or zero. If "Move z/p" is selected, you can use the keyboard arrows to change the
frequency (left and right) and the Q (up and down). The status bar shows the relative increments used for this change.
You can change them by simply typing a new value and pressing enter.

![move](imgs/move_pz.png)

### Keeping gain fixed at a certain frequency

You'll notice than when you move a pole or zero, the low frequency gain is kept constant. In most cases it is more  
useful to keep the gain constant at some other frequency (for example the UGF). To achieve this, select 
"Lock Gain at Freq" and click on the open-loop gain curve at the frequency where you want to gain to be fixed. A black
diamond is shown where you click: as long as the "Lock Gain at Freq" button is selected, any move of poles and zeros
will keep the gain fixed at that value at that frequency.

### Changing the overall gain

You can select "Change gain" and then drag the OLG trace up or down. This will override the "Lock Gain at Freq" 
button during the change. You can also use the keyboard up and down arrows to change the gain. The status bar shows the
increment used, in db.

### Adding or deleting poles and zeros

You can delete any pole or zero by selecting "Delete z/p" and clicking on the corresponding marker.
Conversely, you can add a new pole or zero by selecting ont of the "Add" buttons to add a real or complex pole or zero.
Once selected, just click on the OLG trace where you want to add the pole or zero. Complex pole and zeros are added with
an initial Q equal to 1.

### Adding more complex filters

You can select "Add more" to open a window that allows you to add more complicated features:
- Low pass
- High pass
- Band pass
- Band stop
- Notch
- Resonant gain

![add](imgs/add_feature.png)

For the low / high / bandpass / bandstop filters, you can select the kind of filter (Butterwoth, Elliptical, Chebyshev 1
and 2, Bessel). Depending on the filter, you'll have to specify several parameters (corner frequencies, order, etc.).
For the notch and resonant gain, there's only one kind, but yuo still have to specify the central frequency, the Q and
the depth or height of the feature.

### Direct editing of poles and zeros

You can also edit directly the values of all poles, zeros and gain, by clicking on "Edit ZPK". This will open a new
window with a list of frequency and Q of all poles and zeros. You can edit them directly in the list, or add new poles
and zeros. If you click "Done", the new values are updated back to the main window.

![edit_zpk](imgs/edit_zpk.png)

## 6) Advanced features

### Crosshair

If you toggle "Crosshair" in the toolbar, you'll see that when yopu hover on any plot you get crosshairs that show you
the current frequency and the OLG and CLG values for the current controller and all references. This is useful to 
compare the gain or margings of different controllers

![crosshair](imgs/crosshairs.png)

### Additional plots

You can click on "More plots" to access more plots: 
- Bode plot of the controller
- Bode plot of the plant
- Closed-loop impulse response
- Closed-loop step response
- Nichols plot
- Nyquist plot
- Root locus plot

All those plots are linked with the main window, so any change in poles, zeros or gain will be immediately updated to
the additional plots. This includes all references. Some examples below.

![more_plots](imgs/more_plots1.png)
![more_plots](imgs/more_plots2.png)

The Nichols and Nyquist plots include traces of the overshoot contours: those are the open-loop gain values where the
closed-loop gain will have that overshoot.

![more_plots](imgs/more_plots3.png)

### Optimziation - EXPERIMENTAL

Clicking on "Optimizer" will open a new window that allows you to run automatic optimization of the control loop, based
on constraint and figures of merit. This is still very experimental, not guarantee to succeed or be useful.
Documentation in [OPTIMIZER.md](OPTIMIZER.md)