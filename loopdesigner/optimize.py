# optimize.py - LoopDesigner, Gabriele Vajente (vajente@caltech.edu), 2024-01-14

import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore
from pyqtgraph.Qt import QtWidgets
from pyqtgraph.dockarea.Dock import Dock
from pyqtgraph.dockarea.DockArea import DockArea
import warnings
warnings.filterwarnings('ignore')
from .tf import TF
from queue import LifoQueue
import pickle
import scipy

########################################################################################################################

class Optimizer(QtWidgets.QDialog):
    """
    Graphical user interface to optimize single-input single-output feedback control loops

    Gabriele Vajente (vajente@caltech.edu) - 2024-01-14

    Parameters
    ----------
    plant: TF
        the plant to be controlled
    controller: TF
        optional, the controller transfer function
    parent: LoopDesigner
        optional, parent class
    """

    def __init__(self, plant=None, controller=None, parent=None):
        """
        Class constructor

        Parameters
        ----------
        plant: TF
            the plant to be controlled
        controller: TF
            optional, the controller transfer function
        parent: LoopDesigner
            optional, parent class
        """

        super().__init__()

        # save variables
        self.plant = plant.copy()         # plant
        self.ctrl = controller.copy()     # controller being optimized
        self.ctrl0 = controller.copy()    # initial controller, kept for reference
        # variables to hold error signal spectrum
        self.fr_errorsignal = None
        self.sp_errorsignal_inloop0 = None
        self.sp_errorsignal_inloop  = None
        self.sp_errorsignal_outloop = None

        # flags
        self.adding_line = 0
        self.adding_what = 0
        self.line = None
        self.temp_line = None
        self.line_no_lower_than = None
        self.line_no_larger_than = None
        self.line_mingain = None
        self.line_maxgain = None
        self.trace_no_lower_than = None
        self.trace_no_larger_than = None
        self.trace_mingain = None
        self.trace_maxgain = None

        # collect results
        self.results = []

        # parent LoopDesigner object
        self.parent = parent

        # frequency bins to use, find the lowest and highest frequencies of poles and zeros and space logarithmically
        self.nfr = 1000  # number of points for the  plots
        self.nfr_constraints = 100  # number of points for the FOM and constraints
        self.fr_update_thr = 0.1    # update plots when zoom changes this much
        self.fr = self.initial_frequency_bins(nfr=self.nfr, factor=3)
        self.fr_stab = self.fr.copy()   # use the same bins for stability computation (but they won't be changed when
                                        # the user zooms

        # create GUI
        g = QtWidgets.QGridLayout()
        self.setLayout(g)
        self.resize(1600, 1000)
        self.setWindowTitle("EXPERIMENTAL Optimizer")
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        # pyqtgraph options and white background
        pg.setConfigOptions(antialias=True)
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')

        # plots
        self.plot_abs = pg.PlotWidget()   # olg absolute value
        self.plot_phi = pg.PlotWidget()   # olg phase
        self.plot_clg1 = pg.PlotWidget()  # 1/(1+OLG)
        self.plot_clg2 = pg.PlotWidget()  # OLG/(1+OLG)
        self.plot_psd = pg.PlotWidget()   # error signal spectrum
        self.plot_abs.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_phi.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_clg1.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_clg2.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_psd.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_abs.showGrid(x=True, y=True, alpha=1)
        self.plot_phi.showGrid(x=True, y=True, alpha=1)
        self.plot_clg1.showGrid(x=True, y=True, alpha=1)
        self.plot_clg2.showGrid(x=True, y=True, alpha=1)
        self.plot_psd.showGrid(x=True, y=True, alpha=1)
        # connect x axis ranges
        self.plot_phi.setXLink(self.plot_abs)
        self.plot_clg1.setXLink(self.plot_abs)
        self.plot_clg2.setXLink(self.plot_clg1)
        self.plot_psd.setXLink(self.plot_clg2)
        # titles and labels
        self.plot_abs.setTitle('Transfer function [magnitude]')
        self.plot_phi.setTitle('Transfer function [phase]')
        self.plot_clg1.setTitle('1/(1+OLG)')
        self.plot_clg2.setTitle('OLG/(1+OLG)')
        self.plot_psd.setTitle('Error signal')
        self.plot_abs.setLabel('left', "Magnitude", units='db')
        self.plot_abs.setLabel('bottom', "Frequency", units='Hz')
        self.plot_phi.setLabel('left', "Phase", units='deg')
        self.plot_phi.setLabel('bottom', "Frequency", units='Hz')
        self.plot_clg1.setLabel('left', "Magnitude", units='db')
        self.plot_clg1.setLabel('bottom', "Frequency", units='Hz')
        self.plot_clg2.setLabel('left', "Magnitude", units='db')
        self.plot_clg2.setLabel('bottom', "Frequency", units='Hz')
        self.plot_psd.setLabel('left', "Spectrum", units='a.u./rHz')
        self.plot_psd.setLabel('bottom', "Frequency", units='Hz')
        # set scales and tick options
        self.plot_abs.setLogMode(x=True, y=False)
        self.plot_phi.setLogMode(x=True, y=False)
        self.plot_clg1.setLogMode(x=True, y=False)
        self.plot_clg2.setLogMode(x=True, y=False)
        self.plot_psd.setLogMode(x=True, y=True)
        self.plot_abs.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_phi.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_clg1.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_clg2.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_psd.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_abs.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.plot_phi.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.plot_clg1.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.plot_clg2.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.plot_psd.getPlotItem().getAxis('left').enableAutoSIPrefix(False)

        # legends
        self.plot_abs.getPlotItem().addLegend(brush='w')
        self.plot_phi.getPlotItem().addLegend(brush='w')
        self.plot_clg1.getPlotItem().addLegend(brush='w')
        self.plot_clg2.getPlotItem().addLegend(brush='w')
        self.plot_psd.getPlotItem().addLegend(brush='w')

        # connect zoom methods
        self.plot_abs.sigXRangeChanged.connect(self.zoom)        # to resample frequency bins
        self.plot_abs.sigYRangeChanged.connect(self.zoom_abs_y)  # to update filled plots

        # main plot area using pyqtgraph DockArea to allow resizing and reshuffling
        self.area = DockArea()
        g.addWidget(self.area, 1, 1)
        # plots
        self.dock_abs = Dock('Magnitude')
        self.dock_phi = Dock("Phase")
        self.dock_clg1 = Dock("1/(1+OLG)")
        self.dock_clg2 = Dock("OLG/(1+OLG)")
        self.dock_psd = Dock('Error signal')
        # right side where buttons and parameters will be
        self.dock_param  = Dock('Optimization', size=(1,1))
        self.dock_bounds = Dock('Bounds and method', size=(1, 1))
        # add docks to area
        self.area.addDock(self.dock_abs, 'left')
        self.area.addDock(self.dock_phi, 'bottom', self.dock_abs)
        self.area.addDock(self.dock_psd, 'bottom', self.dock_phi)
        self.area.addDock(self.dock_clg2, 'above', self.dock_psd)
        self.area.addDock(self.dock_clg1, 'above', self.dock_clg2)
        self.area.addDock(self.dock_bounds, 'right')
        self.area.addDock(self.dock_param, 'above', self.dock_bounds)
        # add plots to docks
        self.dock_abs.addWidget(self.plot_abs)
        self.dock_phi.addWidget(self.plot_phi)
        self.dock_clg1.addWidget(self.plot_clg1)
        self.dock_clg2.addWidget(self.plot_clg2)
        self.dock_psd.addWidget(self.plot_psd)

        # populate the parameters dock with GUI
        g = QtWidgets.QGridLayout()
        g.setSpacing(8)
        w = QtWidgets.QWidget()
        w.setLayout(g)
        self.dock_param.addWidget(w)

        # stability contraints
        lb1 = QtWidgets.QLabel('Constrains')
        lb1.setStyleSheet("font-weight: bold")
        g.addWidget(lb1, g.rowCount(), 1)

        h = QtWidgets.QHBoxLayout()
        self.overshoot_check = QtWidgets.QCheckBox('Stability: maximum overshoot [db]')
        self.overshoot = QtWidgets.QLineEdit("10")
        self.overshoot.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        self.overshoot.setMinimumWidth(100)
        h.addWidget(self.overshoot_check)
        h.addStretch()
        h.addWidget(self.overshoot)
        g.addLayout(h, g.rowCount(), 1)

        h = QtWidgets.QHBoxLayout()
        self.max_clq_check = QtWidgets.QCheckBox('Stability: maximum closed loop pole Q')
        self.max_clq = QtWidgets.QLineEdit("10")
        self.max_clq.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        self.max_clq.setMinimumWidth(100)
        h.addWidget(self.max_clq_check)
        h.addStretch()
        h.addWidget(self.max_clq)
        g.addLayout(h, g.rowCount(), 1)

        h = QtWidgets.QHBoxLayout()
        self.phasemargin_check = QtWidgets.QCheckBox('Stability: minimum phase margin [deg]')
        self.phasemargin = QtWidgets.QLineEdit("30")
        self.phasemargin.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        self.phasemargin.setMinimumWidth(100)
        h.addWidget(self.phasemargin_check)
        h.addStretch()
        h.addWidget(self.phasemargin)
        g.addLayout(h, g.rowCount(), 1)

        h = QtWidgets.QHBoxLayout()
        self.gainmargin_check = QtWidgets.QCheckBox('Stability: minimum gain margin [db]')
        self.gainmargin = QtWidgets.QLineEdit("6")
        self.gainmargin.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        self.gainmargin.setMinimumWidth(100)
        h.addWidget(self.gainmargin_check)
        h.addStretch()
        h.addWidget(self.gainmargin)
        g.addLayout(h, g.rowCount(), 1)

        # separator
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setStyleSheet("color: #888888")
        g.addWidget(line, g.rowCount(), 1)

        # open loop gain constraints
        v = QtWidgets.QVBoxLayout()
        v.setSpacing(0)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.no_lower_than_check = QtWidgets.QCheckBox('Open loop gain no lower than')
        self.no_lower_than_combo = QtWidgets.QComboBox()
        self.no_lower_than_combo.addItem("Current gain")
        self.no_lower_than_combo.addItem("Custom values")
        self.no_lower_than_pick = QtWidgets.QPushButton("Draw")
        self.no_lower_than_pick.setEnabled(False)
        self.no_lower_than_pick.setCheckable(True)
        h.addWidget(self.no_lower_than_check)
        h.addWidget(self.no_lower_than_combo)
        h.addWidget(self.no_lower_than_pick)
        v.addLayout(h)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.no_lower_than_label = QtWidgets.QLabel('      in frequency range (min, max)')
        self.no_lower_than_fmin = QtWidgets.QLineEdit()
        self.no_lower_than_fmin.editingFinished.connect(self.update_text_fields)
        self.no_lower_than_fmin.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        self.no_lower_than_fmax = QtWidgets.QLineEdit()
        self.no_lower_than_fmax.editingFinished.connect(self.update_text_fields)
        self.no_lower_than_fmax.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        h.addWidget(self.no_lower_than_label)
        h.addWidget(self.no_lower_than_fmin)
        h.addWidget(self.no_lower_than_fmax)
        v.addLayout(h)
        v.addStretch()
        def toggle_no_lower_than_pick():
            self.no_lower_than_pick.setEnabled(self.no_lower_than_combo.currentIndex() == 1)
            self.no_lower_than_label.setEnabled(self.no_lower_than_combo.currentIndex() == 0)
            self.no_lower_than_fmin.setEnabled(self.no_lower_than_combo.currentIndex() == 0)
            self.no_lower_than_fmax.setEnabled(self.no_lower_than_combo.currentIndex() == 0)
            self.update_text_fields()
        self.no_lower_than_combo.activated.connect(toggle_no_lower_than_pick)
        g.addLayout(v, g.rowCount(), 1)

        v = QtWidgets.QVBoxLayout()
        v.setSpacing(0)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.no_larger_than_check = QtWidgets.QCheckBox('Open loop gain no larger than')
        self.no_larger_than_combo = QtWidgets.QComboBox()
        self.no_larger_than_combo.addItem("Current gain")
        self.no_larger_than_combo.addItem("Custom values")
        self.no_larger_than_pick = QtWidgets.QPushButton("Draw")
        self.no_larger_than_pick.setEnabled(False)
        self.no_larger_than_pick.setCheckable(True)
        h.addWidget(self.no_larger_than_check)
        h.addWidget(self.no_larger_than_combo)
        h.addWidget(self.no_larger_than_pick)
        v.addLayout(h)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.no_larger_than_label = QtWidgets.QLabel('      in frequency range (min, max)')
        self.no_larger_than_fmin = QtWidgets.QLineEdit()
        self.no_larger_than_fmin.editingFinished.connect(self.update_text_fields)
        self.no_larger_than_fmin.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        self.no_larger_than_fmax = QtWidgets.QLineEdit()
        self.no_larger_than_fmax.editingFinished.connect(self.update_text_fields)
        self.no_larger_than_fmax.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        h.addWidget(self.no_larger_than_label)
        h.addWidget(self.no_larger_than_fmin)
        h.addWidget(self.no_larger_than_fmax)
        v.addLayout(h)
        v.addStretch()
        def toggle_no_larger_than_pick():
            self.no_larger_than_pick.setEnabled(self.no_larger_than_combo.currentIndex() == 1)
            self.no_larger_than_label.setEnabled(self.no_larger_than_combo.currentIndex() == 0)
            self.no_larger_than_fmin.setEnabled(self.no_larger_than_combo.currentIndex() == 0)
            self.no_larger_than_fmax.setEnabled(self.no_larger_than_combo.currentIndex() == 0)
            self.update_text_fields()
        self.no_larger_than_combo.activated.connect(toggle_no_larger_than_pick)
        g.addLayout(v, g.rowCount(), 1)

        # separator
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setStyleSheet("color: #888888")
        g.addWidget(line, g.rowCount(), 1)

        # figures of merit
        lb1 = QtWidgets.QLabel('Figures of merit')
        lb1.setStyleSheet("font-weight: bold")
        g.addWidget(lb1, g.rowCount(), 1)

        # minimize RMS
        v = QtWidgets.QVBoxLayout()
        v.setSpacing(0)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.rms_check = QtWidgets.QCheckBox('Minimize RMS')
        self.load_error_signal_button = QtWidgets.QPushButton("Load in-loop error signal spectrum")
        self.load_error_signal_button.clicked.connect(self.load_error_signal)
        h.addWidget(self.rms_check)
        h.addWidget(self.load_error_signal_button)
        v.addLayout(h)
        g.addLayout(v, g.rowCount(), 1)

        # maximize open loop gain
        v = QtWidgets.QVBoxLayout()
        v.setSpacing(0)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.maxgain_check = QtWidgets.QCheckBox('Maximize gain with respect to')
        self.maxgain_combo = QtWidgets.QComboBox()
        self.maxgain_combo.addItem("Current gain")
        self.maxgain_combo.addItem("Custom values")
        self.maxgain_pick = QtWidgets.QPushButton("Draw")
        self.maxgain_pick.setEnabled(False)
        self.maxgain_pick.setCheckable(True)
        h.addWidget(self.maxgain_check)
        h.addWidget(self.maxgain_combo)
        h.addWidget(self.maxgain_pick)
        v.addLayout(h)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.maxgain_label = QtWidgets.QLabel('      in frequency range (min, max)')
        self.maxgain_fmin = QtWidgets.QLineEdit()
        self.maxgain_fmin.editingFinished.connect(self.update_text_fields)
        self.maxgain_fmin.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        self.maxgain_fmax = QtWidgets.QLineEdit()
        self.maxgain_fmax.editingFinished.connect(self.update_text_fields)
        self.maxgain_fmax.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        h.addWidget(self.maxgain_label)
        h.addWidget(self.maxgain_fmin)
        h.addWidget(self.maxgain_fmax)
        v.addLayout(h)
        v.addStretch()
        def toggle_maxgain_pick():
            self.maxgain_pick.setEnabled(self.maxgain_combo.currentIndex() == 1)
            self.maxgain_label.setEnabled(self.maxgain_combo.currentIndex() == 0)
            self.maxgain_fmin.setEnabled(self.maxgain_combo.currentIndex() == 0)
            self.maxgain_fmax.setEnabled(self.maxgain_combo.currentIndex() == 0)
            self.update_text_fields()
        self.maxgain_combo.activated.connect(toggle_maxgain_pick)
        g.addLayout(v, g.rowCount(), 1)

        # minimize open loop gain
        v = QtWidgets.QVBoxLayout()
        v.setSpacing(0)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.mingain_check = QtWidgets.QCheckBox('Minimize gain with respect to')
        self.mingain_combo = QtWidgets.QComboBox()
        self.mingain_combo.addItem("Current gain")
        self.mingain_combo.addItem("Custom values")
        self.mingain_pick = QtWidgets.QPushButton("Draw")
        self.mingain_pick.setCheckable(True)
        self.mingain_pick.setEnabled(False)
        h.addWidget(self.mingain_check)
        h.addWidget(self.mingain_combo)
        h.addWidget(self.mingain_pick)
        v.addLayout(h)
        h = QtWidgets.QHBoxLayout()
        h.setSpacing(11)
        self.mingain_label = QtWidgets.QLabel('      in frequency range (min, max)')
        self.mingain_fmin = QtWidgets.QLineEdit()
        self.mingain_fmin.editingFinished.connect(self.update_text_fields)
        self.mingain_fmin.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        self.mingain_fmax = QtWidgets.QLineEdit()
        self.mingain_fmax.editingFinished.connect(self.update_text_fields)
        self.mingain_fmax.setValidator(pg.QtGui.QDoubleValidator(0., np.Inf, 3))
        h.addWidget(self.mingain_label)
        h.addWidget(self.mingain_fmin)
        h.addWidget(self.mingain_fmax)
        v.addLayout(h)
        v.addStretch()
        def toggle_mingain_pick():
            self.mingain_pick.setEnabled(self.mingain_combo.currentIndex() == 1)
            self.mingain_label.setEnabled(self.mingain_combo.currentIndex() == 0)
            self.mingain_fmin.setEnabled(self.mingain_combo.currentIndex() == 0)
            self.mingain_fmax.setEnabled(self.mingain_combo.currentIndex() == 0)
            self.update_text_fields()
        self.mingain_combo.activated.connect(toggle_mingain_pick)
        g.addLayout(v, g.rowCount(), 1)

        # relative weights of the three FOMs
        g1 = QtWidgets.QGridLayout()
        l1 = QtWidgets.QLabel('Minimize RMS weight')
        l1.setAlignment(QtCore.Qt.AlignCenter)
        self.rms_weight = QtWidgets.QLineEdit()
        self.rms_weight.setValidator(pg.QtGui.QDoubleValidator(0., 1., 2))
        self.rms_weight.setEnabled(False)
        self.rms_weight.setText('1.0')

        l2 = QtWidgets.QLabel('Maximize gain weight')
        l2.setAlignment(QtCore.Qt.AlignCenter)
        self.maxgain_weight = QtWidgets.QLineEdit()
        self.maxgain_weight.setValidator(pg.QtGui.QDoubleValidator(0., 1., 2))
        self.maxgain_weight.setEnabled(False)
        self.maxgain_weight.setText('1.0')

        l3 = QtWidgets.QLabel('Minimize gain weight')
        l3.setAlignment(QtCore.Qt.AlignCenter)
        self.mingain_weight = QtWidgets.QLineEdit()
        self.mingain_weight.setValidator(pg.QtGui.QDoubleValidator(0., 1., 2))
        self.mingain_weight.setEnabled(False)
        self.mingain_weight.setText('1.0')

        # make them active only when a FOM is defined and checked
        def update_weights():
            self.rms_weight.setEnabled(self.rms_check.isChecked())
            self.mingain_weight.setEnabled(self.mingain_check.isChecked())
            self.maxgain_weight.setEnabled(self.maxgain_check.isChecked())
        self.rms_check.toggled.connect(update_weights)
        self.maxgain_check.toggled.connect(update_weights)
        self.mingain_check.toggled.connect(update_weights)
        g1.addWidget(l1, 1, 1)
        g1.addWidget(l2, 1, 2)
        g1.addWidget(l3, 1, 3)
        g1.addWidget(self.rms_weight, 2, 1)
        g1.addWidget(self.maxgain_weight, 2, 2)
        g1.addWidget(self.mingain_weight, 2, 3)
        g.addLayout(g1, g.rowCount(), 1)

        # separator
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setStyleSheet("color: #888888")
        g.addWidget(line, g.rowCount(), 1)

        # allows adding poles and zeros pairs during optimization
        h = QtWidgets.QHBoxLayout()
        self.add_pz_check = QtWidgets.QCheckBox('Allow adding complex poles/zeros pairs [max number]')
        self.num_add_pz = QtWidgets.QLineEdit()
        self.num_add_pz.setValidator(pg.QtGui.QIntValidator(0, 100))
        self.num_add_pz.setEnabled(False)
        h.addWidget(self.add_pz_check)
        h.addWidget(self.num_add_pz)
        def toggle_num_add_pz():
            self.num_add_pz.setEnabled(self.add_pz_check.isChecked())
        self.add_pz_check.toggled.connect(toggle_num_add_pz)
        g.addLayout(h, g.rowCount(), 1)

        # separator
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setStyleSheet("color: #888888")
        g.addWidget(line, g.rowCount(), 1)

        # label to show the optimization status
        self.status = QtWidgets.QLabel('Optimization status')
        self.status.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.status.setStyleSheet("background-color:rgb(255,255,255)")
        self.status.setFont(pg.QtGui.QFont('Courier', 11))
        self.status.setFixedHeight(8*self.status.fontInfo().pixelSize()+10)
        self.status.setWordWrap(True)
        g.addWidget(self.status, g.rowCount(), 1)

        # separator
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setStyleSheet("color: #888888")
        g.addWidget(line, g.rowCount(), 1)

        # check to show curves in real time
        self.realtime = QtWidgets.QCheckBox('Plot optimization curves in real time (slower!)')
        g.addWidget(self.realtime, g.rowCount(), 1)

        # buttons at the bottom
        self.button_run = QtWidgets.QPushButton("Run Optimization")
        self.button_cancel = QtWidgets.QPushButton("Cancel")
        self.button_accept = QtWidgets.QPushButton("Accept result")
        self.button_revert = QtWidgets.QPushButton("Revert")
        h = QtWidgets.QHBoxLayout()
        h.addWidget(self.button_run)
        h.addWidget(self.button_cancel)
        h.addWidget(self.button_revert)
        h.addWidget(self.button_accept)
        self.button_accept.setDisabled(True)
        self.button_revert.setDisabled(True)
        g.addLayout(h, g.rowCount(), 1)
        self.button_cancel.clicked.connect(self.close)
        self.button_run.clicked.connect(self.optimization)
        self.button_revert.clicked.connect(self.revert)
        self.button_accept.clicked.connect(self.accept)
        g.setRowStretch(g.rowCount(), 1)

        # parameter bounds, in a different tab
        grid = QtWidgets.QGridLayout()
        w = QtWidgets.QWidget()
        w.setLayout(grid)
        self.dock_bounds.addWidget(w)

        label = QtWidgets.QLabel("Allow change in pole and zero frequencies and Qs\nby the amount listed below"
                                 " (absolute or relative)")
        grid.addWidget(label, 1, 0, 1, 3)
        label2 = QtWidgets.QLabel('Relative change\nin frequency (min,max)')
        grid.addWidget(label2, 2, 0)
        self.minfr = QtWidgets.QLineEdit()
        self.minfr.setValidator(pg.QtGui.QDoubleValidator(0.0, 1.0, 3))
        grid.addWidget(self.minfr, 2, 1)
        self.maxfr = QtWidgets.QLineEdit()
        self.maxfr.setValidator(pg.QtGui.QDoubleValidator(1.0, np.Inf, 3))
        grid.addWidget(self.maxfr, 2, 2)
        label3 = QtWidgets.QLabel('Relative change\nin Q (min, max)')
        grid.addWidget(label3, 3, 0)
        self.minq = QtWidgets.QLineEdit()
        self.minq.setValidator(pg.QtGui.QDoubleValidator(0.0, 1.0, 3))
        grid.addWidget(self.minq, 3, 1)
        self.maxq = QtWidgets.QLineEdit()
        self.maxq.setValidator(pg.QtGui.QDoubleValidator(1.0, np.Inf, 3))
        grid.addWidget(self.maxq, 3, 2)

        label3b = QtWidgets.QLabel('Relative change\nin gain (min, max)')
        grid.addWidget(label3b, 4, 0)
        self.ming = QtWidgets.QLineEdit()
        self.ming.setValidator(pg.QtGui.QDoubleValidator(0.0, 1.0, 3))
        grid.addWidget(self.ming, 4, 1)
        self.maxg = QtWidgets.QLineEdit()
        self.maxg.setValidator(pg.QtGui.QDoubleValidator(1.0, np.Inf, 3))
        grid.addWidget(self.maxg, 4, 2)
        self.minfr.setText('%.3f' % 0.5)
        self.maxfr.setText('%.3f' % 2)
        self.minq.setText('%.3f' % 0.5)
        self.maxq.setText('%.3f' % 2)
        self.ming.setText('%.3f' % 0.5)
        self.maxg.setText('%.3f' % 2)
        label4 = QtWidgets.QLabel('Absolute frequency\n(min,max)')
        grid.addWidget(label4, 5, 0)
        self.absminfr = QtWidgets.QLineEdit()
        self.absminfr.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.absminfr, 5, 1)
        self.absmaxfr = QtWidgets.QLineEdit()
        self.absmaxfr.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.absmaxfr, 5, 2)
        label5 = QtWidgets.QLabel('Max Q value')
        grid.addWidget(label5, 6, 0)
        self.absmaxq = QtWidgets.QLineEdit()
        self.absmaxq.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.absmaxq, 6, 1)
        self.norm_lin = QtWidgets.QRadioButton("Linear binning")
        self.norm_log = QtWidgets.QRadioButton("Logarithmic binning")
        self.norm_log.setChecked(True)
        tf_group = QtWidgets.QButtonGroup()
        buttons = QtWidgets.QVBoxLayout()
        tf_group.addButton(self.norm_lin)
        buttons.addWidget(self.norm_lin)
        tf_group.addButton(self.norm_log)
        buttons.addWidget(self.norm_log)
        grid.addLayout(buttons, 7, 1, 1, 2)

        # select optimization methods, not working for now
        self.methods = ['Default'] #, 'COBYLA', 'SLSQP', 'trust-constr']
        self.methods_combo = QtWidgets.QComboBox()
        for i in self.methods:
            self.methods_combo.addItem(i)
        self.methods_combo.setCurrentIndex(0)
        label1 = QtWidgets.QLabel('Optimization method')
        grid.addWidget(label1, 8, 0)
        grid.addWidget(self.methods_combo, 8, 1, 1, 2)

        grid.setRowStretch(g.rowCount(), 1)

        # update some initial values with the current controller properties
        self.overshoot.setText('%.2f' % self.compute_overshoot())   # closed-loop overshoot
        self.max_clq.setText('%.2f' % self.compute_pole_q())        # closed-loop pole maximum Q
        gm, pm = self.margins()
        self.gainmargin.setText('%.1f' % gm)    # phase margin
        self.phasemargin.setText('%.1f' % pm)   # gain margin

        # connect event to handle clicks and line drawing
        self.plot_abs.scene().sigMouseClicked.connect(self.clicked)
        self.proxy2 = pg.SignalProxy(self.plot_abs.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)

        # update plots
        self.fit_update_thr = 0.05    # update frequency samples when zooming more than this
        self.lines = {}
        self.plotted = False
        self.updatePlots()

        # start Qt app full screen
        self.showMaximized()

    def initial_frequency_bins(self, nfr=1000, factor=3):
        """

        Parameters
        ----------
        nfr: int
            number of frequency bins
        factor: float
            enlarge the frequency span by this factor below the lowest pole or
            zero and above the largest pole or zero

        Returns
        -------
        fr: numpy.ndarray
            frequency bins

        """

        # extract all pole and zero frequencies
        freqs = np.concatenate([self.plant.z_f, self.plant.p_f, self.ctrl.z_f, self.ctrl.p_f])
        # remove any frequency equal to zero
        freqs = np.abs(freqs)[freqs != 0]
        if len(freqs):
            # log spacing between the minimum and maximum, with enlarging factor
            fr = np.geomspace(freqs.min() / factor, freqs.max() * factor, nfr)
        else:
            # if there are no poles or zeros, just pick something
            fr = np.geomspace(0.1, 10, nfr)
        return fr

    def updatePlots(self, ctrl=None):
        """
        Update all plots

        Parameters
        ----------
        ctrl: TF
            optional, use this controller. If not provided, use self.ctrl
        """

        # use provided controller or self.ctrl if none is provided
        if ctrl is not None:
            self.ctrl = ctrl

        # disable auto zoom
        self.plot_abs.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_phi.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_clg1.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_clg2.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_psd.disableAutoRange(pg.ViewBox.XAxis)

        # initial trace usign ctrl0
        olg = self.plant.fresp(self.fr) * self.ctrl0.fresp(self.fr)
        if 'olg_abs' not in self.lines.keys():
            # create new traces
            # reference initial controller
            self.lines['olg_abs'] = self.plot_abs.plot(self.fr, 20*np.log10(np.abs(olg)),
                                                       pen=pg.mkPen('r', width=1), name='Initial')
            self.lines['olg_phi'] = self.plot_phi.plot(self.fr, 180/np.pi*np.angle(olg),
                                                       pen=pg.mkPen('r', width=1), name='Initial')
            self.lines['clg1'] = self.plot_clg1.plot(self.fr, 20 * np.log10(np.abs(1/(1+olg))),
                                                       pen=pg.mkPen('r', width=1), name='Initial')
            self.lines['clg2'] = self.plot_clg2.plot(self.fr, 20 * np.log10(np.abs(olg/(1+olg))),
                                                       pen=pg.mkPen('r', width=1), name='Initial')
            # current trace
            olg = self.plant.fresp(self.fr) * self.ctrl.fresp(self.fr)
            self.lines['olg_abs'] = self.plot_abs.plot(self.fr, 20*np.log10(np.abs(olg)),
                                                       pen=pg.mkPen('b', width=1), name='Optimized')
            self.lines['olg_phi'] = self.plot_phi.plot(self.fr, 180/np.pi*np.angle(olg),
                                                       pen=pg.mkPen('b', width=1), name='Optimized')
            self.lines['clg1'] = self.plot_clg1.plot(self.fr, 20 * np.log10(np.abs(1/(1+olg))),
                                                       pen=pg.mkPen('b', width=1), name='Optimized')
            self.lines['clg2'] = self.plot_clg2.plot(self.fr, 20 * np.log10(np.abs(olg/(1+olg))),
                                                       pen=pg.mkPen('b', width=1), name='Optimized')
            # set initial frequency range
            self.plot_abs.setXRange(np.log10(self.fr.min()),np.log10(self.fr.max()) )
            self.plotted = True
        else:
            # traces alredy existing, just update data
            # reference trace
            self.lines['olg_abs'].setData(self.fr, 20*np.log10(np.abs(olg)))
            self.lines['olg_phi'].setData(self.fr, 180/np.pi*np.angle(olg))
            self.lines['clg1'].setData(self.fr, 20 * np.log10(np.abs(1/(1+olg))))
            self.lines['clg2'].setData(self.fr, 20 * np.log10(np.abs(olg/(1+olg))))
            # current trace
            olg = self.plant.fresp(self.fr) * self.ctrl.fresp(self.fr)
            self.lines['olg_abs'].setData(self.fr, 20*np.log10(np.abs(olg)))
            self.lines['olg_phi'].setData(self.fr, 180/np.pi*np.angle(olg))
            self.lines['clg1'].setData(self.fr, 20 * np.log10(np.abs(1/(1+olg))))
            self.lines['clg2'].setData(self.fr, 20 * np.log10(np.abs(olg/(1+olg))))

        # draw constraints and FOMs
        if self.line_no_lower_than is not None:
            # draw line filled above
            x = np.array(self.line_no_lower_than)[:, 0]
            y = np.array(self.line_no_lower_than)[:, 1]
            ymin = self.plot_abs.getViewBox().viewRange()[1][0]
            ymax = self.plot_abs.getViewBox().viewRange()[1][1]
            if self.trace_no_lower_than is None:
                self.trace_no_lower_than = self.plot_abs.plot(10**x, y, pen=pg.mkPen((0, 255, 0, 100), width=3),
                                                            fillLevel=ymax, fillBrush=(0, 255, 0, 20),
                                                            name="Constraint: gain no lower than")
            else:
                self.trace_no_lower_than.setData(10 ** x, y, fillLevel=ymax)
            self.zoom_abs_y()
        if self.line_no_larger_than is not None:
            # draw line filled below
            x = np.array(self.line_no_larger_than)[:, 0]
            y = np.array(self.line_no_larger_than)[:, 1]
            ymin = self.plot_abs.getViewBox().viewRange()[1][0]
            ymax = self.plot_abs.getViewBox().viewRange()[1][1]
            if self.trace_no_larger_than is None:
                self.trace_no_larger_than = self.plot_abs.plot(10**x, y, pen=pg.mkPen((255, 0, 0, 100), width=3),
                                                            fillLevel=ymin, fillBrush=(255, 0, 0, 20),
                                                            name="Constraint: gain no larger than")
            else:
                self.trace_no_larger_than.setData(10 ** x, y, fillLevel=ymin)
        if self.line_mingain is not None:
            # draw thick line
            x = np.array(self.line_mingain)[:, 0]
            y = np.array(self.line_mingain)[:, 1]
            if self.trace_mingain is None:
                self.trace_mingain = self.plot_abs.plot(10**x, y, pen=pg.mkPen((255,0,255,50), width=15),
                                                         name='FOM: minimize gain w.r.t. this')
            else:
                self.trace_mingain.setData(10 ** x, y)
        if self.line_maxgain is not None:
            # draw thick line
            x = np.array(self.line_maxgain)[:, 0]
            y = np.array(self.line_maxgain)[:, 1]
            if self.trace_maxgain is None:
                self.trace_maxgain = self.plot_abs.plot(10**x, y, pen=pg.mkPen((255,200,0,50), width=15),
                                                         name='FOM: maximize gain w.r.t. this')
            else:
                self.trace_maxgain.setData(10 ** x, y)

        # error signals
        if self.fr_errorsignal is not None:
            if 'psd_inloop0' not in self.lines.keys():
                # plot PSD for reference, create new traces
                self.lines['psd_outloop'] = self.plot_psd.plot(self.fr_errorsignal, self.sp_errorsignal_outloop,
                                                               pen=pg.mkPen('k', width=1), name='Out-of-Loop')
                self.lines['psd_inloop0'] = self.plot_psd.plot(self.fr_errorsignal, self.sp_errorsignal_inloop0,
                                                              pen=pg.mkPen('r', width=1), name='In-Loop Initial')

                # update with current controller
                olg = self.ctrl.fresp(self.fr_errorsignal) * self.plant.fresp(self.fr_errorsignal)
                clg = 1 / (1 + olg)
                self.sp_errorsignal_inloop = self.sp_errorsignal_outloop * abs(clg)
                self.lines['psd_inloop'] = self.plot_psd.plot(self.fr_errorsignal, self.sp_errorsignal_inloop,
                                                            pen=pg.mkPen('b', width=1), name='In-Loop Optimized')

                # plot rms
                rms = np.sqrt(np.cumsum(self.sp_errorsignal_inloop0[::-1]**2)[::-1]*\
                              (self.fr_errorsignal[1]-self.fr_errorsignal[0]))
                self.lines['rms_inloop0'] = self.plot_psd.plot(self.fr_errorsignal, rms,
                                                            pen=pg.mkPen('r', width=1, style=QtCore.Qt.DashLine))
                rms = np.sqrt(np.cumsum(self.sp_errorsignal_outloop[::-1] ** 2)[::-1] * \
                              (self.fr_errorsignal[1] - self.fr_errorsignal[0]))
                self.lines['rms_outloop'] = self.plot_psd.plot(self.fr_errorsignal, rms,
                                                            pen=pg.mkPen('k', width=1, style=QtCore.Qt.DashLine))
                rms = np.sqrt(np.cumsum(self.sp_errorsignal_inloop[::-1] ** 2)[::-1] * \
                              (self.fr_errorsignal[1] - self.fr_errorsignal[0]))
                self.lines['rms_inloop'] = self.plot_psd.plot(self.fr_errorsignal, rms,
                                                            pen=pg.mkPen('b', width=1, style=QtCore.Qt.DashLine))
            else:
                # plot PSD, update with new controller, existing traces, so just update data
                olg = self.ctrl.fresp(self.fr_errorsignal) * self.plant.fresp(self.fr_errorsignal)
                clg = 1 / (1 + olg)
                self.sp_errorsignal_inloop = self.sp_errorsignal_outloop * abs(clg)
                self.lines['psd_inloop'].setData(self.fr_errorsignal, self.sp_errorsignal_inloop)
                # plot rms
                rms = np.sqrt(np.cumsum(self.sp_errorsignal_inloop[::-1] ** 2)[::-1] * \
                              (self.fr_errorsignal[1] - self.fr_errorsignal[0]))
                self.lines['rms_inloop'].setData(self.fr_errorsignal, rms)

    def zoom(self):
        """
        Called when frequency range is zoomed or panned
        """
        # minimum and maximum frequencies from the view range
        fmin = 10 ** self.plot_abs.getViewBox().viewRange()[0][0]
        fmax = 10 ** self.plot_abs.getViewBox().viewRange()[0][1]

        # check if the range changed enough to call a resample
        current_fmin = self.fr[0]
        current_fmax = self.fr[-1]
        if fmin / current_fmin < 1 - self.fit_update_thr or \
                fmin / current_fmin > 1 + self.fit_update_thr or \
                fmax / current_fmax < 1 - self.fit_update_thr or \
                fmax / current_fmax > 1 + self.fit_update_thr or \
                (current_fmax - current_fmin) / (fmax - fmin) < 1 - self.fr_update_thr or \
                (current_fmax - current_fmin) / (fmax - fmin) > 1 - self.fr_update_thr:
            # new frequency bins to span all the current range
            self.fr = np.logspace(np.log10(fmin), np.log10(fmax), self.nfr)

            if self.plotted:
                self.plot_abs.disableAutoRange(pg.ViewBox.XAxis)
                self.plot_phi.disableAutoRange(pg.ViewBox.XAxis)
                self.plot_clg1.disableAutoRange(pg.ViewBox.XAxis)
                self.plot_clg2.disableAutoRange(pg.ViewBox.XAxis)
                self.plot_psd.disableAutoRange(pg.ViewBox.XAxis)
                # initial trace
                olg = self.plant.fresp(self.fr) * self.ctrl0.fresp(self.fr)
                self.lines['olg_abs'].setData(self.fr, 20 * np.log10(np.abs(olg)))
                self.lines['olg_phi'].setData(self.fr, 180 / np.pi * np.angle(olg))
                self.lines['clg1'].setData(self.fr, 20 * np.log10(np.abs(1 / (1 + olg))))
                self.lines['clg2'].setData(self.fr, 20 * np.log10(np.abs(olg / (1 + olg))))
                # current trace
                olg = self.plant.fresp(self.fr) * self.ctrl.fresp(self.fr)
                self.lines['olg_abs'].setData(self.fr, 20 * np.log10(np.abs(olg)))
                self.lines['olg_phi'].setData(self.fr, 180 / np.pi * np.angle(olg))
                self.lines['clg1'].setData(self.fr, 20 * np.log10(np.abs(1 / (1 + olg))))
                self.lines['clg2'].setData(self.fr, 20 * np.log10(np.abs(olg / (1 + olg))))

    def zoom_abs_y(self):
        """
        Called when the y range is changed in the abs plot, to replot filled traces
        """
        if self.trace_no_lower_than is not None:
            # replot filled trace to the top of the y range
            x = np.array(self.line_no_lower_than)[:, 0]
            y = np.array(self.line_no_lower_than)[:, 1]
            ymin = self.plot_abs.getViewBox().viewRange()[1][0]
            ymax = self.plot_abs.getViewBox().viewRange()[1][1]
            self.plot_abs.disableAutoRange(pg.ViewBox.YAxis)
            self.trace_no_lower_than.setData(10**x, y, pen=pg.mkPen((0, 255, 0, 100), width=3),
                                                            fillLevel=ymax, fillBrush=(0, 255, 0, 20))
        if self.trace_no_larger_than is not None:
            # replot filled trace to the bottom of the y range
            x = np.array(self.line_no_larger_than)[:, 0]
            y = np.array(self.line_no_larger_than)[:, 1]
            ymin = self.plot_abs.getViewBox().viewRange()[1][0]
            ymax = self.plot_abs.getViewBox().viewRange()[1][1]
            self.plot_abs.disableAutoRange(pg.ViewBox.YAxis)
            self.trace_no_larger_than.setData(10**x, y, pen=pg.mkPen((255, 0, 0, 100), width=3),
                                                            fillLevel=ymin, fillBrush=(255, 0, 0, 20))

    def closed_loop_poles(self, plant=None, ctrl=None):
        """
        Compute the close loop poles of the current feedback control system

        Parameters
        ----------
        plant: TF
            if passed, overrides object plant
        ctrl: TF
            if passed, overrides object controller

        Returns
        -------
        poles: numpy.ndarray
            complex poles in s-domain
        """

        if plant is None:
            plant = self.plant
        if ctrl is None:
            ctrl = self.ctrl

        # closed loop response is
        #
        # 1/(1+OLG) = plant.den * ctrl.den / (plant.den * ctrl.den + plan.num * ctrl.num)
        #
        # compute the denominator of the closed loop response
        den = np.polyadd(np.polymul(plant.den, ctrl.den), np.polymul(plant.num, ctrl.num))
        # return poles (roots of denominators)
        return np.roots(den)

    def compute_overshoot(self, ctrl=None):
        """
        Return the overshoot of the current controller

        Parameters
        ----------
        ctrl: TF
            controller

        Returns
        -------
        overshoot: float
            closed loop overshoot in db
        """
        if ctrl is None:
            ctrl = self.ctrl
        # compute closed loop poles
        poles = self.closed_loop_poles(ctrl=ctrl)
        freqs, Q = TF().s_to_fQ(poles)
        freqs= np.abs(freqs)[freqs != 0]
        # sample closed loop gain
        fr = np.geomspace(freqs.min()/10, freqs.max()*10, self.nfr)

        # compute closed-loop gain at those frequencies
        gain = self.plant.fresp(fr) * ctrl.fresp(fr)
        gain = np.abs(1/(1+gain))
        # take the maximum
        return 20*np.log10(gain.max())

    def compute_pole_q(self, ctrl=None):
        """
        Return the maximum Q of the closed-loop response of the current controller

        Parameters
        ----------
        ctrl: TF
            controller

        Returns
        -------
        q: float
            closed loop maximum q
        """
        if ctrl is None:
            ctrl = self.ctrl
        # compute closed loop poles
        poles = self.closed_loop_poles(ctrl=ctrl)
        freqs, Q = TF().s_to_fQ(poles)
        # return maximum
        return Q[~np.isnan(Q)].max()

    def margins(self, ctrl=None):
        """
        Compute gain and phase margins and overshoot

        Parameters
        ----------
        ctrl: TF
            if passed, overrides object controller

        Returns
        -------
        gain_margin: list of (float, float)
            list of (frequency, gain margin [deg])
        phase_margin: list of (float, float)
            frequency, phase margin [deg]
         """

        if ctrl is None:
            ctrl = self.ctrl

        # compute open-loop gain
        olg = self.plant.fresp(self.fr_stab) * ctrl.fresp(self.fr_stab)

        # find frequencies where gain is unity
        gain = abs(olg)
        g = gain - 1
        phase = 180/np.pi*np.angle(olg)
        idx = np.where(g[:-1]*g[1:] < 0)[0]   # zero crossings of abs(olg) - 1
        # compute phase for each of them
        phase_margins = []
        for i in idx:
            phase_margins.append([0.5*(self.fr_stab[i]+self.fr_stab[i+1]), 0.5*(phase[i]+phase[i+1])])

        # find frequencies where phase crosses +-180
        mphase = np.angle(-olg)
        idx = np.where(mphase[:-1] * mphase[1:] < 0)[0]  # zero crossing of angle(-olg)
        # compute gain for each phase crossing
        gain_margins = []
        for i in idx:
            gain_margins.append(
                [0.5 * (self.fr_stab[i] + self.fr_stab[i + 1]), 20*np.log10(0.5 * (gain[i] + gain[i + 1]))])
        # find the smallest positive and negative margins
        gm_pos = [g for g in gain_margins if g[1]>0]
        if len(gm_pos):
            i_pos = np.argmin([g[1] for g in gm_pos])
            gm_pos = gm_pos[i_pos]
        else:
            gm_pos = [0, np.Inf]
        gm_neg = [g for g in gain_margins if g[1]<0]
        if len(gm_neg):
            i_neg = np.argmax([g[1] for g in gm_neg])
            gm_neg = gm_neg[i_neg]
        else:
            gm_neg = [np.Inf, -np.Inf]
        gain_margins = [gm_pos, gm_neg]

        # find the smallest phase margin
        if len(phase_margins):
            pm = np.argmin([p[1]+180 for p in phase_margins])
            phase_margin = [phase_margins[pm][0], phase_margins[pm][1]+180]
        else:
            phase_margin = [0, np.Inf]

        # return results
        return np.abs(gain_margins).min(), phase_margin[1]


    def error_signal_rms(self, ctrl=None):
        """
        Compute the closed-loop RMS of the error signal

        Parameters
        ----------
        ctrl: TF
            controller

        Returns
        -------
        rms: float
            integrated RMS of the error signal

        """
        if ctrl is None:
            ctrl = self.ctrl
        # open-loop gain and closed-loop gain
        olg = ctrl.fresp(self.fr_errorsignal) * self.plant.fresp(self.fr_errorsignal)
        clg = 1 / (1 + olg)
        # new inloop error signal
        error_inloop = self.sp_errorsignal_outloop * abs(clg)
        return np.sqrt((error_inloop**2).sum() * (self.fr_errorsignal[1] - self.fr_errorsignal[0]))

    def load_error_signal(self):
        """
        Open a dialog to load error signal spectrum data
        """
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open error signal spectrum data", "",
                                                            "All Files (*)")
        if fileName:
            # load data
            data = np.loadtxt(fileName)
            self.fr_errorsignal = data[:,0]
            self.sp_errorsignal_inloop0 = data[:,1]
            # compute and compensate loop effect
            olg = self.ctrl0.fresp(self.fr_errorsignal) * self.plant.fresp(self.fr_errorsignal)
            clg = 1/(1+olg)
            self.sp_errorsignal_outloop = self.sp_errorsignal_inloop0 / abs(clg)
            # move the error signal plot on top
            self.area.moveDock(self.dock_psd, 'above', self.dock_clg1)
            # update plots
            self.updatePlots()

    def clicked(self, evt):
        """
        Handle the signal generated by the user clicking on the plot
        """

        # click position
        pos = evt.scenePos()
        # act only if clicked inside abs plot
        if self.plot_abs.sceneBoundingRect().contains(pos):
            # click point in plot coordinates
            mousePoint = self.plot_abs.plotItem.vb.mapSceneToView(pos)
            # do something only if one of the draw buttons is checked
            if (self.no_lower_than_pick.isChecked() or self.no_larger_than_pick.isChecked() or
                    self.mingain_pick.isChecked() or self.maxgain_pick.isChecked()):
                self.adding_what = 1*self.no_lower_than_pick.isChecked() + 2*self.no_larger_than_pick.isChecked() \
                                    + 3*self.mingain_pick.isChecked() + 4*self.maxgain_pick.isChecked()
                # check at which stage of drawing we are
                if self.adding_line == 0:
                    # just started
                    if self.adding_what == 1:
                        # gain no lower than, so point should be below current curve
                        if (mousePoint.y() <= 20*np.log10(abs(self.plant.fresp(10**mousePoint.x()) *
                                                              self.ctrl0.fresp(10**mousePoint.x())))):
                            self.line = [[mousePoint.x(), mousePoint.y()]]
                            self.adding_line = 1
                            self.status.setText('')
                        else:
                            self.status.setText('Setting constraint for maximum value of gain, new point must be'
                                                ' smaller than the current open loop gain.')
                    elif self.adding_what == 2:
                        # gain no larger than, so point should be above current curve
                        if (mousePoint.y() >= 20*np.log10(abs(self.plant.fresp(10**mousePoint.x()) *
                                                              self.ctrl0.fresp(10**mousePoint.x())))):
                            self.line = [[mousePoint.x(), mousePoint.y()]]
                            self.adding_line = 1
                            self.status.setText('')
                        else:
                            self.status.setText('Setting constraint for minimum value of gain, new point must be'
                                                ' larger than the current open loop gain.')
                    else:
                        # in other cases any point is good
                        self.line = [[mousePoint.x(), mousePoint.y()]]
                        self.status.setText('')
                        self.adding_line = 1
                elif self.adding_line == 1:
                    # we have already a line in the works, add point
                    # check if the point (and the last segment) is at an acceptable position
                    if self.adding_what == 1:
                        # gain no lower than, so segment should be below current curve
                        fr = np.linspace(self.line[-1][0], mousePoint.x(), 100)
                        olg = 20 * np.log10(abs(self.plant.fresp(10**fr) * self.ctrl0.fresp(10**fr)))
                        seg = self.line[-1][1] + (fr - self.line[-1][0]) * \
                               (mousePoint.y() - self.line[-1][1])/(mousePoint.x() - self.line[-1][0])
                        if all(seg <= olg):
                            self.line.append([mousePoint.x(), mousePoint.y()])
                            self.status.setText('')
                        else:
                            self.status.setText('Setting constraint for maximum value of gain, news point must be'
                                                ' smaller than the current open loop gain.')
                    elif self.adding_what == 2:
                        # gain no larger than, so segment should be above current curve
                        fr = np.linspace(self.line[-1][0], mousePoint.x(), 100)
                        olg = 20 * np.log10(abs(self.plant.fresp(10 ** fr) * self.ctrl0.fresp(10 ** fr)))
                        seg = self.line[-1][1] + (fr - self.line[-1][0]) * \
                              (mousePoint.y() - self.line[-1][1]) / (mousePoint.x() - self.line[-1][0])
                        if all(seg >= olg):
                            self.line.append([mousePoint.x(), mousePoint.y()])
                            self.status.setText('')
                        else:
                            self.status.setText('Setting constraint for minimum value of gain, new points must be'
                                                ' larger than the current open loop gain.')
                    else:
                        # in other cases any point is good
                        self.line.append([mousePoint.x(), mousePoint.y()])
                        self.status.setText('')

                # double click terminates
                if evt.double():
                    # move temporary line to the right pointer
                    if self.adding_what == 1:
                        self.line_no_lower_than = self.line.copy()
                    elif self.adding_what == 2:
                        self.line_no_larger_than = self.line.copy()
                    if self.adding_what == 3:
                        self.line_mingain = self.line.copy()
                    elif self.adding_what == 4:
                        self.line_maxgain = self.line.copy()
                    # remove temporary line
                    self.plot_abs.removeItem(self.temp_line)
                    self.temp_line = None
                    self.line = None
                    self.adding_line = 0

                    # deselect all buttons
                    if self.no_lower_than_pick.isChecked():
                        self.no_lower_than_pick.toggle()
                    if self.no_larger_than_pick.isChecked():
                        self.no_larger_than_pick.toggle()
                    if self.mingain_pick.isChecked():
                        self.mingain_pick.toggle()
                    if self.maxgain_pick.isChecked():
                        self.maxgain_pick.toggle()

                    # replot
                    self.status.setText('')
                    self.updatePlots()

    def mouseMoved(self, evt):
        """
        Called when the mouse pointer moves inside the plot area
        """
        pos = evt[0]
        if self.plot_abs.sceneBoundingRect().contains(pos):
            # get the mouse position
            mousePoint = self.plot_abs.plotItem.vb.mapSceneToView(pos)
            if (self.no_lower_than_pick.isChecked() or self.no_larger_than_pick.isChecked() or
                    self.mingain_pick.isChecked() or self.maxgain_pick.isChecked()):
                # plot line if we're adding one
                if self.adding_line == 1:
                    line = self.line.copy()
                    line.append([mousePoint.x(), mousePoint.y()])
                    x = np.array(line)[:,0]
                    y = np.array(line)[:,1]

                    if self.temp_line is None:
                        self.temp_line = self.plot_abs.plot(10**x, y, pen=pg.mkPen('k', width=2))
                    else:
                        self.temp_line.setData(10**x, y)

    def update_text_fields(self):
        """
        Called when the user finishes editing the text input values
        """

        if self.no_lower_than_combo.currentIndex() == 0:
            if self.no_lower_than_fmin.text() != '' and self.no_lower_than_fmax.text() != '':
                # get range and update trace
                fmin = float(self.no_lower_than_fmin.text())
                fmax = float(self.no_lower_than_fmax.text())
                fr = np.geomspace(fmin, fmax, self.nfr)
                olg = self.plant.fresp(fr) * self.ctrl0.fresp(fr)
                self.line_no_lower_than = np.c_[np.log10(fr), 20*np.log10(np.abs(olg))]
                self.updatePlots()
        if self.no_larger_than_combo.currentIndex() == 0:
            if self.no_larger_than_fmin.text() != '' and self.no_larger_than_fmax.text() != '':
                # get range and update trace
                fmin = float(self.no_larger_than_fmin.text())
                fmax = float(self.no_larger_than_fmax.text())
                fr = np.geomspace(fmin, fmax, self.nfr)
                olg = self.plant.fresp(fr) * self.ctrl0.fresp(fr)
                self.line_no_larger_than = np.c_[np.log10(fr), 20*np.log10(np.abs(olg))]
                self.updatePlots()
        if self.mingain_combo.currentIndex() == 0:
            if self.mingain_fmin.text() != '' and self.mingain_fmax.text() != '':
                # get range and update trace
                fmin = float(self.mingain_fmin.text())
                fmax = float(self.mingain_fmax.text())
                fr = np.geomspace(fmin, fmax, self.nfr)
                olg = self.plant.fresp(fr) * self.ctrl0.fresp(fr)
                self.line_mingain = np.c_[np.log10(fr), 20*np.log10(np.abs(olg))]
                self.updatePlots()
        if self.maxgain_combo.currentIndex() == 0:
            if self.maxgain_fmin.text() != '' and self.maxgain_fmax.text() != '':
                # get range and update trace
                fmin = float(self.maxgain_fmin.text())
                fmax = float(self.maxgain_fmax.text())
                fr = np.geomspace(fmin, fmax, self.nfr)
                olg = self.plant.fresp(fr) * self.ctrl0.fresp(fr)
                self.line_maxgain = np.c_[np.log10(fr), 20*np.log10(np.abs(olg))]
                self.updatePlots()

    def run_optimization(self, ctrl0, add_pz=0):
        """
        Run the optimization with the current set of parameters

        Parameters
        ----------
        ctrl0: TF
            initial controller transfer function
        add_pz: int
            how many pole/zero pair to add
        """

        self.status.setText('')

        # make a copy of ctrl0 to avoid overwriting
        ctrl0 = ctrl0.copy()

        # check if we are adding poles and zeros
        if add_pz != 0:
            # find frequency range for poles and zeros
            nz0 = ctrl0.z_f.shape[0]
            np0 = ctrl0.p_f.shape[0]
            freqs = np.concatenate([ctrl0.z_f, ctrl0.p_f])
            freqs = freqs[freqs != 0]   # exclude zeros
            fmin_ = freqs.min()
            fmax_ = freqs.max()
            if self.absminfr.text() != '':
                fmin_ = min(fmin_, float(self.absminfr.text()))
            if self.absminfr.text() != '':
                fmax_ = min(fmax_, float(self.absmaxfr.text()))
            # compute new frequencies, logarithmically spaced in the interval
            new_freqs = np.geomspace(fmin_, fmax_, add_pz+2)[1:-1]
            # add a zero/pole pair for each frequency
            for f in new_freqs:
                ctrl0.add_pole_fQ(f, 1)
                ctrl0.add_zero_fQ(f, 1)

        ### build initial parameter vector (frequency, Q and gain are relative to current for better scaling)
        p0 = np.r_[np.ones_like(ctrl0.z_f),
                   np.ones_like(ctrl0.p_f),
                   np.ones_like(ctrl0.z_Q),
                   np.ones_like(ctrl0.p_Q), 1]

        ### parameter bounds (relative to current values) from input text boxes
        fmin = float(self.minfr.text())
        fmax = float(self.maxfr.text())
        qmin = float(self.minq.text())
        qmax = float(self.maxq.text())
        gmin = float(self.ming.text())
        gmax = float(self.maxg.text())
        bounds = (ctrl0.z_f.shape[0] * [[fmin, fmax]] +
                  ctrl0.p_f.shape[0] * [[fmin, fmax]] +
                  ctrl0.z_Q.shape[0] * [[qmin, qmax]] +
                  ctrl0.p_Q.shape[0] * [[qmin, qmax]] +
                  [[gmin, gmax]])
        # increase bounds for the poles and zeros that were just added, so that they can move anywhere
        for i in range(add_pz):
            bounds[nz0 + i] = [fmin_/new_freqs[i], fmax_/new_freqs[i]]
            bounds[ctrl0.z_f.shape[0] + np0 + i] = [fmin_/new_freqs[i], fmax_/new_freqs[i]]
            bounds[ctrl0.z_f.shape[0] + ctrl0.p_f.shape[0] + nz0 + i] = [0.1, 10]
            bounds[2*ctrl0.z_f.shape[0] + ctrl0.p_f.shape[0] + np0 + i] = [0.1, 10]

        # take into account absolute bounds, if any
        if self.absminfr.text() != '':
            afmin = float(self.absminfr.text())
        else:
            afmin = 0
        if self.absmaxfr.text() != '':
            afmax = float(self.absmaxfr.text())
        else:
            afmax = np.Inf
        if self.absmaxq.text() != '':
            aqmax = float(self.absmaxq.text())
        else:
            aqmax = np.Inf
        bmin = np.r_[afmin / abs(ctrl0.z_f), afmin / ctrl0.p_f,
                     np.zeros_like(ctrl0.z_Q), np.zeros_like(ctrl0.p_Q), 0]
        bmax = np.r_[afmax / abs(ctrl0.z_f), afmax / ctrl0.p_f,
                     aqmax / ctrl0.z_Q, aqmax / ctrl0.p_Q, np.Inf]

        # take the most restrictive min and max
        self.bounds = [[max(bounds[i][0], bmin[i]), min(bounds[i][1], bmax[i])] for i in range(len(bounds))]

        ### define the function that converts the parameter vector to the TF controller
        n_z = ctrl0.z_f.shape[0]   # number of zeros
        n_p = ctrl0.p_f.shape[0]   # number of poles
        def p2ctrl(p):
            # extract zero and pole frequencies and Qs
            z_f = ctrl0.z_f * p[0:n_z]
            p_f = ctrl0.p_f * p[n_z:n_z + n_p]
            z_Q = ctrl0.z_Q * p[n_z + n_p:2 * n_z + n_p]
            p_Q = ctrl0.p_Q * p[2 * n_z + n_p:2 * n_z + 2 * n_p]
            k = ctrl0.k * p[-1]
            # build transfer function
            ctrl = TF()
            ctrl.set_fQ(z_f, z_Q, p_f, p_Q, k)
            return ctrl

        ### constraints
        self.constraints = []

        # always include a strict stability constraint, Re(poles)<0
        def stability(p):
            ctrl = p2ctrl(p)
            poles = self.closed_loop_poles(ctrl=ctrl)
            return np.real(poles).max()
        # must be < 0, although this is <= 0
        self.constraints.append(scipy.optimize.NonlinearConstraint(stability, -np.Inf, 0))

        # constrain value of OLG to be no lower than
        if self.no_lower_than_check.isChecked():
            if self.line_no_lower_than is None:
                self.status.setText("<font color=#ff0000>Must specify a constraint on the minimum open loop gain when"
                            " option 'Open loop gain no lower than' is selected</font>")
                return
            else:
                # compute frequency range and values by interpolating in log scale
                fr = np.array(self.line_no_lower_than)[:,0]
                value = np.array(self.line_no_lower_than)[:,1]
                self.no_lower_than_fr = np.log10(np.geomspace(10**fr.min(), 10**fr.max(), self.nfr_constraints))
                self.no_lower_than_value = np.interp(self.no_lower_than_fr, fr, value)
                self.no_lower_than_fr = 10**self.no_lower_than_fr
                def no_lower_than_constraint(p):
                    ctrl = p2ctrl(p)
                    olg = 20*np.log10(np.abs(self.plant.fresp(self.no_lower_than_fr) *
                                             ctrl.fresp(self.no_lower_than_fr)))
                    return (olg - self.no_lower_than_value).min()
                self.constraints.append(scipy.optimize.NonlinearConstraint(no_lower_than_constraint, 0, np.Inf))

        # constrain value of OLG to be no larger than
        if self.no_larger_than_check.isChecked():
            if self.line_no_larger_than is None:
                self.status.setText("<font color=#ff0000>Must specify a constraint on the maximum open loop gain when"
                            " option 'Open loop gain no larger than' is selected</font>")
                return
            else:
                # compute frequency range and values by interpolating in log scale
                fr = np.array(self.line_no_larger_than)[:,0]
                value = np.array(self.line_no_larger_than)[:,1]
                self.no_larger_than_fr = np.log10(np.geomspace(10**fr.min(), 10**fr.max(), self.nfr_constraints))
                self.no_larger_than_value = np.interp(self.no_larger_than_fr, fr, value)
                self.no_larger_than_fr = 10**self.no_larger_than_fr
                def no_larger_than_constraint(p):
                    ctrl = p2ctrl(p)
                    olg = 20*np.log10(np.abs(self.plant.fresp(self.no_larger_than_fr) *
                                             ctrl.fresp(self.no_larger_than_fr)))
                    return (self.no_larger_than_value - olg).min()
                self.constraints.append(scipy.optimize.NonlinearConstraint(no_larger_than_constraint, 0, np.Inf))

        # constrain closed loop overshoot
        if self.overshoot_check.isChecked():
            max_over = float(self.overshoot.text())
            if max_over < self.compute_overshoot(ctrl=ctrl0):
                self.status.setText('<font color=#ff0000>Current controller has overshoot '
                                    '%.2f db that is larger than constraint.</font>' %
                            self.compute_overshoot())
                return
            def overshoot_constraint(p):
                ctrl = p2ctrl(p)
                os = self.compute_overshoot(ctrl=ctrl)
                return max_over - os
            self.constraints.append(scipy.optimize.NonlinearConstraint(overshoot_constraint, 0, np.Inf))

        # constrain phase margin
        if self.phasemargin_check.isChecked():
            min_pm = float(self.phasemargin.text())
            gm, pm = self.margins(ctrl=ctrl0)
            if min_pm > pm:
                self.status.setText('<font color=#ff0000>Current controller has phase margin '
                                    '%.2f deg that is smaller than constraint.</font>' % pm)
                return
            def phasemargin_constraint(p):
                ctrl = p2ctrl(p)
                gm, pm = self.margins(ctrl=ctrl)
                return pm - min_pm
            self.constraints.append(scipy.optimize.NonlinearConstraint(phasemargin_constraint, 0, np.Inf))

        # constrain gain margin
        if self.gainmargin_check.isChecked():
            min_gm = float(self.gainmargin.text())
            gm, pm = self.margins(ctrl=ctrl0)
            if min_gm > gm:
                self.status.setText('<font color=#ff0000>Current controller has gain margin '
                                    '%.2f db that is smaller than constraint.</font>' % gm)
                return
            def gainmargin_constraint(p):
                ctrl = p2ctrl(p)
                gm, pm = self.margins(ctrl=ctrl)
                return gm - min_gm
            self.constraints.append(scipy.optimize.NonlinearConstraint(gainmargin_constraint, 0, np.Inf))

        # constrain maximum value of closed-loop pole Q
        if self.max_clq_check.isChecked():
            max_q = float(self.max_clq.text())
            if max_q < self.compute_pole_q().max():
                self.status.setText(
                    '<font color=#ff0000>Current controller has largest closed-loop pole Q'
                    ' %.2f that is larger than constraint.</font>' %
                            self.compute_pole_q().max()())
                return
            def clq_constraint(p):
                ctrl = p2ctrl(p)
                q = self.compute_pole_q(ctrl=ctrl).max()
                return max_q - q
            self.constraints.append(scipy.optimize.NonlinearConstraint(clq_constraint, 0, np.Inf))

        ### figures of merit

        # minimize gain in band
        if self.mingain_check.isChecked():
            if self.line_mingain is None:
                self.status.setText("<font color=#ff0000>Must specify a reference gain when the figure of merit"
                            " 'Minimize gain with respect to' is selected</font>")
                return
            else:
                # compute reference by interpolating
                fr = np.array(self.line_mingain)[:, 0]
                value = 10**(np.array(self.line_mingain)[:, 1]/20)
                # log or linear binning
                if self.norm_log.isChecked():
                    self.mingain_fr = np.log10(np.geomspace(10**fr.min(), 10**fr.max(), self.nfr_constraints))
                else:
                    self.mingain_fr = np.log10(np.linspace(10**fr.min(), 10**fr.max(), self.nfr_constraints))
                self.mingain_value = np.interp(self.mingain_fr, fr, value)
                self.mingain_fr = 10**self.mingain_fr
                self.mingain_scale = 1
                # FOM is normalized sum of ratio of current OLG over reference. Value is 1 for current controller
                def fom_mingain(p):
                    ctrl = p2ctrl(p)
                    olg = np.abs(self.plant.fresp(self.mingain_fr) * ctrl.fresp(self.mingain_fr))
                    return self.mingain_scale * (olg / self.mingain_value).sum()
                self.mingain_scale = 1/fom_mingain(p0)
        else:
            self.mingain_scale = 0
            def fom_mingain(p):
                return 0

        # maximize gain in band
        if self.maxgain_check.isChecked():
            if self.line_maxgain is None:
                self.status.setText("<font color=#ff0000>Must specify a reference gain when the figure of merit"
                            " 'Maximize gain with respect to' is selected</font>")
                return
            else:
                # compute reference by interpolating
                fr = np.array(self.line_maxgain)[:, 0]
                value = 10**(np.array(self.line_maxgain)[:, 1]/20)
                # log or linear binning
                if self.norm_log.isChecked():
                    self.maxgain_fr = np.log10(np.geomspace(10**fr.min(), 10**fr.max(), self.nfr_constraints))
                else:
                    self.maxgain_fr = np.log10(np.linspace(10**fr.min(), 10**fr.max(), self.nfr_constraints))
                self.maxgain_value = np.interp(self.maxgain_fr, fr, value)
                self.maxgain_fr = 10**self.maxgain_fr
                self.maxgain_scale = 1
                # FOM is normalized sum of ratio of reference over current OLG. Value is 1 for current controller
                def fom_maxgain(p):
                    ctrl = p2ctrl(p)
                    olg = np.abs(self.plant.fresp(self.maxgain_fr) * ctrl.fresp(self.maxgain_fr))
                    return self.maxgain_scale * (self.maxgain_value / olg).sum()
                self.maxgain_scale = 1/fom_maxgain(p0)
        else:
            self.maxgain_scale = 0
            def fom_maxgain(p):
                return 0

        # minimize total error signal RMS
        if self.rms_check.isChecked():
            if self.fr_errorsignal is None:
                self.status.setText("<font color=#ff0000>Must load an error signal spectrum when the figure of merit"
                                    " 'Minimize RMS' is selected</font>")
                return
            else:
                # FOM is normalized to 1 for current controller
                self.rms_scale = 1
                def fom_rms(p):
                    ctrl = p2ctrl(p)
                    return self.rms_scale * self.error_signal_rms(ctrl)
                self.rms_scale = 1/fom_rms(p0)
        else:
            self.rms_scale = 0
            def fom_rms(p):
                return 0

        # define sum of FOMs, with weights
        w_rms = float(self.rms_weight.text())
        w_max = float(self.maxgain_weight.text())
        w_min = float(self.mingain_weight.text())
        def fom(p):
            return w_rms*fom_rms(p) + w_max*fom_maxgain(p) + w_min*fom_mingain(p)

        ### prepare the infrastructure to run the optimization in a separate thread
        class WorkerSignals(QtCore.QObject):
            # signals
            finished = QtCore.pyqtSignal()
            update = QtCore.pyqtSignal(TF)

        class Worker(QtCore.QRunnable):
            # just a class to run the optimization in a separate thread
            def __init__(self, p0, bounds, cost, constraints, parent):
                super(Worker, self).__init__()
                self.p0 = p0
                self.bounds = bounds
                self.cost = cost
                self.signals = WorkerSignals()
                self.parent = parent
                self.constraints = constraints
                self.count = 0

            def callback(self, res):
                # called every time the optimization does one step, to update values and plots

                # compute FOMs
                f_rms = fom_rms(res)
                f_max = fom_maxgain(res)
                f_min = fom_mingain(res)
                self.count += 1
                # update message
                msg  = 'Num poles: %d, Num zeros: %d\n' % (ctrl0.p_f.shape[0], ctrl0.z_f.shape[0])
                msg += 'Iteration %d\n' % self.count
                msg += 'Total FOM:            %10.6f\n' % (w_rms * f_rms + w_max * f_max + w_min * f_min)
                if f_rms != 0:
                    msg += 'Error signal RMS FOM: %10.6f\n' % f_rms
                if f_min != 0:
                    msg += 'Minimize gain FOM:    %10.6f\n' % f_min
                if f_max != 0:
                    msg += 'Maximize gain FOM:    %10.6f\n' % f_rms
                self.parent.status.setText(msg)
                # if we want real time plots, emit a signal to update the results
                if self.parent.realtime.isChecked():
                    self.signals.update.emit(p2ctrl(res))

            def run(self):
                # run the minimization
                from scipy.optimize import minimize
                # pick method
                if self.parent.methods_combo.currentText() == 'Default':
                    method = None
                else:
                    method = self.parent.methods_combo.currentText()
                # run the actual minimization
                self.parent.res = scipy.optimize.minimize(fom, p0, bounds=self.bounds,
                                                          constraints=self.constraints,
                                                          callback=self.callback,
                                                          method=method,
                                                          options={'maxiter':1000})
                # update with best solution
                self.parent.ctrl = p2ctrl(self.parent.res.x)
                # send the signal the means the optimization is finished
                self.signals.finished.emit()

        # do the actual minimization
        worker = Worker(p0, self.bounds, fom, self.constraints, self)
        # connect signal for finished optimization to function that append the result to list
        worker.signals.finished.connect(self.add_result)
        # if we want real time plot, connect the signal that is emitted at every callback
        if self.realtime.isChecked():
            worker.signals.update.connect(self.updatePlots)
        # start thread
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.start(worker)

    def revert(self):
        # revert back to original controller and clear optimization
        self.ctrl = self.ctrl0.copy()
        self.updatePlots()
        self.status.setText('')
        self.button_accept.setDisabled(True)
        self.button_revert.setDisabled(True)

    # called when all optimizations are finished
    def optimization_finished(self):
        # append all successfull optimizations
        results = [r[0]['fun'] for r in self.results if r[0]['success']]
        if len(results):
            # find the best, lowest total FOM
            best = self.results[np.argmin(results)]
            self.ctrl = best[1].copy()
            msg = 'Optimization concluded.\nBest FOM %10.6f\n' % best[0]['fun']
            msg += 'Number of poles: %d, number of zeros: %d' % (self.ctrl.p_f.shape[0], self.ctrl.z_f.shape[0])
            self.status.setText(msg)
            # update all plots and enable buttons
            self.updatePlots()
            self.button_accept.setDisabled(False)
            self.button_revert.setDisabled(False)
        else:
            # no optimization was successfull, print diagnostic to terminal
            self.status.setText('Optimization concluded unsuccessfully, see terminal for diagnostic messages'
                                ' returned by scipy.optimize.minimize.')
            for r in self.results:
                print(r)
                print()
            self.button_revert.setDisabled(False)

    # called when one optimization is concluded
    def add_result(self):
        # append the result
        self.results.append([self.res.copy(), self.ctrl.copy()])
        # remove the first item in the list of runs, since we just finished doing it
        self.runs = self.runs[1:]
        if len(self.runs):
            # if anything left to do, run next optimization
            self.run_optimization(self.ctrl0, add_pz=self.runs[0])
        else:
            # all done, call final function
            self.optimization_finished()

    # called when the user clicks on the optimization button
    def optimization(self):
        # build the list of optimizations to run
        self.runs = [0]  # always run once without any additional poles and zeros
        if self.add_pz_check.isChecked():
            if self.num_add_pz.text() != '':
                # add additional runs with added zeros and poles
                num_pz = int(self.num_add_pz.text())
                for i in range(1, num_pz+1):
                    self.runs.append(i)
        # start the first optimization in the list, when done it will emit a signal to call add_result()
        # that in turn will call the next, until done
        self.run_optimization(self.ctrl0, add_pz=self.runs[0])

    # called when user clicks on Accept
    def accept(self):
        if self.parent is not None:
            # propagate result to LoopDesigner parent
            self.parent.undo.put(self.parent.ctrl.copy())
            self.parent.ctrl = self.ctrl
            self.close()


