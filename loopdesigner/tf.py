# tf.py - InteractiveFitting, Gabriele Vajente (vajente@caltech.edu), 2024-01-03

import numpy
import numpy as np
import scipy
import scipy.signal

class TF:
    """
    Define and handle a transfer function.
    2023-10-30 - Gabriele Vajente (vajente@caltech.edu)
    """

    def __init__(self, z=None, p=None, k=None, name=None, delay=0, pade_order=3, zero_to_nan=False):
        """
        Construct a transfer function. Zeros, Poles and gain (in Laplace domain)
        can be passed upon construction. Otherwise, the TF is empty (zero).

        The transfer function is defined as
        TF = k * prod(s - z[i]) / prod(s - p[i])

        Parameters
        ----------
        z: numpy.ndarray(complex)
            Optional, list of zeros
        p: numpy.ndarray(complex)
            Optional, list of poles
        k: float
            Optional, gain
        name: str
            Optional, the name of the transfer function
        delay: float
            delay in seconds (will be implemented with a Pade approximation if pade_order != 0)
        pade_order: int
            Pade approximation order (default is 3). If 0, no approximation is used, but then
            only the sampled frequency response contains the delay
        zero_to_nan: bool
            if True, substitute bins with zero value with a NaN
        """
        # if passed by user, store values, otherwise zero TF
        if z is None:
            z = numpy.array([])
        if p is None:
            p = numpy.array([])
        if k is None:
            k = 0

        # process delay if any
        if delay != 0:
            # pade approximant
            if pade_order > 0:
                pade_num, pade_den = self.pade(delay, pade_order)
                pade_z, pade_p, pade_k = scipy.signal.tf2zpk(pade_num, pade_den)
                # append
                z = numpy.concatenate([z, pade_z])
                p = numpy.concatenate([p, pade_p])
                k = k * pade_k
                # delay is approximated with Pade
                self.delay_approx_pade = True
                self.delay = delay
            else:
                # no approximation to delay, just store the value
                self.delay = delay
                self.delay_approx_pade = False
        else:
            self.delay = 0
            self.delay_approx_pade = False

        self.z = numpy.array(z)
        self.p = numpy.array(p)
        self.k = k

        # update other representations
        self.num, self.den = scipy.signal.zpk2tf(self.z, self.p, self.k)
        self.z_f, self.z_Q = self.s_to_fQ(self.z)
        self.p_f, self.p_Q = self.s_to_fQ(self.p)

        # keep name
        self.name = name
        self.zero_to_nan = zero_to_nan

    def set_zpk(self, z, p, k):
        """
        Set the transfer function z,p,k
        Parameters
        ----------
        z: numpy.ndarray(complex)
            list of zeros
        p: numpy.ndarray(complex)
            list of poles
        k: float
            gain
        """
        self.z = numpy.array(z)
        self.p = numpy.array(p)
        self.k = k
        # update other representations
        self.num, self.den = scipy.signal.zpk2tf(self.z, self.p, self.k)
        self.z_f, self.z_Q = self.s_to_fQ(self.z)
        self.p_f, self.p_Q = self.s_to_fQ(self.p)

    def set_fQ(self, z_f, z_Q, p_f, p_Q, k):
        """
        Set the transfer function using frequency and Q of poles / zeros
        Parameters
        ----------
        z_f: numpy.ndarray(float)
            list of zero frequencies [Hz]
        z_Q: numpy.ndarray(float)
            list of zero Qs
        p_f: numpy.ndarray(float)
            list of pole frequencies [Hz]
        p_Q: numpy.ndarray(float)
            list of pole Qs
        k: float
            gain
        """
        z = self.fQ_to_s(z_f, z_Q)
        p = self.fQ_to_s(p_f, p_Q)
        self.z = numpy.array(z)
        self.p = numpy.array(p)
        self.k = k
        # update other representations
        self.num, self.den = scipy.signal.zpk2tf(self.z, self.p, self.k)
        self.z_f, self.z_Q = z_f, z_Q
        self.p_f, self.p_Q = p_f, p_Q

    def add_pole_fQ(self, f, Q):
        """
        Add a pole in f,Q format

        Parameters
        ----------
        f: float
            pole frequency [Hz]
        Q: float
            pole Q (0 mean real)
        """
        p_f = numpy.r_[self.p_f, f]
        p_Q = numpy.r_[self.p_Q, Q]
        if Q == 0:
            k = (2 * numpy.pi * f)
        else:
            k = (2 * numpy.pi * f) ** 2
        self.set_fQ(self.z_f, self.z_Q, p_f, p_Q, self.k * k)

    def add_zero_fQ(self, f, Q, k=1):
        """
        Add a zero in f,Q format

        Parameters
        ----------
        f: float
            zero frequency [Hz]
        Q: float
            zero Q (0 mean real)
        """
        z_f = numpy.r_[self.z_f, f]
        z_Q = numpy.r_[self.z_Q, Q]
        if Q == 0:
            k = (2 * numpy.pi * f)
        else:
            k = (2 * numpy.pi * f) ** 2
        self.set_fQ(z_f, z_Q, self.p_f, self.p_Q, self.k / k)

    def remove_pole_fQ(self, f, Q):
        """
        Remove a pole in f,Q format

        Parameters
        ----------
        f: float
            pole frequency [Hz]
        Q: float
            pole Q (0 mean real)
        """
        if not np.isinf(Q):
            i = numpy.argmin(abs(self.p_f - f) ** 2 + abs(self.p_Q - Q) ** 2)
        else:
            i = numpy.argmin(abs(self.p_f - f) ** 2)
            if not np.isinf(self.p_Q[i]):
                return
        ii = numpy.ones_like(self.p_f, dtype=bool)
        if Q == 0:
            k = (2 * numpy.pi * f)
        else:
            k = (2 * numpy.pi * f) ** 2

        ii[i] = False
        self.set_fQ(self.z_f, self.z_Q, self.p_f[ii], self.p_Q[ii], self.k / k)

    def remove_zero_fQ(self, f, Q):
        """
        Remove a zero in f,Q format

        Parameters
        ----------
        f: float
            zero frequency [Hz]
        Q: float
            zero Q (0 mean real)
        """
        if not np.isinf(Q):
            i = numpy.argmin(abs(self.z_f - f) ** 2 + abs(self.z_Q - Q) ** 2)
        else:
            i = numpy.argmin(abs(self.z_f - f) ** 2)
            if not np.isinf(self.z_Q[i]):
                return
        ii = numpy.ones_like(self.z_f, dtype=bool)
        if Q == 0:
            k = (2 * numpy.pi * f)
        else:
            k = (2 * numpy.pi * f) ** 2

        ii[i] = False
        self.set_fQ(self.z_f[ii], self.z_Q[ii], self.p_f, self.p_Q, self.k * k)

    def set_gain(self, f, g):
        """
        Set the transfer function gain at a given frequency

        Parameters
        ----------
        f: float
            frequency [Hz]
        g: float
            gain at the frequency f (absolute value)
        """
        self.k = self.k * abs(g / self.fresp(f))
        self.num, self.den = scipy.signal.zpk2tf(self.z, self.p, self.k)

    def set_k(self, k):
        """
        Set the transfer function gain k

        Parameters
        ----------
        k: float
            gain (in teh z,p,k sense)
        """
        self.k = k
        self.num, self.den = scipy.signal.zpk2tf(self.z, self.p, self.k)

    def s_to_fQ(self, s0):
        """
        Convert poles or zeros from Laplace plane to frequency [Hz] and Q.
        As a convention, Q is allowed to be >= 0.5.
        If Q = 0.5, this still represents a double pole / zero, although real.
        Q < 0.5 is not allowed.
        Q = 0 conventionally represents a real pole / zero

        Frequencies and Qs are returned only once for complex poles / zeros

        Parameters
        ----------
        s0: numpy.ndarray(complex)
            array of poles / zeros in Laplace domain

        Returns
        -------
        f: numpy.ndarray(float):
            array of frequencies
        Q: numpy.ndarray(float):
            array of Q values
        """

        # make sure we get an array as input
        s0 = numpy.array(s0).reshape(-1)
        # select only one pole/zero for each complex pair
        idx = numpy.imag(s0 >= 0)
        f = []
        Q = []
        for s0_ in s0:
            if numpy.imag(s0_) == 0:
                f.append(-numpy.sign(numpy.real(s0_)) * abs(s0_) / 2 / numpy.pi)
                Q.append(0)
            elif numpy.imag(s0_) > 0:
                if numpy.real(s0_) != 0:
                    f.append(-numpy.sign(numpy.real(s0_)) * numpy.abs(s0_) / 2 / numpy.pi)
                else:
                    f.append(numpy.abs(s0_) / 2 / numpy.pi)
                Q.append(abs(1 / (-2 * numpy.real(s0_) / 2 / numpy.pi / (numpy.abs(s0_) / 2 / numpy.pi))))
        return numpy.array(f).ravel(), numpy.array(Q).ravel()

    def fQ_to_s(self, f, Q):
        """
        Convert poles or zeros from frequency [Hz] and Q to Laplace plane.
        As a convention, Q is allowed to be >= 0.5.
        If Q = 0.5, this still represents a double pole / zero, although real.
        Q < 0.5 is not allowed.
        Q = 0 conventionally represents a real pole / zero

        Frequencies and Qs should be listed only once for complex poles / zeros

        Parameters
        ----------
        f: numpy.ndarray(float):
            array of frequencies
        Q: numpy.ndarray(float):
            array of Q values

        Returns
        -------
        s0: numpy.ndarray(complex)
            array of poles / zeros in Laplace domain
        """
        # make sure we have arrays as input
        f = numpy.array(f).reshape(-1)
        Q = numpy.array(Q).reshape(-1)

        # separate conversion for real and complex poles / zeros
        s = []
        for f_, Q_ in zip(f, Q):
            if Q_ == 0:
                s.append(-2 * numpy.pi * f_)
            else:
                if Q_ > 0.5:
                    s.append(-2 * numpy.pi * f_ / 2 / Q_ + numpy.sign(f_) * 1j * 2 * numpy.pi * f_ * numpy.sqrt(
                        1 - 1 / 4 / Q_ ** 2))
                    s.append(-2 * numpy.pi * f_ / 2 / Q_ - numpy.sign(f_) * 1j * 2 * numpy.pi * f_ * numpy.sqrt(
                        1 - 1 / 4 / Q_ ** 2))
                else:
                    s.append(-2 * numpy.pi * f_ / 2 / Q_ + numpy.sign(f_) * 2 * numpy.pi * f_ * numpy.sqrt(
                        1 / 4 / Q_ ** 2 - 1))
                    s.append(-2 * numpy.pi * f_ / 2 / Q_ - numpy.sign(f_) * 2 * numpy.pi * f_ * numpy.sqrt(
                        1 / 4 / Q_ ** 2 - 1))

        return numpy.array(s).ravel()

    def fresp(self, fr):
        """
        Compute the frequency response of the transfer function.

        Parameters
        ----------
        fr: numpy.ndarray(float)
            array of frequency values or single value

        Returns
        -------
        t: numpy.ndarray(complex)
            array of transfer function values for each frequency bin
        """

        t = numpy.polyval(self.num, 2j * numpy.pi * fr) / numpy.polyval(self.den, 2j * numpy.pi * fr)
        # substitute zeros with NaNs if required
        if self.zero_to_nan:
            if type(t) is np.ndarray:
                t[abs(t) == 0] = np.nan
        if not self.delay_approx_pade:
            t = t * np.exp(-2j*np.pi*fr*self.delay)
        return t

    def fresp_db_deg(self, fr):
        """
        Compute the frequency response of the transfer function in db and deg

        Parameters
        ----------
        fr: numpy.ndarray(float)
            array of frequency values or single value

        Returns
        -------
        db: numpy.ndarray(float)
            absolute value of the transfer function in db
        deg: numpy.ndarray(float)
            phase of the transfer function, in degrees
        """
        t = self.fresp(fr)
        return 20 * numpy.log10(numpy.abs(t)), 180 / numpy.pi * numpy.angle(t)

    def copy(self):
        """
        Returns a copy of the transfer function object
        """
        if not self.delay_approx_pade:
            return TF(z=self.z, p=self.p, k=self.k, zero_to_nan=self.zero_to_nan, delay=self.delay, pade_order=0)
        else:
            tf = TF(z=self.z, p=self.p, k=self.k, zero_to_nan=self.zero_to_nan)
            tf.delay_approx_pade = True
            tf.delay = self.delay
            return tf

    def pade(self, T, N):
        """
        Pade approximant of a delay T, of order N

        Parameters
        ----------
        d: float
            delay in seconds
        N: int
            approximation order (recommended for N<10, typically 3 is enough)

        Returns
        -------
        num: numpy.ndarray
            coefficients of numerator in s-domain
        den: numpy.ndarray
            coefficients of denominator in s-domain
        """

        num = [T ** i * (-1) ** i * (scipy.special.factorial(2 * N - i) * scipy.special.factorial(N)) /
               (scipy.special.factorial(2 * N) * scipy.special.factorial(i) * scipy.special.factorial(N - i)) for i in
               range(N + 1)]
        den = [T ** i * (scipy.special.factorial(2 * N - i) * scipy.special.factorial(N)) /
               (scipy.special.factorial(2 * N) * scipy.special.factorial(i) * scipy.special.factorial(N - i)) for i in
               range(N + 1)]

        return numpy.array(num)[::-1] / num[0], numpy.array(den)[::-1] / den[0]

    def add_delay(self, delay=0, pade_order=3):
        """
        Add a delay to the current transfer function, using a Pade approximant


        Parameters
        ----------
        delay: float
            delay in seconds
        pade_order: int
            order of the Pade approximant. If zero, the delay is not approximated

        """

        if delay != 0:
            if pade_order > 0:
                # pade approximant
                pade_num, pade_den = self.pade(delay, pade_order)
                pade_z, pade_p, pade_k = scipy.signal.tf2zpk(pade_num, pade_den)
                # append
                self.z = numpy.concatenate([self.z, pade_z])
                self.p = numpy.concatenate([self.p, pade_p])
                self.k = self.k * pade_k
                self.delay_approx_pade = True
            else:
                self.delay_approx_pade = False
            self.delay = delay

            # update other representations
            self.num, self.den = scipy.signal.zpk2tf(self.z, self.p, self.k)
            self.z_f, self.z_Q = self.s_to_fQ(self.z)
            self.p_f, self.p_Q = self.s_to_fQ(self.p)

    def simplify(self, tol=1e-9):
        """
        Simplify the transfer function, by removing poles and zeros that overlap in s-domain.

        Parameters
        ----------
        tol: float
            poles and zeros are considered concident if the absolute value of the difference is
            smaller than this

        Returns
        -------
        tf: TF
            new transfer function with redundant poles and zeros removed
        """

        Z = self.z.copy()
        P = self.p.copy()
        K = self.k

        Znew = []
        Pnew = []
        for z in Z:
            if not any(abs(P - z) < tol):
                Znew.append(z)
        for p in P:
            if not any(abs(Z - p) < tol):
                Pnew.append(p)

        return TF(Znew, Pnew, K)

    def add_zpk(self, z, p, k):
        """

        Parameters
        ----------
        z: numpy.ndarray
            list of zeros in Laplace domain
        p: numpy.ndarray
            list of poles in Laplace domain
        k: float
            gain
        """
        # add poles and zeros to list
        self.z = np.concatenate([self.z, z])
        self.p = np.concatenate([self.p, p])
        self.k = self.k * k
        # update other representations
        self.num, self.den = scipy.signal.zpk2tf(self.z, self.p, self.k)
        self.z_f, self.z_Q = self.s_to_fQ(self.z)
        self.p_f, self.p_Q = self.s_to_fQ(self.p)