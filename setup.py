from setuptools import setup

with open('README.md', 'rb') as f:
    longdesc = f.read().decode().strip()

setup(
    name='loopdesigner',
    version='0.1.2',
    description="Graphical user interface to design single-input single-output feedback control systems",
    long_description=longdesc,
    long_description_content_type='text/markdown',
    author='Gabriele Vajente',
    author_email='gabriele.vajente@ligo.org',
    url='https://git.ligo.org/gabriele-vajente/loopdesigner',
    license='GPL-3.0-or-later',
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Development Status :: 5 - Production/Stable',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],

    install_requires=[
        'numpy',
        'scipy',
        'pyqtgraph',
        'QtPy',
        'gpstime',
        'foton',
        'gwpy'
    ],

    packages=[
        'loopdesigner',
    ],
    include_package_data=True,

    package_data={
        '': ['loopdesigner/example_data/*',
             'loopdesigner/imgs/*',
             'loopdesigner/TUTORIAL.md'],
    },
)
