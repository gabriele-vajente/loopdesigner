# LoopDesigner

Graphical user interface to design and tune single-input single-output feedback control loops. Loosely inspired by
MATLAB sisotool

Gabriele Vajente (vajente@caltech.edu) - 2024-02-07


*Parameters*

- plant: TF or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
    - the plant to be controlled, either as a TF object, or as a list [z, p, k]
- controller: TF or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
    - optional, the controller transfer function, either as a TF object, or as a list [z, p, k]

If no plant is provided, opens a dialog to load a saved session from file.

Examples
--------

Run the script `from loopdesigner.examples import *` to define some example plants and controllers:
1. `plant` is a simple pendulum, and `ctrl` is a simple controller for this
2. `plant_dhard_y` is the O4 DHARD Y plant at LHO, fit from data, and `ctrl_dhard_y` is the O4 controller

You can then run either command to open the loop design interface
```
from loopdesigner import *
from loopdesigner.examples import *
LoopDesigner(plant=plant, controller=ctrl)
```

or

```
from loopdesigner import *
from loopdesigner.examples import *
LoopDesigner(plant=plant_dhard_y, controller=ctrl_dhard_y)
```

Please refer to the [TUTORIAL.md](tutorial/TUTORIAL.md) file for instructions on how to use the GUI.

![loopdesigner](loopdesigner.png)
