## Installation instructions

### Recommended from conda-forge
```
conda install -c conda-forge loopdesigner
```

### Manually from development version

Install dependencies

```
conda create -n loopdesigner python=3.9
conda activate loopdesigner
conda install -c conda-forge numpy scipy pyqtgraph pyqt python-foton gpstime ipython
```

Close this repository and from the main folder, run this to install
```
pip install .
```

Start LoopDesigner
```
>> ipython
Python 3.9.18 | packaged by conda-forge | (main, Dec 23 2023, 16:35:41) 
Type 'copyright', 'credits' or 'license' for more information
IPython 8.18.1 -- An enhanced Interactive Python. Type '?' for help.

In [1]: from loopdesigner import *

In [2]: from loopdesigner.example import *

In [3]: l = LoopDesigner(plant=plant, controller=ctrl)
```
